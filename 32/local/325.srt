﻿70
00:01:47,150 --> 00:01:48,480
你這麼說
Are you saying that

71
00:01:48,480 --> 00:01:50,700
是因為這事難以啟齒嗎
because the things are unspeakable?

72
00:01:50,700 --> 00:01:51,900
你的父母年紀大了
Your parents are old.

73
00:01:51,900 --> 00:01:53,900
九點半前肯定就完事了
Anything unspeakable was finished by 9:30.

74
00:01:53,900 --> 00:01:55,120
去睡吧
Go to sleep.

75
00:01:55,950 --> 00:01:58,150
好的
Very well.

76
00:01:59,990 --> 00:02:03,710
很抱歉這事讓婚禮變得很尷尬
I'm sorry if this stuff is gonna make the ceremony awkward.

77
00:02:03,710 --> 00:02:05,460
天啊 我覺得我弟弟出獄這件事
God, I thought my brother fresh out of jail

78
00:02:05,460 --> 00:02:07,220
會讓所有人覺得不舒服 但是現在
was gonna make everyone uncomfortable, but now this...

79
00:02:07,250 --> 00:02:07,920
嘿 如果你們想讓我睡覺
Hey, if you want me to sleep

80
00:02:07,920 --> 00:02:10,720
就別再說話了
you're gonna have to stop talking.

81
00:02:39,690 --> 00:02:41,270
好了 我要去接我的家人了
Okay, I'm gonna go pick up my family.

82
00:02:41,270 --> 00:02:43,690
可能要一個半小時或者兩個小時 得看交通狀況
Like an hour and half, two hours, depending on traffic.

83
00:02:43,690 --> 00:02:44,860
好的 小心開車
Yeah, drive safe. Oh, hey,

84
00:02:44,860 --> 00:02:46,130
答應我一件事 好嗎
and do yourself a favor, all right?

85
00:02:46,130 --> 00:02:49,260
貝弗利到這兒時 不要提起昨天晚上的事
When Beverly gets here, do not bring up last night.

86
00:02:49,260 --> 00:02:50,360
好嗎 對你們來說
All right? As far as you're concerned,

87
00:02:50,360 --> 00:02:52,560
你們什麼都不知道
you don't know anything,

88
00:02:52,570 --> 00:02:53,580
也什麼都沒看到
you didn't see anything.

89
00:02:53,580 --> 00:02:58,040
裝傻就好了
I want you just to play dumb.

90
00:02:58,040 --> 00:03:01,090
她在這兒很好的給我們示範了如何裝傻
It was nice of her to show us playing dumb with an example.

91
00:03:01,090 --> 00:03:02,510
什麼
What?

92
00:03:02,510 --> 00:03:04,210
喔
Oh.

93
00:03:04,210 --> 00:03:06,380
嗨 對不起 我現在該走了
Hi. Okay, hey there, I got-- I'm sorry, I got to go now.

94
00:03:06,380 --> 00:03:09,300
-佩妮 等等 -為什麼
- Penny, wait. - Why?

95
00:03:09,300 --> 00:03:12,180
我想感謝你們克服重重困難
I wanted to thank you for going through all the trouble

96
00:03:12,190 --> 00:03:15,440
邀請我來參加婚禮
of planning a second wedding ceremony for me,

97
00:03:15,440 --> 00:03:17,610
但很可惜我不能參加
but unfortunately I cannot attend.

98
00:03:17,610 --> 00:03:19,730
為什麼 怎麼了
Well, why? What's wrong?

99
00:03:19,730 --> 00:03:21,360
我們還要繼續裝傻嗎
Wha-- are we still doing the dumb thing?

100
00:03:21,360 --> 00:03:23,450
好的 為什麼 怎麼了
Okay, why, what's wrong?

101
00:03:23,450 --> 00:03:25,610
我只是不能在這兒忍受你爸
I just cannot stay here while your father

102
00:03:25,620 --> 00:03:27,820
故意讓我難堪
goes out of his way to humiliate me.

103
00:03:27,820 --> 00:03:30,950
天啊 他怎麼羞辱你了
Oh, golly, however did he humiliate you?

104
00:03:30,960 --> 00:03:32,570
別說了 謝爾頓
Stop it, Sheldon.

105
00:03:32,570 --> 00:03:36,080
我應該問別說什麼 還是不再說話了
Do I say "Stop what?" Or just throw in the towel?

106
00:03:36,080 --> 00:03:38,290
我不明白為什麼我要看著你爸
I don't see why I should have to watch your father

107
00:03:38,300 --> 00:03:41,500
和某個迷信的鄉巴佬在一起
parade around with some Bible-thumping bumpkin.

108
00:03:41,500 --> 00:03:43,880
等下 你是在說我媽嗎
Oh, excuse me, that is my mother you're talking about,

109
00:03:43,880 --> 00:03:46,090
雖然說得很對
however accurately.

110
00:03:46,090 --> 00:03:47,590
好了 貝弗利
Okay, Beverly,

111
00:03:47,590 --> 00:03:48,750
你有點反應過度了
aren't you overreacting a little?

112
00:03:48,760 --> 00:03:50,920
他們坐了同一輛計程車並喝了點酒
All we know is they shared a cab and had a nightcap.

113
00:03:50,930 --> 00:03:52,140
然後他們的電話就關機了
And turned their phones off.

114
00:03:52,140 --> 00:03:55,430
別解釋了 夥計
Not helping, buddy.

115
00:03:55,430 --> 00:03:57,900
媽媽 佩妮和我非常希望你參加
Mother, Penny and I really want you to be part of this.

116
00:03:57,900 --> 00:03:59,010
請留下來吧
Please stay.

117
00:03:59,020 --> 00:03:59,980
再說如果你走了
Yeah, plus if you leave,

118
00:03:59,980 --> 00:04:01,930
埃弗瑞德就知道他成功惹怒你了
Alfred will know he got under your skin.

119
00:04:05,410 --> 00:04:07,440
我才不能讓他得逞
Well, we can't have that.

120
00:04:08,830 --> 00:04:10,330
再說如果他們真的發生了關係
You know, also, if they did have coitus,

121
00:04:10,330 --> 00:04:12,530
我們都需要你這位經驗豐富的精神病醫生
we'll all be needing a skilled psychiatrist.

122
00:04:15,830 --> 00:04:18,280
你這麼想真有意思
It is funny when you think about it.

123
00:04:18,290 --> 00:04:19,330
對你來說而已
Maybe to you.

124
00:04:19,340 --> 00:04:21,500
你沒有一張五百美元的罰單
You didn't get a $500 traffic ticket.

125
00:04:21,510 --> 00:04:24,840
因為你開車像個瘋子一樣
Because you were driving like a lunatic.

126
00:04:24,840 --> 00:04:26,630
嘿 是因為政府特工
Hey, if thinking secret government agents

127
00:04:26,630 --> 00:04:30,800
在追著你讓你發狂的話 好吧 我是
are chasing you makes you a lunatic, then... yeah, okay.

128
00:04:32,880 --> 00:04:35,930
我去開門
Oh, I'll get it.

129
00:04:35,940 --> 00:04:37,640
這也不能解釋當警察把你攔下的時候
It didn't help that you couldn't walk a straight line

130
00:04:37,640 --> 00:04:39,810
你不能走直線
when the cop pulled you over.

131
00:04:39,810 --> 00:04:43,480
我有表現性焦慮癥
I have performance anxiety.

132
00:04:44,560 --> 00:04:47,560
你們都應該知道的
You of all people should know that.

133
00:04:57,320 --> 00:04:59,490
你好
Hello.

134
00:04:59,490 --> 00:05:01,240
我找霍華德·沃羅威茨
I'm looking for Howard Wolowitz.

135
00:05:01,250 --> 00:05:05,330
霍華德·沃羅威茨
Howard Wolowitz?

136
00:05:05,330 --> 00:05:07,550
我找對地址了 是嗎
I have the right address, don't I?

137
00:05:07,550 --> 00:05:10,340
地址
Address?

138
00:05:10,340 --> 00:05:11,970
他在這兒嗎
Is he here?

139
00:05:11,970 --> 00:05:15,670
不在
Um... no.

140
00:05:15,680 --> 00:05:17,730
你知道哪裏可以找到他嗎
Do you know where I can find him?