﻿28
00:02:52,710 --> 00:02:55,990
宇宙最初并不大 高温高密乱如麻
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}Our whole universe was in a hot dense state,

29
00:02:55,990 --> 00:02:59,920
一百四十亿年前 突如其来大爆炸 等一下
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}Then nearly fourteen billion years ago expansion started. Wait.

30
00:02:59,990 --> 00:03:01,340
地球开始降温啦
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}The Earth began to cool,

31
00:03:01,340 --> 00:03:02,760
自养生物霸天下
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}The autotrophs began to drool,

32
00:03:02,760 --> 00:03:04,350
穴居人民有文化
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}Neanderthals developed tools,

33
00:03:04,350 --> 00:03:06,220
我们建造古长城 我们建造金字塔
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}We built a wall, we built the pyramids,

34
00:03:06,220 --> 00:03:09,480
数学科学和历史 揭开神秘的面纱
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}Math, science, history, unraveling the mysteries,

35
00:03:09,480 --> 00:03:10,600
世间之万物 源于大爆炸
{\3c&H000000&\fs30}{\3c&H000000&\fs30}{\3c&H000000&\fs30}That all started with the big bang!

36
00:03:10,940 --> 00:03:12,440
{\fn方正大黑简体\fs60\c&H000000&\b1\bord0\shad0\fscy120\t(0,100,\fs80)\pos(634,702.4)\fad(50,0)}生活{\c&H070B7A&}大{\c&H0605B4&}爆炸{\fs10}
{\3c&H000000&\fs30}{\3c&H000000&\fs30\b1}{\fn微软雅黑\c&H182729&\fs40\shad1\4c&H43717E&\t(0,100,\fs48)}第十一季   第一集

37
00:00:01,340 --> 00:00:02,910
《生活大爆炸》前情提要
{\3c&H000000&\fs30}Previously on The Big Bang Theory...

38
00:00:03,270 --> 00:00:06,070
我得到了普林斯顿大学的暑期研究员职位
{\3c&H000000&\fs30}I was offered a summer research fellowship at Princeton.

39
00:00:06,070 --> 00:00:09,140
不错的大学
{\3c&H000000&\fs30}A fine institution.

40
00:00:09,140 --> 00:00:11,240
爱因斯坦曾经教书的地方
{\3c&H000000&\fs30}The place where Albert Einstein taught,

41
00:00:11,240 --> 00:00:13,340
以及莱纳德拿博士学位的地方
{\3c&H000000&\fs30}and where Leonard got his PhD,

42
00:00:13,340 --> 00:00:15,810
所以它可能已经走下坡路了
{\3c&H000000&\fs30}so it may have gone downhill.

43
00:00:15,810 --> 00:00:18,380
先生们 你们还记得诺维茨基博士吧
{\3c&H000000&\fs30}Gentlemen, you may remember Dr. Nowitzki.

44
00:00:18,380 --> 00:00:20,780
她又回加州理工读博士后了
{\3c&H000000&\fs30}She's back at Caltech for her postdoc.

45
00:00:20,780 --> 00:00:22,080
-嗨 -嗨
{\3c&H000000&\fs30}- Hello. - Hi.

46
00:00:23,550 --> 00:00:27,120
那末问题来了：你是想跟我建立恋爱关系吗
{\3c&H000000&\fs30}are you seeking a romantic relationship with me?

47
00:00:27,120 --> 00:00:28,720
假如我是呢
{\3c&H000000&\fs30}What if I were?

48
00:00:28,720 --> 00:00:32,190
呃，那会引发一系列问题
{\3c&H000000&\fs30}Well, that would raise a number of problems.