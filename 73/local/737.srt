﻿380
00:15:19,370 --> 00:15:22,140
结果发现是肚子里绦虫作祟
{\3c&H000000&\fs30}Turned out it was a tapeworm.

381
00:15:24,640 --> 00:15:26,380
好吧 呃
{\3c&H000000&\fs30}Cool. Uh...

382
00:15:27,450 --> 00:15:28,680
只是
{\3c&H000000&\fs30}It's just...

383
00:15:28,680 --> 00:15:30,850
要和其他朋友分享我的感受不容易
{\3c&H000000&\fs30}it's hard talking to my other friends about this,

384
00:15:30,850 --> 00:15:32,120
但我知道你会理解我的
{\3c&H000000&\fs30}but I knew you would understand.

385
00:15:32,120 --> 00:15:33,350
为什么
{\3c&H000000&\fs30}Why is that?

386
00:15:33,350 --> 00:15:34,980
因为你和我都是单身狗
{\3c&H000000&\fs30}Because you and I are both alone,

387
00:15:34,990 --> 00:15:36,990
说起来还有点小欣慰呢
{\3c&H000000&\fs30}which is actually kind of comforting,

388
00:15:36,990 --> 00:15:39,120
起码我们在单身的路上还能有个伴
{\3c&H000000&\fs30}because at least we can be alone together.

389
00:15:39,120 --> 00:15:41,490
这就尴尬了
{\3c&H000000&\fs30}Mm. This is-this is awkward.

390
00:15:41,490 --> 00:15:44,590
其实今晚我打算早点关店
{\3c&H000000&\fs30}I, um, I was actually gonna close up a little early tonight

391
00:15:44,600 --> 00:15:46,300
因为我还要去约会
{\3c&H000000&\fs30}'cause I have a date.

392
00:15:46,300 --> 00:15:48,060
真的吗
{\3c&H000000&\fs30}Really?

393
00:15:48,070 --> 00:15:49,870
真的
{\3c&H000000&\fs30}Yeah.

394
00:15:51,170 --> 00:15:54,770
原谅我没法为你高兴
{\3c&H000000&\fs30}Forgive me if I'm having trouble being happy for you.

395
00:15:54,770 --> 00:15:59,340
别傻了 你惨兮兮的样子真讨人喜欢
{\3c&H000000&\fs30}Don't be silly, I'm loving your pain.

396
00:16:02,250 --> 00:16:04,510
以后我们的婚姻状态就是这个样子了吗
{\3c&H000000&\fs30}Is this how our marriage is going to be?

397
00:16:04,520 --> 00:16:06,120
有时人们会对她的事情更感兴趣
{\3c&H000000&\fs30}Sometimes people will be more interested

398
00:16:06,120 --> 00:16:08,280
多过于我么
{\3c&H000000&\fs30}in talking to her than to me?

399
00:16:08,290 --> 00:16:11,250
你现在在浴室里吗
{\3c&H000000&\fs30}Are you sitting in a bathroom?

400
00:16:11,260 --> 00:16:13,320
是的
{\3c&H000000&\fs30}Yes.

401
00:16:13,330 --> 00:16:14,920
我需要气冲冲的时候可以去的地方
{\3c&H000000&\fs30}I needed a place to storm off to

402
00:16:14,930 --> 00:16:17,390
只有这个屋能容得下我
{\3c&H000000&\fs30}and it was all that was available.

403
00:16:17,400 --> 00:16:18,660
好吧
{\3c&H000000&\fs30}Fine.

404
00:16:18,660 --> 00:16:22,970
但如果听到冲厕所的声音 我就不和你聊了
{\3c&H000000&\fs30}But if I hear a flush, this conversation is over.

405
00:16:22,970 --> 00:16:26,340
那些人都有着过人的智慧
{\3c&H000000&\fs30}Those people were in the presence of a world-class mind,

406
00:16:26,340 --> 00:16:30,010
但却只顾着聊一些他们自己的有的没的
{\3c&H000000&\fs30}and all they wanted to talk about was their own nonsense.

407
00:16:30,010 --> 00:16:33,740
你听出来讽刺的地方了吗
{\3c&H000000&\fs30}Can you see the irony in that statement?

408
00:16:35,580 --> 00:16:37,780
现在呢
{\3c&H000000&\fs30}How about now?

409
00:16:39,720 --> 00:16:41,920
现在呢
{\3c&H000000&\fs30}How about now?

410
00:16:44,960 --> 00:16:46,320
我等着
{\3c&H000000&\fs30}I'll wait.

411
00:16:49,800 --> 00:16:51,160
惊喜
{\3c&H000000&\fs30}Surprise.

412
00:16:51,160 --> 00:16:53,900
擦 今天是我们结婚纪念日吗
{\3c&H000000&\fs30}Oh, crap, is it our anniversary?

413
00:16:53,900 --> 00:16:55,230
不是
{\3c&H000000&\fs30}No.

414
00:16:55,230 --> 00:16:57,430
等一下
{\3c&H000000&\fs30}Wait.

415
00:16:59,570 --> 00:17:02,010
不是
{\3c&H000000&\fs30}No.

416
00:17:02,010 --> 00:17:03,810
好吧 那这是在庆祝什么
{\3c&H000000&\fs30}All right, so what are we celebrating?

417
00:17:03,810 --> 00:17:06,380
伯纳黛特和霍华德喜得二宝
{\3c&H000000&\fs30}Well, you know, Bernadette and Howard are pregnant again,

418
00:17:06,380 --> 00:17:08,140
而且艾米和谢尔顿要结婚了
{\3c&H000000&\fs30}and Amy and Sheldon are getting married.

419
00:17:08,150 --> 00:17:09,810
我不想你觉得自己受到冷落了
{\3c&H000000&\fs30}I didn't want you to feel left out.

420
00:17:09,820 --> 00:17:11,680
什么 受到冷落吗
{\3c&H000000&\fs30}Ah. Left out?

421
00:17:11,680 --> 00:17:13,720
呃 伯纳黛特肚子里要长出一个宝宝
{\3c&H000000&\fs30}Well, Bernadette has to grow a baby inside of her,

422
00:17:13,720 --> 00:17:15,020
而艾米将要嫁给一个宝宝
{\3c&H000000&\fs30}and Amy has to marry one.

423
00:17:15,020 --> 00:17:16,950
老娘的日子好着呢
{\3c&H000000&\fs30}My life is great.

424
00:17:18,520 --> 00:17:20,090
那你还要不要蛋糕
{\3c&H000000&\fs30}So do you not want the cake?

425
00:17:20,090 --> 00:17:22,930
你把它拿走试试
{\3c&H000000&\fs30}Try and take it away, see what happens.

426
00:17:24,900 --> 00:17:28,460
我去 今天还真是我们的结婚纪念日
{\3c&H000000&\fs30}Oh, crap, it is our anniversary.

427
00:17:28,470 --> 00:17:31,430
结婚纪念日快乐
{\3c&H000000&\fs30}Happy anniversary!

428
00:17:33,740 --> 00:17:35,470
艾米
{\3c&H000000&\fs30}Amy.

429
00:17:35,470 --> 00:17:39,880
我有些话必须跟你说
{\3c&H000000&\fs30}There's something I need to say to you.

430
00:17:39,880 --> 00:17:41,040
我听着呢
{\3c&H000000&\fs30}I'm listening.

424
00:17:18,520 --> 00:17:20,090
那你还要不要蛋糕
{\3c&H000000&\fs30}So do you not want the cake?

425
00:17:20,090 --> 00:17:22,930
你把它拿走试试
{\3c&H000000&\fs30}Try and take it away, see what happens.

426
00:17:24,900 --> 00:17:28,460
我去 今天还真是我们的结婚纪念日
{\3c&H000000&\fs30}Oh, crap, it is our anniversary.

427
00:17:28,470 --> 00:17:31,430
结婚纪念日快乐
{\3c&H000000&\fs30}Happy anniversary!

428
00:17:33,740 --> 00:17:35,470
艾米
{\3c&H000000&\fs30}Amy.

429
00:17:35,470 --> 00:17:39,880
我有些话必须跟你说
{\3c&H000000&\fs30}There's something I need to say to you.

430
00:17:39,880 --> 00:17:41,040
我听着呢
{\3c&H000000&\fs30}I'm listening.