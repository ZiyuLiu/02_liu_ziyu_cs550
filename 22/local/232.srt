﻿50
00:00:39,500 --> 00:00:41,670
艾米 艾米
{\3c&H000000&\fs30}Amy? Amy.

51
00:00:41,670 --> 00:00:43,540
艾米
{\3c&H000000&\fs30}Amy?

52
00:00:56,750 --> 00:00:57,950
你愿意嫁给我吗
{\3c&H000000&\fs30}Will you marry me?

53
00:01:01,290 --> 00:01:03,920
稍等片刻
{\3c&H000000&\fs30}One moment, please.

54
00:01:03,930 --> 00:01:06,330
你逗我吧 非要现在接电话吗
{\3c&H000000&\fs30}Really, you're going to answer that right now?

55
00:01:06,330 --> 00:01:07,530
是莱纳德
{\3c&H000000&\fs30}It's Leonard.

56
00:01:07,530 --> 00:01:10,060
我不想失礼
{\3c&H000000&\fs30}I don't want to be rude.

57
00:01:10,070 --> 00:01:10,830
喂
{\3c&H000000&\fs30}Hello?

58
00:01:10,830 --> 00:01:11,870
嘿 你去哪儿了
{\3c&H000000&\fs30}Oh, hey, where you been?

59
00:01:11,870 --> 00:01:13,200
我们给你打了几个小时电话了
{\3c&H000000&\fs30}We've been calling you for hours.

60
00:01:13,200 --> 00:01:16,140
啊 抱歉 我的电话处于飞行模式
{\3c&H000000&\fs30}Oh, I'm sorry, my phone was on "Airplane" Mode.

61
00:01:16,140 --> 00:01:17,100
为什么
{\3c&H000000&\fs30}Why?

62
00:01:17,110 --> 00:01:21,110
因为我刚在飞行啊
{\3c&H000000&\fs30}Because I was on an airplane.

63
00:01:21,110 --> 00:01:22,580
嘿 开免提
{\3c&H000000&\fs30}Hey, put him on speaker.

64
00:01:22,580 --> 00:01:24,340
-嗯 -嘿 你在哪儿
{\3c&H000000&\fs30}- Yeah. - Hey, where are you?

65
00:01:24,350 --> 00:01:26,950
我来普林斯顿看艾米
{\3c&H000000&\fs30}I came to Princeton to see Amy.

66
00:01:26,950 --> 00:01:28,550
说起来 还挺好笑的
{\3c&H000000&\fs30}It's a funny story, actually.

67
00:01:28,550 --> 00:01:31,180
我当时正和诺维茨基博士一起吃午饭
{\3c&H000000&\fs30}I was having lunch with Dr. Nowitzki,

68
00:01:31,190 --> 00:01:32,550
然后她亲了我
{\3c&H000000&\fs30}and she kissed me.

69
00:01:32,550 --> 00:01:33,650
-啥 -神马
{\3c&H000000&\fs30}- Excuse me? - What?

70
00:01:33,660 --> 00:01:34,850
纳尼
{\3c&H000000&\fs30}I'm sorry?

71
00:01:34,860 --> 00:01:37,990
那一刻我意识到
{\3c&H000000&\fs30}And in that moment, I realized

72
00:01:37,990 --> 00:01:41,360
艾米才是我余生唯一
{\3c&H000000&\fs30}that Amy was the only woman I ever wanted to kiss

73
00:01:41,360 --> 00:01:43,160
想吻的女人
{\3c&H000000&\fs30}for the rest of my life.

74
00:01:43,170 --> 00:01:46,630
所以我来新泽西向她求婚
{\3c&H000000&\fs30}So I came to New Jersey to ask her to marry me.

75
00:01:46,640 --> 00:01:48,440
喔 好感人哦
{\3c&H000000&\fs30}Oh, that's so sweet.

76
00:01:48,440 --> 00:01:50,740
-谢尔顿 -对的 不过
{\3c&H000000&\fs30}- Sheldon... - Yeah, although

77
00:01:50,740 --> 00:01:53,940
我需要先得到一个人的祝福
{\3c&H000000&\fs30}there was one man whose blessing I needed first.

78
00:01:53,940 --> 00:01:57,810
我已经想好了 我想要和艾米
{\3c&H000000&\fs30}I've thought about it, and I really want to spend

79
00:01:57,810 --> 00:02:00,310
一起共度余生
{\3c&H000000&\fs30}the rest of my life with Amy.

80
00:02:00,320 --> 00:02:03,050
我能得到你的祝福吗
{\3c&H000000&\fs30}Do I have your blessing?

81
00:02:03,050 --> 00:02:05,320
呃 谢尔顿
{\3c&H000000&\fs30}Well, Sheldon...

82
00:02:06,590 --> 00:02:09,560
我觉得你应该让她的手指像土星那样
{\3c&H000000&\fs30}...I think you should make her finger like Saturn

83
00:02:09,560 --> 00:02:12,490
被光环（戒指）围绕
{\3c&H000000&\fs30}and put a ring on it.

84
00:02:13,500 --> 00:02:16,500
你问了斯蒂芬·霍金而不是她爸吗
{\3c&H000000&\fs30}You asked Stephen Hawking and not her father?

85
00:02:16,500 --> 00:02:17,960
斯蒂芬·霍金是个天才
{\3c&H000000&\fs30}Stephen Hawking's a genius.

86
00:02:17,970 --> 00:02:22,200
如果他说不 我就不会在她爸身上浪费时间了
{\3c&H000000&\fs30}If he said no, I wasn't gonna waste my time on her father.

87
00:02:23,040 --> 00:02:25,110
所以你问过我爸了吗
{\3c&H000000&\fs30}But you did ask my father?

88
00:02:25,110 --> 00:02:27,840
是的 他说可以
{\3c&H000000&\fs30}I did. He said yes.

89
00:02:27,840 --> 00:02:30,610
可惜不是用机器人声效 没那么吊
{\3c&H000000&\fs30}Although, not in a robot voice, so it wasn't nearly as cool.

90
00:02:30,610 --> 00:02:32,950
好吧 天啊 简直不敢相信你俩订婚了
{\3c&H000000&\fs30}Okay. Oh, my God, I can't believe you guys are engaged.

91
00:02:32,950 --> 00:02:34,450
我们没还没订婚呢
{\3c&H000000&\fs30}We're not engaged, yet.

92
00:02:34,450 --> 00:02:36,050
她那么长时间了还没答应
{\3c&H000000&\fs30}She's taking forever to answer.

93
00:02:36,050 --> 00:02:38,250
因为你在打电话
{\3c&H000000&\fs30}Because you're on the phone!

94
00:02:40,220 --> 00:02:42,020
我们晚点说
{\3c&H000000&\fs30}We'll call you back.

95
00:02:47,100 --> 00:02:48,130
她说愿意
{\3c&H000000&\fs30}She said yes.

96
00:02:48,130 --> 00:02:50,000
耶 恭喜你们
{\3c&H000000&\fs30}Yay! Congratulations!

97
00:03:21,790 --> 00:03:23,820
妈 我有好消息要告诉你
{\3c&H000000&\fs30}Mother, I have some good news to share.

98
00:03:23,820 --> 00:03:25,690
我们订婚了
{\3c&H000000&\fs30}We're engaged.

99
00:03:27,090 --> 00:03:30,160
我真为你俩高兴 但我一点也不吃惊
{\3c&H000000&\fs30}I am so happy for you two, but I'm not surprised.

100
00:03:30,160 --> 00:03:31,560
因为我为此祈祷很久了
{\3c&H000000&\fs30}I've been praying for this.