﻿394
00:16:38,980 --> 00:16:41,390
今天我們歡聚一堂 共祝愛情
We're here today to celebrate love.

395
00:16:44,570 --> 00:16:46,820
嘆大聲點兒 聽不清
Sigh louder, no one heard you.

396
00:16:48,820 --> 00:16:50,650
說真的 我願意換座兒
Really, I can move.

397
00:16:51,990 --> 00:16:53,870
不僅祝福萊納德和佩妮的真愛
Not just Leonard and Penny's love, but the love

398
00:16:53,870 --> 00:16:56,160
還有我們對他們的喜愛 以及大家之間的關愛
we have for them, as well as each other.

399
00:16:56,160 --> 00:17:00,500
說到愛 老年人性病比率飛漲
Speaking of love, STDs among the elderly are skyrocketing.

400
00:17:03,330 --> 00:17:05,500
愛就是耐心 但絕不能容忍
Love is patient, but it's not gonna put up

401
00:17:05,500 --> 00:17:08,170
有人在下面竊竊私語 所以趕快收嘴吧
with all the side chatter, so let's knock it off!

402
00:17:09,840 --> 00:17:12,090
至少這次沒有對我吼了
At least she's yelling at someone else for a change.

403
00:17:12,090 --> 00:17:14,310
霍華德
Howard!

404
00:17:18,180 --> 00:17:19,900
好吧 我知道今天大家都有點情緒
Okay, I understand everyone's a little tense today,

405
00:17:19,900 --> 00:17:22,400
所以我直接切入主題了
so I am just gonna get to the important stuff.

406
00:17:22,400 --> 00:17:25,740
萊納德 今天當著家人和朋友
Leonard, standing here with you in front

407
00:17:25,740 --> 00:17:29,660
站在你面前 讓我感慨萬千
of our family and friends is bringing up a lot of feelings.

408
00:17:30,690 --> 00:17:33,750
比如我們當初私奔 是多麼的明智
Like what a good idea it was to elope the first time.

409
00:17:35,620 --> 00:17:39,120
以及 一路走來你讓我多麼的幸福
But also how incredibly happy you make me.

410
00:17:40,450 --> 00:17:42,200
謝謝你娶我
Thank you for marrying me.

411
00:17:42,210 --> 00:17:44,040
希望這是最後一次了
Hopefully for the last time.

412
00:17:47,010 --> 00:17:48,760
佩妮
Penny...

413
00:17:48,760 --> 00:17:52,430
作為一名科學家 我的工作就是搞清楚為什麼
as a scientist, my job is to figure out why things happen.

414
00:17:52,430 --> 00:17:55,630
但我覺得我永遠也搞不清楚
But I don't think I'll ever understand

415
00:17:55,640 --> 00:18:00,140
這樣的我為什麼可以和這樣的你在一起
how someone like me could get to be with someone like you.

416
00:18:01,230 --> 00:18:04,230
也許 我不需要搞清楚
You know... maybe... I don't need to understand it,

417
00:18:04,230 --> 00:18:05,310
我只需要心存感激
I just need to be grateful.

418
00:18:05,310 --> 00:18:06,480
我愛你 佩妮
I love you, Penny.

419
00:18:06,480 --> 00:18:08,280
噢
Oh...

420
00:18:09,900 --> 00:18:12,870
誰想表達反對意見嗎
Anybody have anything snarky to say about that?

421
00:18:14,910 --> 00:18:16,960
諒你們也不敢
Didn't think so.

422
00:18:17,910 --> 00:18:19,540
我想多說幾句
I'd like to say something.

423
00:18:20,740 --> 00:18:23,380
貝弗利 我知道
Beverly, I know that we don't bring out

424
00:18:23,380 --> 00:18:24,750
我們的感情不是最好的
the best in each other.

425
00:18:24,750 --> 00:18:28,920
但我們的感情卻析出了美妙的結晶
But something wonderful did come from our relationship:

426
00:18:28,920 --> 00:18:30,220
就是台上這位年輕人
that young man right there.

427
00:18:31,260 --> 00:18:33,090
完全同意
I couldn't agree more.

428
00:18:35,390 --> 00:18:37,680
簡直感人
That's beautiful.

429
00:18:39,730 --> 00:18:42,850
謝謝 好了 我們繼續
Thank you. All right, let's continue.

430
00:18:42,850 --> 00:18:45,770
恕我冒昧 我也想說幾句
Yeah, excuse me, I need to say something

431
00:18:45,770 --> 00:18:48,770
對這位生命中特殊的人
to someone pretty special, and...

432
00:18:48,770 --> 00:18:50,910
我不想再等了
I just can't wait any longer.

433
00:18:50,910 --> 00:18:52,610
夢果然能成真
It's happening.

434
00:18:52,610 --> 00:18:54,580
萊納德
Leonard...

435
00:18:58,280 --> 00:19:00,780
我們之間有笑有淚
You and I have our ups and downs.

436
00:19:00,780 --> 00:19:04,170
但我永遠都當你是我的親人
But I have always considered you my family.

437
00:19:04,170 --> 00:19:08,090
不管我們父母是否在搞七捻三
Even before the recent threat of our parents fornicating

438
00:19:08,090 --> 00:19:09,930
像兩隻皺巴巴的老兔子
like wrinkly old rabbits.

439
00:19:14,600 --> 00:19:18,880
我很少表露 但你們對我無比重要
I don't always show it, but you are of great importance to me.

440
00:19:20,190 --> 00:19:21,890
你們兩個人
Both of you.

441
00:19:21,890 --> 00:19:23,470
噢
Oh...

442
00:19:23,470 --> 00:19:25,020
謝謝你
Thank you.

443
00:19:25,030 --> 00:19:26,520
贊
Okay.

444
00:19:26,530 --> 00:19:30,950
我鄭重宣佈 你們倆結為夫婦
I now pronounce you husband and wife.

445
00:19:30,950 --> 00:19:33,450
以及家裏另一位怪怪的小丈夫
And weird other husband who came with the apartment.

446
00:19:45,230 --> 00:19:47,480
謝謝你送我們去機場
Thank you for taking us to the airport.

447
00:19:47,480 --> 00:19:50,410
我們仨在一起的時光 我很開心
Hey, I'm just thrilled we're all getting along for a minute.

448
00:19:50,420 --> 00:19:51,550
我也是
Yeah, me, too.

449
00:19:51,550 --> 00:19:54,200
貝弗利 給你帶來困擾 我很抱歉
Beverly, I'm sorry if I upset you.

450
00:19:54,200 --> 00:19:55,850
覆水難收 埃弗瑞德
Water under the bridge, Alfred.

451
00:19:57,190 --> 00:19:59,360
萊納德 怎麼還不轉到並車道
Leonard, why don't you get into the carpool lane?

452
00:19:59,360 --> 00:20:01,270
現在還是實線 還不能轉
Well, that's a solid line. He can't cross it.

453
00:20:01,280 --> 00:20:02,610
沒事啦 我這就轉
That's okay. I can make it over.

454
00:20:02,610 --> 00:20:03,990
不不 我們還是慢慢排吧
No, no, let's plod along.

455
00:20:04,000 --> 00:20:06,280
這樣你老爹心裏舒服點
It'll make your father feel more comfortable.

456
00:20:07,830 --> 00:20:09,780
真正讓我舒服的是 明天一早起來
What makes me comfortable is knowing I don't have to

457
00:20:09,790 --> 00:20:12,400
我可以不用看到你的臭臉
wake up tomorrow morning and see your sour face.

458
00:20:14,430 --> 00:20:16,670
行行好吧 明早你一覺別醒
Do the world a favor, and don't wake up tomorrow morning.

459
00:20:20,300 --> 00:20:21,990
真是開心的時光啊
That was almost a minute.

460
00:20:24,820 --> 00:20:27,160
挺堵的 不會來不及吧
There's a lot of traffic. Are we gonna be okay?

461
00:20:27,330 --> 00:20:29,180
我們能在起飛前一個小時到機場
You'll be at the airport an hour before your flight.

462
00:20:29,210 --> 00:20:30,050
很好 謝謝
Good. Thank you.

463
00:20:30,070 --> 00:20:33,330
你有足夠時間再勾搭一個老男人
Plenty of time for you to meet another geriatric boy toy.

464
00:20:35,670 --> 00:20:36,660
嘿
Hey.

465
00:20:36,680 --> 00:20:38,810
我不允許你對我無禮
I will not have you be disrespectful to me.

466
00:20:38,830 --> 00:20:39,900
遵命 夫人
Yes ma'am.

467
00:20:40,330 --> 00:20:42,090
謝爾頓 你媽媽是位有魅力的女性
Sheldon, you're mother's an attractive woman.

468
00:20:42,290 --> 00:20:43,590
你要習慣這個事實
You need to get use to the fact

469
00:20:43,660 --> 00:20:45,310
男人們肯定會對她產生興趣
that men are going to be interest in her.

470
00:20:45,340 --> 00:20:47,970
你要開你自己的車 管你自己的事
Well.. and you need to drive the car and mind your business.

471
00:20:49,450 --> 00:20:51,590
我不允許你對我無禮
I will not have you be disrespectful to me.

472
00:20:51,610 --> 00:20:52,640
神馬 你又不是我媽
What-- you're not my mother.

473
00:20:52,660 --> 00:20:54,240
我不允許你對她無禮
Don't you be disrespectful to her.

474
00:20:54,270 --> 00:20:55,270
遵命 夫人
Yes ma'am.

475
00:20:56,900 --> 00:20:57,780
你會學會的
You'll get there.

476
00:20:57,810 --> 00:20:59,950
只要找到竅門就可以了
You've just gotta put some zing on it.

477
00:21:04,070 --> 00:21:06,380
佩妮 我不知道以前我都在擔心些什麼
Penny, I don't know what I was worried about.

478
00:21:06,610 --> 00:21:08,060
你的朋友們太可愛了
You're friends are just lovely.

479
00:21:08,080 --> 00:21:09,630
謝謝 媽媽
Oh, thanks, Mom.

480
00:21:09,800 --> 00:21:12,390
雖然那位謝爾頓稍微有點特別
Although that Sheldon is a bit peculiar.

481
00:21:14,140 --> 00:21:16,270
是嗎 我從來沒發現
Is he? I never noticed.

482
00:21:17,430 --> 00:21:19,500
他讓我想起了我們養過的那只火雞
He reminds me of that turkey we had

483
00:21:19,500 --> 00:21:21,560
抬頭看雨的時候淹死了
who drowned looking up at the rain.

484
00:21:24,670 --> 00:21:26,820
警察警察 冷靜
Cops, cops, be cool!