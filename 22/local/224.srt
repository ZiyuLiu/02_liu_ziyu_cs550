﻿12
00:02:10,690 --> 00:02:13,970
宇宙最初並不大 高溫高密亂如麻
{\3c&H000000&\fs30}{\3c&H000000&\fs30}Our whole universe was in a hot dense state,

13
00:02:13,970 --> 00:02:17,900
一百四十億年前 突如其來大爆炸 等一下
{\3c&H000000&\fs30}{\3c&H000000&\fs30}Then nearly fourteen billion years ago expansion started. Wait.

14
00:02:17,970 --> 00:02:19,320
地球開始降溫啦
{\3c&H000000&\fs30}{\3c&H000000&\fs30}The Earth began to cool,

15
00:02:19,320 --> 00:02:20,740
自養生物霸天下
{\3c&H000000&\fs30}{\3c&H000000&\fs30}The autotrophs began to drool,

16
00:02:20,740 --> 00:02:22,330
穴居人民有文化
{\3c&H000000&\fs30}{\3c&H000000&\fs30}Neanderthals developed tools,

17
00:02:22,330 --> 00:02:24,200
我們建造古長城 我們建造金字塔
{\3c&H000000&\fs30}{\3c&H000000&\fs30}We built a wall, we built the pyramids,

18
00:02:24,200 --> 00:02:27,460
數學科學和歷史 揭開神秘的面紗
{\3c&H000000&\fs30}{\3c&H000000&\fs30}Math, science, history, unraveling the mysteries,

19
00:02:27,460 --> 00:02:29,000
世間之萬物 源於大爆炸
{\3c&H000000&\fs30}{\3c&H000000&\fs30}That all started with the big bang!

20
00:02:29,190 --> 00:02:30,490
{\an8}生活大爆炸
第十季   第一集

21
00:14:14,370 --> 00:14:17,160
{\an8}曼哈頓計劃：美國著名的原子彈計劃，領導者是物理學家羅伯特·奧本海默

22
00:00:00,760 --> 00:00:02,160
生活大爆炸前情提要
Previously on The Big Bang Theory...

23
00:00:02,270 --> 00:00:04,080
老爸 這是謝爾頓的媽媽 瑪麗
Dad, this is Sheldon's mother, Mary.

24
00:00:04,260 --> 00:00:04,960
你好
How do you do?

25
00:00:04,960 --> 00:00:06,180
見到你真高興
Nice to meet you.

26
00:00:06,180 --> 00:00:07,210
當然 還有媽媽
And, of course, Mom.

27
00:00:07,210 --> 00:00:09,380
你好 惡毒潑婦
Hello, my hateful shrew.

28
00:00:09,380 --> 00:00:12,970
你好 糟老混蛋
Hello to you, you wrinkled old bastard.

29
00:00:15,140 --> 00:00:16,300
嘿
Hey.

30
00:00:16,300 --> 00:00:19,110
我剛收到一封美國空軍的信耶
Just got an e-mail from the U.S. Air Force.

31
00:00:19,110 --> 00:00:23,190
我在想如果我回復了 會被他們知道我收到了
I'm just afraid if I respond, then they'll know I got it.

32
00:00:23,200 --> 00:00:25,700
兄弟 你打開這封郵件的時候 他們就已經知道了
Dude, the minute you opened that e-mail, they knew you got it.

33
00:00:25,700 --> 00:00:26,610
我的意思是 他們現在可能利用
I mean, they're probably looking at you

34
00:00:26,620 --> 00:00:29,570
電腦攝像頭看著你
through the camera right now.

35
00:00:29,570 --> 00:00:31,320
哦 天啦
Oh, God.

36
00:00:31,320 --> 00:00:33,540
我覺得有人跟著我們
I think someone's following us.

37
00:00:34,910 --> 00:00:37,210
在這左轉 看他們是不是還跟著
Uh, turn left here and see if he turns with us.

38
00:00:37,210 --> 00:00:39,240
為什麼他在這左轉 酒店在另一邊
Why is he turning here? The restaurant's the other way.

39
00:00:39,240 --> 00:00:41,410
我不知道 他在用那個交通路況的軟體
I don't know. He uses that traffic app.

40
00:00:41,410 --> 00:00:42,750
可能那邊出了事故
Maybe there's an accident.

41
00:00:42,750 --> 00:00:44,330
哦 那跟著他吧
Oh, so follow him.

42
00:00:44,330 --> 00:00:47,220
哦 不
Oh, no!

43
00:00:48,050 --> 00:00:49,140
萊納德 如果你不介意的話
Leonard, if you don't mind,

44
00:00:49,140 --> 00:00:50,390
我有點累了
I think I'm a little tired.

45
00:00:50,390 --> 00:00:51,470
今晚就到此結束吧
I'm gonna call it a night.

46
00:00:51,470 --> 00:00:52,670
當然 爸爸
Sure, Dad.

47
00:00:52,670 --> 00:00:54,420
我也有點累了
I'm a little tuckered out myself.

48
00:00:54,430 --> 00:00:57,640
我們明天早上見
Well, I will see you all in the morning.

49
00:00:57,650 --> 00:00:59,100
那我們一起坐計程車吧
Would you like to share a cab?

50
00:00:59,100 --> 00:01:00,180
那好的
That would be fine.

51
00:01:00,180 --> 00:01:01,930
-你住哪兒 -威斯汀酒店
Where are you staying? I'm at the Westin.

52
00:01:01,930 --> 00:01:03,020
我也是
Well, so am I.

53
00:01:03,020 --> 00:01:04,400
要不要睡前小喝一杯
Could I interest you in a nightcap?

54
00:01:04,400 --> 00:01:06,270
我覺得挺好的
I think that you could.

55
00:01:10,940 --> 00:01:14,280
萊納德
Leonard?

56
00:01:14,280 --> 00:01:16,610
萊納德
Leonard?

57
00:01:16,620 --> 00:01:17,830
幹嘛
What?

58
00:01:17,830 --> 00:01:21,170
我覺得我和你能成為兄弟
You realize you and I could become brothers.

59
00:01:22,040 --> 00:01:23,950
我們不會成為兄弟
We're not gonna be brothers.

60
00:01:23,960 --> 00:01:25,420
也不會成為繼兄弟
We're not gonna be stepbrothers.

61
00:01:25,420 --> 00:01:26,960
快去睡覺
Go to sleep.

62
00:01:26,960 --> 00:01:28,960
希望你是對的
I hope you're right.