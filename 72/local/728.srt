﻿310
00:12:32,730 --> 00:12:34,060
很好 怀特
Very nice, Wyatt.

311
00:12:34,060 --> 00:12:36,700
你还想不通这一个怎么会变成今天这个样子的
And you wonder why this one turned out the way he did.

312
00:12:36,700 --> 00:12:39,030
你看到了我每天都要忍受什么了吗
You see what I've gotta put up with!

313
00:12:39,040 --> 00:12:41,040
你每天都要忍受什么了
What you've gotta put up with?

314
00:12:41,040 --> 00:12:42,740
你难道非要去蹲监狱吗
Why did you have to go to jail?

315
00:12:42,740 --> 00:12:45,960
那是一不小心被捕了 老妈
It's called getting caught, Mother!

316
00:12:45,960 --> 00:12:47,660
哈啰
Hello!

317
00:12:47,660 --> 00:12:48,830
-嗨 他来了 -嗨
- Hey, there he is! - Hey!

318
00:12:48,830 --> 00:12:51,750
莱纳德 真高兴能再见到你
Oh, Leonard! It's so nice to see you again!

319
00:12:51,750 --> 00:12:52,830
噢 我也是
Oh, you, too!

320
00:12:52,830 --> 00:12:55,300
嗨 大家 这是我妈 贝弗利
Hey, everyone, this is my mother, Beverly.

321
00:12:55,300 --> 00:12:56,420
-你好 -你好啊
- Hello. - Hi.

322
00:12:56,420 --> 00:12:59,640
我们不是白人垃圾哦
We are not white trash!

323
00:13:03,890 --> 00:13:06,310
你儿子就要步入婚礼殿堂了 你开心吗
Are you excited to see your son walk down the aisle?

324
00:13:06,310 --> 00:13:07,430
对 很开心
Yes, I am.

325
00:13:07,430 --> 00:13:08,650
只是我给他带来那么多烦恼
I'm just feeling a little guilty

326
00:13:08,650 --> 00:13:10,430
我感到很愧疚
about all the trouble I've caused.

327
00:13:10,430 --> 00:13:12,100
噢 我也是
Oh, so am I.

328
00:13:12,100 --> 00:13:15,600
你给上帝都带来烦恼了 妈妈
You made God sad today, Mom.

329
00:13:22,580 --> 00:13:24,250
谢尔顿 他们又没做错什么
Sheldon, they haven't done anything wrong.

330
00:13:24,250 --> 00:13:26,780
他们能合得来 我觉得也挺好呀
I think it's nice they're hitting it off.

331
00:13:26,780 --> 00:13:28,450
那也没有必要发展得这么迅速
Well, that's still no reason to rush into anything.

332
00:13:28,450 --> 00:13:29,530
看看我们
Look at us.

333
00:13:29,540 --> 00:13:31,250
我们就慢得荡气回肠
We took things remarkably slow.

334
00:13:31,260 --> 00:13:34,090
你和我 前两年连手都没牵过
You and I, we didn't even hold hands for two years.

335
00:13:35,260 --> 00:13:38,290
真实故事比你听到的要浪漫得多
It was a lot hotter than it sounds.

336
00:13:38,300 --> 00:13:39,600
你是一个耐心的好姑娘
You're a patient young lady.

337
00:13:39,600 --> 00:13:40,630
嘿嘿
Hey, hey!

338
00:13:40,630 --> 00:13:42,800
她是我的 你矜持一点 老爷爷
She's mine! Take a cold shower, grandpa!

339
00:13:46,940 --> 00:13:49,190
那位上校为什么不说要开什么会
Why wouldn't that colonel say what the meeting's about?

340
00:13:49,190 --> 00:13:50,720
一定是坏消息
It has to be bad news.

341
00:13:50,720 --> 00:13:52,980
冷静一点好吗 不要想太多了
Calm down, okay? Try not to think about it.

342
00:13:52,980 --> 00:13:55,530
你这个建议好蠢
That's really stupid advice.

343
00:13:55,530 --> 00:13:57,360
这句话很伤你造吗
You know that hurts my feelings.

344
00:13:58,200 --> 00:14:00,530
冷静一点 不要想太多了
Calm down, try not to think about it.

345
00:14:01,570 --> 00:14:03,120
也行
Okay.

346
00:14:04,710 --> 00:14:06,320
和你聊天简直自讨没趣
Why do I bother talking to you?

347
00:14:06,320 --> 00:14:08,990
拜托 结果再坏能坏到哪里去呢
Oh, come on. What's the worst that could come of this meeting?

348
00:14:08,990 --> 00:14:11,130
我不知道 也许他们会抢走发明
I don't know. They take the invention away,

349
00:14:11,130 --> 00:14:12,290
我却一无所获
and I get nothing?

350
00:14:12,300 --> 00:14:14,330
好吧 那不是最坏的情况
Okay, that's not so bad.

310
00:12:32,730 --> 00:12:34,060
很好 怀特
Very nice, Wyatt.

311
00:12:34,060 --> 00:12:36,700
你还想不通这一个怎么会变成今天这个样子的
And you wonder why this one turned out the way he did.

312
00:12:36,700 --> 00:12:39,030
你看到了我每天都要忍受什么了吗
You see what I've gotta put up with!

313
00:12:39,040 --> 00:12:41,040
你每天都要忍受什么了
What you've gotta put up with?

314
00:12:41,040 --> 00:12:42,740
你难道非要去蹲监狱吗
Why did you have to go to jail?

315
00:12:42,740 --> 00:12:45,960
那是一不小心被捕了 老妈
It's called getting caught, Mother!

316
00:12:45,960 --> 00:12:47,660
哈啰
Hello!

317
00:12:47,660 --> 00:12:48,830
-嗨 他来了 -嗨
- Hey, there he is! - Hey!

318
00:12:48,830 --> 00:12:51,750
莱纳德 真高兴能再见到你
Oh, Leonard! It's so nice to see you again!

319
00:12:51,750 --> 00:12:52,830
噢 我也是
Oh, you, too!

320
00:12:52,830 --> 00:12:55,300
嗨 大家 这是我妈 贝弗利
Hey, everyone, this is my mother, Beverly.

321
00:12:55,300 --> 00:12:56,420
-你好 -你好啊
- Hello. - Hi.

322
00:12:56,420 --> 00:12:59,640
我们不是白人垃圾哦
We are not white trash!

323
00:13:03,890 --> 00:13:06,310
你儿子就要步入婚礼殿堂了 你开心吗
Are you excited to see your son walk down the aisle?

324
00:13:06,310 --> 00:13:07,430
对 很开心
Yes, I am.

325
00:13:07,430 --> 00:13:08,650
只是我给他带来那么多烦恼
I'm just feeling a little guilty

326
00:13:08,650 --> 00:13:10,430
我感到很愧疚
about all the trouble I've caused.

327
00:13:10,430 --> 00:13:12,100
噢 我也是
Oh, so am I.

328
00:13:12,100 --> 00:13:15,600
你给上帝都带来烦恼了 妈妈
You made God sad today, Mom.

329
00:13:22,580 --> 00:13:24,250
谢尔顿 他们又没做错什么
Sheldon, they haven't done anything wrong.

330
00:13:24,250 --> 00:13:26,780
他们能合得来 我觉得也挺好呀
I think it's nice they're hitting it off.

331
00:13:26,780 --> 00:13:28,450
那也没有必要发展得这么迅速
Well, that's still no reason to rush into anything.

332
00:13:28,450 --> 00:13:29,530
看看我们
Look at us.

333
00:13:29,540 --> 00:13:31,250
我们就慢得荡气回肠
We took things remarkably slow.

334
00:13:31,260 --> 00:13:34,090
你和我 前两年连手都没牵过
You and I, we didn't even hold hands for two years.

335
00:13:35,260 --> 00:13:38,290
真实故事比你听到的要浪漫得多
It was a lot hotter than it sounds.

336
00:13:38,300 --> 00:13:39,600
你是一个耐心的好姑娘
You're a patient young lady.

337
00:13:39,600 --> 00:13:40,630
嘿嘿
Hey, hey!

338
00:13:40,630 --> 00:13:42,800
她是我的 你矜持一点 老爷爷
She's mine! Take a cold shower, grandpa!

339
00:13:46,940 --> 00:13:49,190
那位上校为什么不说要开什么会
Why wouldn't that colonel say what the meeting's about?

340
00:13:49,190 --> 00:13:50,720
一定是坏消息
It has to be bad news.

341
00:13:50,720 --> 00:13:52,980
冷静一点好吗 不要想太多了
Calm down, okay? Try not to think about it.

342
00:13:52,980 --> 00:13:55,530
你这个建议好蠢
That's really stupid advice.

343
00:13:55,530 --> 00:13:57,360
这句话很伤你造吗
You know that hurts my feelings.

344
00:13:58,200 --> 00:14:00,530
冷静一点 不要想太多了
Calm down, try not to think about it.

345
00:14:01,570 --> 00:14:03,120
也行
Okay.

346
00:14:04,710 --> 00:14:06,320
和你聊天简直自讨没趣
Why do I bother talking to you?

347
00:14:06,320 --> 00:14:08,990
拜托 结果再坏能坏到哪里去呢
Oh, come on. What's the worst that could come of this meeting?

348
00:14:08,990 --> 00:14:11,130
我不知道 也许他们会抢走发明
I don't know. They take the invention away,

349
00:14:11,130 --> 00:14:12,290
我却一无所获
and I get nothing?

350
00:14:12,300 --> 00:14:14,330
好吧 那不是最坏的情况
Okay, that's not so bad.