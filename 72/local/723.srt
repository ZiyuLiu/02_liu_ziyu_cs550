﻿80
00:02:19,420 --> 00:02:20,840
自养生物霸天下
The autotrophs began to drool,

81
00:02:20,840 --> 00:02:22,430
穴居人民有文化
Neanderthals developed tools,

82
00:02:22,430 --> 00:02:24,300
我们建造古长城 我们建造金字塔
We built a wall, we built the pyramids,

83
00:02:24,300 --> 00:02:27,560
数学科学和历史 揭开神秘的面纱
Math, science, history, unraveling the mysteries,

84
00:02:27,560 --> 00:02:29,100
世间之万物 源於大爆炸
That all started with the big bang!

85
00:02:29,290 --> 00:02:30,590
生活大爆炸
第十季   第一集

86
00:02:39,790 --> 00:02:41,370
好了 我要去接我的家人了
Okay, I'm gonna go pick up my family.

87
00:02:41,370 --> 00:02:43,790
可能要一个半小时或者两个小时 得看交通状况
Like an hour and half, two hours, depending on traffic.

88
00:02:43,790 --> 00:02:44,960
好的 小心开车
Yeah, drive safe. Oh, hey,

89
00:02:44,960 --> 00:02:46,230
答应我一件事 好吗
and do yourself a favor, all right?

90
00:02:46,230 --> 00:02:49,360
贝弗利到这儿时 不要提起昨天晚上的事
When Beverly gets here, do not bring up last night.

91
00:02:49,360 --> 00:02:50,460
好吗 对你们来说
All right? As far as you're concerned,

92
00:02:50,460 --> 00:02:52,660
你们什麽都不知道
you don't know anything,

93
00:02:52,670 --> 00:02:53,680
也什麽都没看到
you didn't see anything.

94
00:02:53,680 --> 00:02:58,140
装傻就好了
I want you just to play dumb.

95
00:02:58,140 --> 00:03:01,190
她在这儿很好的给我们示范了如何装傻
It was nice of her to show us playing dumb with an example.

96
00:03:01,190 --> 00:03:02,610
什麽
What?

97
00:03:02,610 --> 00:03:04,310
喔
Oh.

98
00:03:04,310 --> 00:03:06,480
嗨 对不起 我现在该走了
Hi. Okay, hey there, I got-- I'm sorry, I got to go now.

99
00:03:06,480 --> 00:03:09,400
-佩妮 等等 -为什麽
- Penny, wait. - Why?

100
00:03:09,400 --> 00:03:12,280
我想感谢你们克服重重困难
I wanted to thank you for going through all the trouble

101
00:03:12,290 --> 00:03:15,540
邀请我来参加婚礼
of planning a second wedding ceremony for me,

102
00:03:15,540 --> 00:03:17,710
但很可惜我不能参加
but unfortunately I cannot attend.

103
00:03:17,710 --> 00:03:19,830
为什麽 怎麽了
Well, why? What's wrong?

104
00:03:19,830 --> 00:03:21,460
我们还要继续装傻吗
Wha-- are we still doing the dumb thing?

105
00:03:21,460 --> 00:03:23,550
好的 为什麽 怎麽了
Okay, why, what's wrong?

106
00:03:23,550 --> 00:03:25,710
我只是不能在这儿忍受你爸
I just cannot stay here while your father

107
00:03:25,720 --> 00:03:27,920
故意让我难堪
goes out of his way to humiliate me.

108
00:03:27,920 --> 00:03:31,050
天啊 他怎麽羞辱你了
Oh, golly, however did he humiliate you?

109
00:03:31,060 --> 00:03:32,670
别说了 谢尔顿
Stop it, Sheldon.

110
00:03:32,670 --> 00:03:36,180
我应该问别说什麽 还是不再说话了
Do I say "Stop what?" Or just throw in the towel?

111
00:03:36,180 --> 00:03:38,390
我不明白为什麽我要看着你爸
I don't see why I should have to watch your father

112
00:03:38,400 --> 00:03:41,600
和某个迷信的乡巴佬在一起
parade around with some Bible-thumping bumpkin.

113
00:03:41,600 --> 00:03:43,980
等下 你是在说我妈吗
Oh, excuse me, that is my mother you're talking about,

114
00:03:43,980 --> 00:03:46,190
虽然说得很对
however accurately.

115
00:03:46,190 --> 00:03:47,690
好了 贝弗利
Okay, Beverly,




