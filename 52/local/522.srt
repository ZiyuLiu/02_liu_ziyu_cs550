﻿
19
00:02:27,460 --> 00:02:29,000
世间之万物 源于大爆炸
{\3c&H000000&\fs30}{\3c&H000000&\fs30}That all started with the big bang!

20
00:02:29,190 --> 00:02:30,490
{\an8}生活大爆炸
第十季   第一集

21
00:14:14,370 --> 00:14:17,160
{\an8}曼哈顿计划：美国著名的原子弹计划，领导者是物理学家罗伯特·奥本海默

22
00:00:00,760 --> 00:00:02,160
生活大爆炸前情提要
Previously on The Big Bang Theory...

23
00:00:02,270 --> 00:00:04,080
老爸 这是谢尔顿的妈妈 玛丽
Dad, this is Sheldon's mother, Mary.

24
00:00:04,260 --> 00:00:04,960
你好
How do you do?

25
00:00:04,960 --> 00:00:06,180
见到你真高兴
Nice to meet you.

26
00:00:06,180 --> 00:00:07,210
当然 还有妈妈
And, of course, Mom.

27
00:00:07,210 --> 00:00:09,380
你好 恶毒泼妇
Hello, my hateful shrew.

28
00:00:09,380 --> 00:00:12,970
你好 糟老混蛋
Hello to you, you wrinkled old bastard.

29
00:00:15,140 --> 00:00:16,300
嘿
Hey.

30
00:00:16,300 --> 00:00:19,110
我刚收到一封美国空军的信耶
Just got an e-mail from the U.S. Air Force.

31
00:00:19,110 --> 00:00:23,190
我在想如果我回复了 会被他们知道我收到了
I'm just afraid if I respond, then they'll know I got it.

32
00:00:23,200 --> 00:00:25,700
兄弟 你打开这封邮件的时候 他们就已经知道了
Dude, the minute you opened that e-mail, they knew you got it.

33
00:00:25,700 --> 00:00:26,610
我的意思是 他们现在可能利用
I mean, they're probably looking at you

34
00:00:26,620 --> 00:00:29,570
电脑摄像头看着你
through the camera right now.

35
00:00:29,570 --> 00:00:31,320
哦 天啦
Oh, God.

36
00:00:31,320 --> 00:00:33,540
我觉得有人跟着我们
I think someone's following us.

37
00:00:34,910 --> 00:00:37,210
在这左转 看他们是不是还跟着
Uh, turn left here and see if he turns with us.

38
00:00:37,210 --> 00:00:39,240
为什么他在这左转 酒店在另一边
Why is he turning here? The restaurant's the other way.

39
00:00:39,240 --> 00:00:41,410
我不知道 他在用那个交通路况的软件
I don't know. He uses that traffic app.

40
00:00:41,410 --> 00:00:42,750
可能那边出了事故
Maybe there's an accident.

41
00:00:42,750 --> 00:00:44,330
哦 那跟着他吧
Oh, so follow him.

42
00:00:44,330 --> 00:00:47,220
哦 不
Oh, no!

43
00:00:48,050 --> 00:00:49,140
莱纳德 如果你不介意的话
Leonard, if you don't mind,

44
00:00:49,140 --> 00:00:50,390
我有点累了
I think I'm a little tired.

45
00:00:50,390 --> 00:00:51,470
今晚就到此结束吧
I'm gonna call it a night.

46
00:00:51,470 --> 00:00:52,670
当然 爸爸
Sure, Dad.

47
00:00:52,670 --> 00:00:54,420
我也有点累了
I'm a little tuckered out myself.

48
00:00:54,430 --> 00:00:57,640
我们明天早上见
Well, I will see you all in the morning.

49
00:00:57,650 --> 00:00:59,100
那我们一起坐计程车吧
Would you like to share a cab?

50
00:00:59,100 --> 00:01:00,180
那好的
That would be fine.

51
00:01:00,180 --> 00:01:01,930
-你住哪儿 -威斯汀酒店
Where are you staying? I'm at the Westin.

52
00:01:01,930 --> 00:01:03,020
我也是
Well, so am I.

53
00:01:03,020 --> 00:01:04,400
要不要睡前小喝一杯
Could I interest you in a nightcap?

54
00:01:04,400 --> 00:01:06,270
我觉得挺好的
I think that you could.

55
00:01:10,940 --> 00:01:14,280
莱纳德
Leonard?

56
00:01:14,280 --> 00:01:16,610
莱纳德
Leonard?

57
00:01:16,620 --> 00:01:17,830
干嘛
What?

58
00:01:17,830 --> 00:01:21,170
我觉得我和你能成为兄弟
You realize you and I could become brothers.

59
00:01:22,040 --> 00:01:23,950
我们不会成为兄弟
We're not gonna be brothers.

60
00:01:23,960 --> 00:01:25,420
也不会成为继兄弟
We're not gonna be stepbrothers.

61
00:01:25,420 --> 00:01:26,960
快去睡觉
Go to sleep.

62
00:01:26,960 --> 00:01:28,960
希望你是对的
I hope you're right.

63
00:01:28,960 --> 00:01:30,790
因为一个成年男性和他兄弟
'Cause a grown man living with his brother

64
00:01:30,800 --> 00:01:34,050
还有他兄弟的老婆一起住会很奇怪的
and his brother's wife is weird.

65
00:01:34,050 --> 00:01:36,380
睡觉吧
Go to sleep.

66
00:01:36,380 --> 00:01:38,550
好
Okay.

67
00:01:41,310 --> 00:01:43,220
你认为你爸和我妈现在正在
Do you think your father's doing unspeakable things

68
00:01:43,230 --> 00:01:45,980
做难以启齿的事情吗
to my mother?

69
00:01:45,980 --> 00:01:47,140
没有
No.



