﻿160
00:06:06,390 --> 00:06:08,230
别这么做了
Well, quit it!

161
00:06:10,360 --> 00:06:12,560
兰德尔 不敢相信
So, uh, Randall, can't believe

162
00:06:12,570 --> 00:06:14,030
多年后你终于来看我了
after all these years you finally get to visit me

163
00:06:14,030 --> 00:06:15,370
在加利福尼亚
in California.

164
00:06:15,370 --> 00:06:17,290
幸好我是一个非暴力罪犯
Well, good thing I was a nonviolent offender,

165
00:06:17,290 --> 00:06:19,320
否则我都不能离开本州
otherwise I couldn't have left the state.

166
00:06:19,320 --> 00:06:23,240
好了 别再说监狱的事了
All right, that's enough jail talk.

167
00:06:23,240 --> 00:06:26,300
佩妮知道我在哪 她会给我送烟
Penny knows where I was; she sent me cigarettes.

168
00:06:26,300 --> 00:06:29,000
你给你弟弟送烟
You sent your brother cigarettes?

169
00:06:29,000 --> 00:06:30,750
他制作和贩卖冰毒 苏珊
He was cooking and selling crystal meth, Susan,

170
00:06:30,750 --> 00:06:33,920
我觉得吸烟是小事吧
I think we can let the cigarettes slide.

171
00:06:33,920 --> 00:06:35,670
你别再装酷爸的样子了
Stop trying to be the cool dad;

172
00:06:35,670 --> 00:06:39,510
你有件T恤上还印了我们家的猫咪呢
you have a shirt with our cat's picture on it.

173
00:06:39,510 --> 00:06:41,980
管他呢 反正我们终于来了 小拳手
Anyway, we're here, slugger.

174
00:06:44,770 --> 00:06:47,650
现在好了 他们都知道我的住址了
That's great, now they know where I live.

175
00:06:47,650 --> 00:06:48,900
你在说什么
What are you talking about?

176
00:06:48,900 --> 00:06:50,440
他们一直知道你住在哪
They've always known where you live.

177
00:06:50,440 --> 00:06:51,600
是啊 如果你想逃离他们的视线
Yeah, if you want to go off the grid,

178
00:06:51,610 --> 00:06:53,330
你就得搬出你老妈的房子
you have to move out of your mother's house.

179
00:06:55,160 --> 00:06:57,330
我们能不能先谈一谈
Can we take a moment to discuss

180
00:06:57,330 --> 00:06:59,490
我刚刚为了你向政府官员说谎了
that I just lied to the government for you?

181
00:06:59,500 --> 00:07:00,610
是啊
Yeah.

182
00:07:00,610 --> 00:07:02,780
换做我 我肯定不会为你那样做的
I would not have done that for you.

183
00:07:03,920 --> 00:07:06,750
霍华德 你就给那人回个电话 看看他到底要干嘛
Howard, please just call the man, see what he wants.

184
00:07:06,750 --> 00:07:08,670
好 好
All right, all right.

185
00:07:08,670 --> 00:07:10,120
嘿
Hey,

186
00:07:10,120 --> 00:07:12,170
确保你别说漏嘴 他来的时候你并不在家
make sure you tell him that you weren't home when he came by

187
00:07:12,180 --> 00:07:14,180
并且你的印度小伙伴在你刚进门的
and that your Indian friend gave you the message

188
00:07:14,180 --> 00:07:16,850
第一时间就通知了你
the moment you stepped through the door.

189
00:07:16,850 --> 00:07:20,470
好的 你好
Yes, hello. Uh,

190
00:07:20,470 --> 00:07:23,550
我是霍华德·沃罗威茨 找理查德·威廉姆斯上校
this is Howard Wolowitz for Colonel Richard Williams.

191
00:07:23,550 --> 00:07:25,220
我后悔了 干脆别提到我
Oh, I take it back, don't mention me.

192
00:07:25,220 --> 00:07:28,310
嗨 威廉姆斯上校 您有什么需要找我的吗
Hi, Colonel Williams, how can I help you?

193
00:07:28,310 --> 00:07:31,610
什么 呃 是 他是印度人
What? Oh, uh, yes, he is from India.

194
00:07:33,530 --> 00:07:36,650
不 我不清楚他的移民资格状况
No, I don't know his immigration status.

195
00:07:39,490 --> 00:07:42,650
放松点 我还在等他接电话呢
Relax, I'm still on hold!

196
00:07:44,160 --> 00:07:46,240
你说
Speaking.

197
00:07:46,240 --> 00:07:50,410
好的 当然 我周四可以和你会面
Okay, sure, I can meet with you on Thursday.

198
00:07:50,410 --> 00:07:52,000
就在加州理工会面 可以
Caltech is fine.

199
00:07:52,000 --> 00:07:54,170
呃 我能问问找我有什么事吗
Yeah, and may I ask what this is about?

200
00:07:55,550 --> 00:07:57,500
不可以吗
I may not?

201
00:07:57,500 --> 00:08:00,510
他也是这么跟我说的
That's what he said to me.

202
00:08:01,640 --> 00:08:02,760
这是你的 老妈
Here you are, Mother.

203
00:08:02,760 --> 00:08:04,390
谢谢
Thank you.

204
00:08:04,400 --> 00:08:06,060
我很开心你能留下来
I'm glad you decided to stay.

205
00:08:06,060 --> 00:08:09,510
能和你共度这样的特殊日子 对佩妮和我有着非同一般的意义
It's gonna be special for Penny and me to share this with you.

206
00:08:09,520 --> 00:08:13,240
我巴不得这一天赶快结束
I can't wait for this day to be over.

207
00:08:14,070 --> 00:08:16,440
是啊 非同一般 我老妈就是这样
Yeah, special, like that.

208
00:08:17,270 --> 00:08:19,860
他们来了
That's them.

209
00:08:19,860 --> 00:08:20,860
本来就够尴尬了
Please don't make things

210
00:08:20,860 --> 00:08:22,830
求你别再雪上加霜了
any more awkward than they already are.

211
00:08:22,830 --> 00:08:26,920
好吧 所以尽量减少尴尬或者保持不变 明白了
All right, so less or equally awkward, got it.

212
00:08:28,420 --> 00:08:29,500
嗨 快请进
Hey, guys, come on in.

213
00:08:29,500 --> 00:08:31,170
-谢谢 -早上好啊
- Oh, thank you. - Good morning.

214
00:08:31,170 --> 00:08:32,090
早上好 嗨
Morning. Hello.

215
00:08:32,090 --> 00:08:34,090
大家今天都过得怎么样啊
How is everyone today?

216
00:08:34,090 --> 00:08:35,090
过得不错 你呢
Good, and you?

217
00:08:35,090 --> 00:08:37,180
-好 很好 -很好
- Good, good. - Good.

218
00:08:37,180 --> 00:08:38,540
我也过得不错
I'm good, too.

219
00:08:38,550 --> 00:08:40,430
妙极了
Good.

220
00:08:43,220 --> 00:08:45,430
所以 你到底睡了我老妈没有
So, did you defile my mother or not?

221
00:08:46,390 --> 00:08:48,940
谢尔顿
Sheldon!

222
00:08:48,940 --> 00:08:51,020
你太不像话了
You're being rude.

223
00:08:51,030 --> 00:08:52,440
恕我直言 我向你保证
If I may, I can assure you,

224
00:08:52,440 --> 00:08:53,890
我们只是搭了同一辆的士并且聊了会儿天
your mother and I did nothing more

225
00:08:53,890 --> 00:08:56,110
除此之外什么都没做
than share a cab and a conversation.

226
00:08:56,110 --> 00:08:58,280
那你们的聊天内容有没有涉及
Did that conversation include the phrase

227
00:08:58,280 --> 00:09:00,450
"你的小拐杖真是顺手啊"
"Your genitals are a joy to behold"?

228
00:09:00,450 --> 00:09:02,650
够了
That's enough!

229
00:09:02,650 --> 00:09:07,620
听着 我向你发誓 我绝对没有 也没有其他人 说过那样的话
Look, I promise you, neither I, nor anyone, has ever said that.

