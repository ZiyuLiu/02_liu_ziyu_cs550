﻿310
00:12:32,730 --> 00:12:34,060
很好 懷特
Very nice, Wyatt.

311
00:12:34,060 --> 00:12:36,700
你還想不通這一個怎麼會變成今天這個樣子的
And you wonder why this one turned out the way he did.

312
00:12:36,700 --> 00:12:39,030
你看到了我每天都要忍受什麼了嗎
You see what I've gotta put up with!

313
00:12:39,040 --> 00:12:41,040
你每天都要忍受什麼了
What you've gotta put up with?

314
00:12:41,040 --> 00:12:42,740
你難道非要去蹲監獄嗎
Why did you have to go to jail?

315
00:12:42,740 --> 00:12:45,960
那是一不小心被捕了 老媽
It's called getting caught, Mother!

316
00:12:45,960 --> 00:12:47,660
哈囉
Hello!

317
00:12:47,660 --> 00:12:48,830
-嗨 他來了 -嗨
- Hey, there he is! - Hey!

318
00:12:48,830 --> 00:12:51,750
萊納德 真高興能再見到你
Oh, Leonard! It's so nice to see you again!

319
00:12:51,750 --> 00:12:52,830
噢 我也是
Oh, you, too!

320
00:12:52,830 --> 00:12:55,300
嗨 大家 這是我媽 貝弗利
Hey, everyone, this is my mother, Beverly.

321
00:12:55,300 --> 00:12:56,420
-你好 -你好啊
- Hello. - Hi.

322
00:12:56,420 --> 00:12:59,640
我們不是白人垃圾哦
We are not white trash!

323
00:13:03,890 --> 00:13:06,310
你兒子就要步入婚禮殿堂了 你開心嗎
Are you excited to see your son walk down the aisle?

324
00:13:06,310 --> 00:13:07,430
對 很開心
Yes, I am.

325
00:13:07,430 --> 00:13:08,650
只是我給他帶來那麼多煩惱
I'm just feeling a little guilty

326
00:13:08,650 --> 00:13:10,430
我感到很愧疚
about all the trouble I've caused.

327
00:13:10,430 --> 00:13:12,100
噢 我也是
Oh, so am I.

328
00:13:12,100 --> 00:13:15,600
你給上帝都帶來煩惱了 媽媽
You made God sad today, Mom.

329
00:13:22,580 --> 00:13:24,250
謝爾頓 他們又沒做錯什麼
Sheldon, they haven't done anything wrong.

330
00:13:24,250 --> 00:13:26,780
他們能合得來 我覺得也挺好呀
I think it's nice they're hitting it off.

331
00:13:26,780 --> 00:13:28,450
那也沒有必要發展得這麼迅速
Well, that's still no reason to rush into anything.

332
00:13:28,450 --> 00:13:29,530
看看我們
Look at us.

333
00:13:29,540 --> 00:13:31,250
我們就慢得蕩氣迴腸
We took things remarkably slow.

334
00:13:31,260 --> 00:13:34,090
你和我 前兩年連手都沒牽過
You and I, we didn't even hold hands for two years.

335
00:13:35,260 --> 00:13:38,290
真實故事比你聽到的要浪漫得多
It was a lot hotter than it sounds.

336
00:13:38,300 --> 00:13:39,600
你是一個耐心的好姑娘
You're a patient young lady.

337
00:13:39,600 --> 00:13:40,630
嘿嘿
Hey, hey!

338
00:13:40,630 --> 00:13:42,800
她是我的 你矜持一點 老爺爺
She's mine! Take a cold shower, grandpa!

339
00:13:46,940 --> 00:13:49,190
那位上校為什麼不說要開什麼會
Why wouldn't that colonel say what the meeting's about?

340
00:13:49,190 --> 00:13:50,720
一定是壞消息
It has to be bad news.

341
00:13:50,720 --> 00:13:52,980
冷靜一點好嗎 不要想太多了
Calm down, okay? Try not to think about it.

342
00:13:52,980 --> 00:13:55,530
你這個建議好蠢
That's really stupid advice.

343
00:13:55,530 --> 00:13:57,360
這句話很傷你造嗎
You know that hurts my feelings.

344
00:13:58,200 --> 00:14:00,530
冷靜一點 不要想太多了
Calm down, try not to think about it.

345
00:14:01,570 --> 00:14:03,120
也行
Okay.

346
00:14:04,710 --> 00:14:06,320
和你聊天簡直自討沒趣
Why do I bother talking to you?

347
00:14:06,320 --> 00:14:08,990
拜託 結果再壞能壞到哪裏去呢
Oh, come on. What's the worst that could come of this meeting?

348
00:14:08,990 --> 00:14:11,130
我不知道 也許他們會搶走發明
I don't know. They take the invention away,

349
00:14:11,130 --> 00:14:12,290
我卻一無所獲
and I get nothing?

350
00:14:12,300 --> 00:14:14,330
好吧 那不是最壞的情況
Okay, that's not so bad.

351
00:14:14,330 --> 00:14:15,580
你知道曼哈頓計劃里
You know what happened to the scientists

352
00:14:15,580 --> 00:14:17,170
那些科學家的結局嗎
that worked on the Manhattan Project?

353
00:14:17,170 --> 00:14:19,330
美國政府逼迫他們搬到了沙漠裏
The government forced them to move to the desert.

354
00:14:19,340 --> 00:14:21,800
他們不得不隱姓埋名
They had to live in secret, and when Oppenheimer objected

355
00:14:21,800 --> 00:14:23,670
後來奧本海默反對政府這種暴行
to what they made him do,

356
00:14:23,670 --> 00:14:25,670
政府就故意搞臭他的名聲
they destroyed his reputation.

357
00:14:26,760 --> 00:14:29,900
你這個故事想說明什麼
What's the point of that story?

358
00:14:31,180 --> 00:14:32,680
我剛讀了一本關於奧本海默的書
I just read a book about Oppenheimer,

359
00:14:32,680 --> 00:14:35,980
正好炫耀賣弄一下
seemed like a chance to show off.

360
00:14:38,360 --> 00:14:40,240
這就對了嘛
There he is!

361
00:14:40,240 --> 00:14:42,410
我們快樂的猶太宅男又回來了
There's my happy Hebraic homeboy.

362
00:14:42,410 --> 00:14:43,910
對 我會永遠記住你的苦笑
Yeah, that's the smile I'm gonna remember

363
00:14:43,910 --> 00:14:48,360
當你搬到沙漠裏 而我就可以勾引你老婆了
when you're living in the desert and I'm living with your wife.

364
00:14:52,250 --> 00:14:54,250
你做什麼工作的
So, what do you do for a living?

365
00:14:55,040 --> 00:14:57,210
媽咪 幫我來回答好嗎
Mommy, you want to take this one?

366
00:14:58,130 --> 00:15:00,540
呃 蘭德爾正在找工作
Um, Randall's in between jobs.

367
00:15:00,540 --> 00:15:03,350
還偶爾逛逛法院
And court appearances.

368
00:15:04,210 --> 00:15:05,210
很高興能見到你這位母親
It's nice to meet the woman

369
00:15:05,220 --> 00:15:07,430
養出了這麼優秀的年輕人
who raised this fine young man.

370
00:15:07,430 --> 00:15:09,180
真是期待見到他的父親
I'm looking forward to meeting his father.

371
00:15:09,190 --> 00:15:11,640
你就準備好大失所望吧
Prepare to be disappointed.

372
00:15:12,520 --> 00:15:13,690
他也等不及見你了
And he can't wait to meet you, too.

373
00:15:13,690 --> 00:15:15,060
誰還要喝的
Can I get anyone a drink?

374
00:15:15,060 --> 00:15:16,480
再給我來一瓶啤酒吧
Well, I could use another beer.

375
00:15:16,480 --> 00:15:17,560
你夠了
You're done.

376
00:15:19,860 --> 00:15:21,150
他喝夠了
He's done.

377
00:15:22,650 --> 00:15:25,280
嗨 你們都相互熟絡了嗎
Hey! Is everyone getting to know each other?

378
00:15:25,290 --> 00:15:26,820
一點都沒熟
Not at all!

379
00:15:37,630 --> 00:15:39,580
很高興再次見到你 霍夫斯塔德博士
Nice to see you again, Dr. Hofstadter.

380
00:15:39,580 --> 00:15:41,420
我是 額 萊納德的朋友 斯圖爾特
I'm, uh, Leonard's friend, Stuart.

381
00:15:41,420 --> 00:15:43,750
很高興見到你
Nice to see you, too.

382
00:15:43,750 --> 00:15:44,970
嗨 我是斯圖爾特
Hi, I'm Stuart.

383
00:15:44,970 --> 00:15:47,640
噢 我是埃弗瑞德 萊納德的爸爸
Ooh, I'm Alfred. Leonard's father.

384
00:15:47,640 --> 00:15:51,260
噢 嗨 呃抱歉 你們想坐在一起嗎
Oh! Oh, hi. Uh, I'm sorry, did you two want to sit together?

385
00:15:51,260 --> 00:15:53,230
-不 -不
- No. - No.

386
00:15:54,430 --> 00:15:59,270
剛我還在奇怪怎麼前排還有空座兒
I was wondering why the front row was available.

387
00:15:59,270 --> 00:16:01,440
好啦 看來一切就緒了
Okay, I think we're ready.

388
00:16:09,280 --> 00:16:11,500
人們為什麼會在婚禮上哭呢
Why do people cry at weddings?

389
00:16:11,500 --> 00:16:14,670
他們在為婚後生活做準備
They're practicing for what's coming later.

390
00:16:19,290 --> 00:16:22,210
謝謝你為了妹妹的婚禮把自己打理好了
Thank you for cleaning yourself up for your sister's wedding.