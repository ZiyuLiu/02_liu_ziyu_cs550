﻿470
00:20:45,340 --> 00:20:47,970
你要开你自己的车 管你自己的事
Well.. and you need to drive the car and mind your business.

471
00:20:49,450 --> 00:20:51,590
我不允许你对我无礼
I will not have you be disrespectful to me.

472
00:20:51,610 --> 00:20:52,640
神马 你又不是我妈
What-- you're not my mother.

473
00:20:52,660 --> 00:20:54,240
我不允许你对她无礼
Don't you be disrespectful to her.

474
00:20:54,270 --> 00:20:55,270
遵命 夫人
Yes ma'am.

475
00:20:56,900 --> 00:20:57,780
你会学会的
You'll get there.

476
00:20:57,810 --> 00:20:59,950
只要找到窍门就可以了
You've just gotta put some zing on it.

477
00:21:04,070 --> 00:21:06,380
佩妮 我不知道以前我都在担心些什么
Penny, I don't know what I was worried about.

478
00:21:06,610 --> 00:21:08,060
你的朋友们太可爱了
You're friends are just lovely.

479
00:21:08,080 --> 00:21:09,630
谢谢 妈妈
Oh, thanks, Mom.

480
00:21:09,800 --> 00:21:12,390
虽然那位谢尔顿稍微有点特别
Although that Sheldon is a bit peculiar.

481
00:21:14,140 --> 00:21:16,270
是吗 我从来没发现
Is he? I never noticed.

482
00:21:17,430 --> 00:21:19,500
他让我想起了我们养过的那只火鸡
He reminds me of that turkey we had

483
00:21:19,500 --> 00:21:21,560
抬头看雨的时候淹死了
who drowned looking up at the rain.

484
00:21:24,670 --> 00:21:26,820
警察警察 冷静
Cops, cops, be cool!