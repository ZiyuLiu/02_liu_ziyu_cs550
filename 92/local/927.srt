﻿240
00:09:37,150 --> 00:09:38,100
那我也会
then I can, too.

241
00:09:38,110 --> 00:09:40,440
敲敲 是谁啊
Knock, knock. Who's there?

242
00:09:40,440 --> 00:09:43,160
我们家简直令人羞耻
Our family is an embarrassment.

243
00:09:43,160 --> 00:09:45,090
那可不是个笑话
That's not much of a joke.

244
00:09:45,090 --> 00:09:45,740
好了
Okay.

245
00:09:45,750 --> 00:09:48,110
听着 老妈 我知道你很忐忑 但是我向你保证
Listen, Mom, I know you're nervous, but I promise you,

246
00:09:48,120 --> 00:09:50,450
没人会对你或者我们家评头论足
no one is gonna judge you or this family.

247
00:09:50,450 --> 00:09:51,950
我很抱歉
Oh, I'm sorry.

248
00:09:51,950 --> 00:09:53,290
只是这是我们第一次见
It's just we're meeting Leonard's parents

249
00:09:53,290 --> 00:09:54,620
莱纳德的家人
for the first time,

250
00:09:54,620 --> 00:09:57,790
并且 并且他们都是知识分子而且非常聪明
and-and they're academics and-and intellectuals,

251
00:09:57,790 --> 00:10:00,180
我不想让他们觉得我们家是白人垃圾
and I don't want them thinking we're white trash.

252
00:10:01,350 --> 00:10:04,600
那 不然呢 你以为他们会觉得我们是什么颜色的垃圾
Well, what color trash do you think they'll believe?

253
00:10:07,130 --> 00:10:08,970
你怎么会认为我会和一个刚认识的男人
How could you think that I would spend the night

254
00:10:08,970 --> 00:10:10,390
鬼混一晚
with a man I just met?

255
00:10:10,390 --> 00:10:13,810
那个叫耶稣的男人让你们在非洲建教堂
A man named Jesus convinced you to build a church in Africa.

256
00:10:13,810 --> 00:10:16,140
你真让他失望
You're kind of a sucker.

257
00:10:16,140 --> 00:10:19,480
反正什么也没发生 对吧
Well, nothing happened, right?

258
00:10:19,480 --> 00:10:20,360
都过去了
I-It's over.

259
00:10:20,360 --> 00:10:21,650
除非我们要结第三次婚
Until we get married a third time,

260
00:10:21,650 --> 00:10:24,570
不然你们再也不用见面了
you guys will never have to see each other again.

261
00:10:24,570 --> 00:10:26,450
呃 事实上并不完全是这样
Well, you know, actually that's not the case.

262
00:10:26,450 --> 00:10:28,740
-玛丽会去纽约拜访我 -嗯
- Mary may visit me in New York. - Mm-hmm.

263
00:10:28,740 --> 00:10:30,910
而且他从没去过德克萨斯州
And he's never been to Texas.

264
00:10:30,910 --> 00:10:32,210
也许我们在中途的某个地方见面
Maybe we meet halfway.

265
00:10:32,210 --> 00:10:36,910
什么 在佐治亚洲的查特胡奇国家森林
What? In the Chattahoochee National Forest in Georgia?

266
00:10:36,910 --> 00:10:41,880
不会只有我一个人知道这里是两地中点站吧
I can't be the only one that knows that's halfway.

267
00:10:41,890 --> 00:10:44,090
你们不会真的去拜访对方的吧
You're not seriously going to visit each other.

268
00:10:44,120 --> 00:10:45,140
为什么我们不会
And why wouldn't we?

269
00:10:45,160 --> 00:10:47,230
得了吧 你只是说这些想让我吃醋的
Oh please, you're just saying this to antagonize me.

270
00:10:47,520 --> 00:10:50,340
才不是呢 玛丽是位很好的女士
Oh, not at all. Mary happens to be a wonderful woman.

271
00:10:50,340 --> 00:10:52,640
如果这让你吃醋了的话 那倒只是福利
And if it antagonizes you, that's just a bonus.

272
00:10:54,850 --> 00:10:57,070
玛丽 我很抱歉把你卷进来
Mary, I'm sorry you're in the middle of this.

273
00:10:57,070 --> 00:10:59,100
不不 没什么好抱歉的
No, no, nothing to be sorry about.

274
00:10:59,100 --> 00:11:01,350
我真心挺喜欢你父亲的
I genuinely like your father.

275
00:11:01,360 --> 00:11:04,110
什么 但他只是个平庸的学者
What? But he's a mediocre academic.

276
00:11:04,110 --> 00:11:07,690
并且根据贝弗利所说 他的床上功夫逊透了
And according to Beverly, his sexual prowess is subpar.

277
00:11:08,830 --> 00:11:12,110
他基本上就是前列腺肥大版的莱纳德
He's basically Leonard with a bigger prostate.

278
00:11:14,500 --> 00:11:17,720
你的意思是我老爸配不上你妈吗
Are you saying that my dad's not good enough for your mom?

279
00:11:17,720 --> 00:11:20,040
是啊 顺便还挖苦了一下你
Yes, while also getting in a solid dig at you.

280
00:11:20,040 --> 00:11:21,620
很一语双关吧
Pretty efficient, huh?

281
00:11:22,880 --> 00:11:25,710
这太荒唐了 我要去对面待会儿
This is ridiculous. I-I'm going across the hall.

282
00:11:25,710 --> 00:11:27,630
凭什么你拍屁股走人而把我和你争吵不休的父母
But why should you get to go and leave me here

283
00:11:27,630 --> 00:11:28,630
留在一起
with your bickering parents?

284
00:11:28,630 --> 00:11:29,880
好吧 那你走啊
Fine, then you go!

285
00:11:29,880 --> 00:11:32,020
算了 我不想和她待在一起 我走吧
Well, I don't want to stay here with her. I'll go.

286
00:11:32,020 --> 00:11:32,930
我和你一起走
I'll go with you.

287
00:11:32,940 --> 00:11:34,190
这样我还是被和谢尔顿丢在一起啊
That still leaves me here with him.

288
00:11:34,190 --> 00:11:35,720
等一下 等一下
Hang on, hang on!

289
00:11:35,720 --> 00:11:37,220
我们这么聪明 一定能想出解决办法的
We're smart, we can figure this out.

290
00:11:37,220 --> 00:11:40,690
玛丽和贝弗利不能共处一室
Mary and Beverly can't be together.

291
00:11:40,690 --> 00:11:43,860
埃弗瑞德和贝弗利不能待在一起
Uh, Alfred and Beverly can't be together.

292
00:11:43,860 --> 00:11:46,360
莱纳德和我不能待在一起
Leonard and I can't be together.

293
00:11:46,370 --> 00:11:49,120
所以 我可以和埃弗瑞德
Now, I could be with Alfred...

