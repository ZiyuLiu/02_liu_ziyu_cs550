﻿120
00:04:10,870 --> 00:04:14,570
这一件还是令我拜服
{\3c&H000000&\fs30}that was mighty impressive.

121
00:04:15,210 --> 00:04:16,840
我们订婚了
{\3c&H000000&\fs30}We're engaged.

122
00:04:16,840 --> 00:04:18,740
天啊 太棒了
{\3c&H000000&\fs30}Oh, my God, that's amazing!

123
00:04:18,750 --> 00:04:20,410
等等 呃呃 快告诉我全过程
{\3c&H000000&\fs30}Wait, uh, tell me everything.

124
00:04:20,410 --> 00:04:23,080
好吧 诺维茨基博士当时正在亲我
{\3c&H000000&\fs30}Well, Dr. Nowitzki was kissing me...

125
00:04:23,080 --> 00:04:25,620
好吧 你可以别用这段做引子了
{\3c&H000000&\fs30}Okay, you can stop leading with that part of the story.

126
00:04:26,620 --> 00:04:28,220
恭喜你们
{\3c&H000000&\fs30}Well, congratulations.

127
00:04:28,220 --> 00:04:29,720
我真为你俩开心
{\3c&H000000&\fs30}I'm so happy for you two.

128
00:04:29,720 --> 00:04:31,520
等等 我得告诉伯纳黛特
{\3c&H000000&\fs30}Hold on, I have to tell Bernadette.

129
00:04:31,520 --> 00:04:32,690
伯妮 猜猜发生了什么
{\3c&H000000&\fs30}Hey, Bernie, guess what?

130
00:04:32,690 --> 00:04:35,460
谢尔顿和艾米订婚了 你相信吗
{\3c&H000000&\fs30}Sheldon and Amy got engaged. Can you believe it?

131
00:04:35,460 --> 00:04:40,160
哦 我的上帝 我简直不敢相信
{\3c&H000000&\fs30}Oh, my God. I cannot believe it.

132
00:04:41,670 --> 00:04:44,600
她太高兴了 我觉得她哭了
{\3c&H000000&\fs30}She's so happy... I think she's crying.

133
00:04:47,670 --> 00:04:49,070
你觉得谢尔顿会不会想要
{\3c&H000000&\fs30}Do you think Sheldon's gonna want

134
00:04:49,080 --> 00:04:52,180
办个诡异的星际迷航主题婚礼
{\3c&H000000&\fs30}some weird Star Trek wedding?

135
00:04:52,180 --> 00:04:54,380
我不知道
{\3c&H000000&\fs30}I don't know.

136
00:04:54,380 --> 00:04:56,610
莱纳德还没说完
{\3c&H000000&\fs30}Well, Leonard could barely finish the words

137
00:04:56,620 --> 00:04:57,780
“神秘博士新婚蛋糕”这几个字
{\3c&H000000&\fs30}"Doctor Who wedding cake"

138
00:04:57,780 --> 00:05:00,890
就被我狠狠拒绝了
{\3c&H000000&\fs30}before I shut that down hard.

139
00:05:00,890 --> 00:05:02,420
呵呵
{\3c&H000000&\fs30}Mm-hmm.

140
00:05:02,420 --> 00:05:04,690
你在听吗
{\3c&H000000&\fs30}Are you listening to me?