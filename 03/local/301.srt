﻿100
00:03:30,160 --> 00:03:31,560
因为我为此祈祷很久了
{\3c&H000000&\fs30}I've been praying for this.

101
00:03:31,560 --> 00:03:33,600
呃 上帝与此无关
{\3c&H000000&\fs30}Well, God had nothing to do with it.

102
00:03:33,600 --> 00:03:36,200
我求婚是因为我当时正在亲另一个女人
{\3c&H000000&\fs30}It happened because I was kissing another woman,

103
00:03:36,200 --> 00:03:39,640
这使我意识到我想和艾米在一起
{\3c&H000000&\fs30}and it made me realize I wanted to be with Amy.

104
00:03:39,640 --> 00:03:41,670
不只一个女人喜欢你吗
{\3c&H000000&\fs30}More than one woman was interested in you?

105
00:03:41,670 --> 00:03:44,410
我可能祈祷的有点过头
{\3c&H000000&\fs30}I might have prayed a little too hard.

106
00:03:44,410 --> 00:03:46,040
等等 哦 我只是
{\3c&H000000&\fs30}Wait, oh, and I just...

107
00:03:46,050 --> 00:03:47,310
我想让你知道
{\3c&H000000&\fs30}I want to let you know right now

108
00:03:47,310 --> 00:03:49,480
我们不打算在教堂里结婚
{\3c&H000000&\fs30}that we are not getting married in a church.

109
00:03:49,480 --> 00:03:50,410
没关系 谢尔顿
{\3c&H000000&\fs30}That's all right, Sheldon.

110
00:03:50,420 --> 00:03:53,250
主所在之处皆为教堂
{\3c&H000000&\fs30}Anywhere Jesus is is a church.