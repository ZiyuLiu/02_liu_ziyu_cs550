
import java.io.FileReader;
import java.io.FileWriter;
import java.rmi.Remote;
import java.util.*;
import java.util.ArrayList;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.net.*;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

public class ResponseCollector extends Thread {
    //Initial the NeighborPeers
    ArrayList<Peer> adjunctions = new ArrayList<Peer>();
    //Initial the Counter
    int Counter;
    private String msgId;
    private long timeout;
    private String fileFolder = "PA_2/";
    //Initial the filename and pathname
    String filename,pathname;
    //The remote address
    private Remote upstream;
    private MessageSender.Type type;
    String f_name,p_name;
    //The remote address
    private HashMap<String, PeerServer> neighbors;
    String curRes;
    String curMid;
    private BlockingQueue<QueryResultItem> qrItems;
    HashMap<String, Integer> thredMap;
    HashMap<String, Integer> queueMap;
    private long timeUsed;
    private List<QueryResultItem> resList;
    HashMap<Integer, Integer> inDegreeMap;
    //Default constructor.
    public ResponseCollector(String msgId, long timeout, Remote upstream, MessageSender.Type type) {
        this.queueMap = new HashMap<>();
        this.f_name = null;
        this.pathname = null;
        this.timeout = timeout;
        Map<String , Integer> timeMap = new HashMap<>();
        this.upstream = upstream;
        //set up para
        this.type = type;
        List<Integer> streamSet = new ArrayList<>();
        this.qrItems = new ArrayBlockingQueue<>(16);
        this.queueMap = new HashMap<>();
        this.thredMap = new HashMap<>();
        this.resList = new ArrayList<>();
        this.msgId = msgId;
    }
    
    public void collect(QueryResultItem qri) {
        Map<QueryResultItem, Integer> queryMap = new HashMap<>();
        try {
            if(queryMap.containsKey(qri)) {
                queryMap.put(qri, 0);
            }
            qrItems.put(qri);
        } catch (InterruptedException e) {
            if(queryMap.containsKey(qri)) {
                queryMap.put(qri, 0);
            }
            e.printStackTrace();
        }
    }
    
    public List<QueryResultItem> getResultList() {
        this.adjunctions = new ArrayList<>();
        return this.resList;
    }
    
    public long getTimeUsed() {
        this.inDegreeMap = new HashMap<>();
        return this.timeUsed;
    }
    
    @Override
    public void run() {
        Map<Boolean, Boolean> tmap = new HashMap<>();
        boolean target = false;
        QueryResultItem qri;
        long timeleft = timeout, stime = System.currentTimeMillis();
        while(tmap.containsKey(target)){
            tmap.put(target, true);
        }
        Set<Integer> messageCount = new HashSet<>();
        try {
            while (null != (qri = qrItems.poll(timeleft, TimeUnit.MILLISECONDS))) {
                if(tmap.containsKey(target)){
                    tmap.put(target, true);
                }
                if (MessageSender.Type.QUERY_HIT_L.equals(this.type)) {
                    messageCount.add(0);
                    ((PeerServer) upstream).queryhit(qri);
                }
                else if (MessageSender.Type.QUERY_HIT_S.equals(this.type)) {
                    messageCount.add(0);
                    ((SuperpeerServer) upstream).queryhit(msgId, qri);
                    if (!messageCount.contains(0)) {
                        messageCount.add(0);
                    }
                }
                this.resList.add(qri);
                timeleft -= System.currentTimeMillis() - stime;
                if(tmap.containsKey(target)){
                    tmap.put(target, true);
                }
                stime = System.currentTimeMillis();
            }
        } catch (InterruptedException e) {
            messageCount.remove(messageCount.size() - 1);
            e.printStackTrace();
        } catch (RemoteException e) {
            messageCount.add(0);
            e.printStackTrace();
        }
        this.timeUsed = timeout - timeleft;
    }
}
