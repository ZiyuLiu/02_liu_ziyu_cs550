import java.math.BigInteger;
import java.util.Vector;

public class test {
    public static void main(String[] args) {
        KeyGenerator kg = new KeyGenerator();
        Encryptor encryptor = new Encryptor();
        String msg = "aaadsfsdfsdfsdfsfsf";
        PublicKey pub = kg.getPublicKey();
        Vector<BigInteger> en = encryptor.encrypt(msg, pub);
        System.out.println(en);
        Decryptor decryptor = new Decryptor();
        PrivateKey privateKey = kg.getPrivateKey();
        String s = decryptor.decrypt(en, privateKey);
        System.out.println(s);
    }
}
