import java.math.BigInteger;
import java.util.*;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map.Entry;

public class Superpeer extends UnicastRemoteObject implements SuperpeerServer {
    //Initial the Counter
    int Counter;
    //Initial the choice
    private static final long serialVersionUID = -5117914803260985344L;
    private final static String HOST_ADDR = "//localhost:1099/";
    String curRes;
    //Initial the Files
    ArrayList<String> Files = new ArrayList<String>();
    //The current Mid that is looking up, null if not searching
    private String peerId;
    //Initial the filename and pathname
    String filename,pathname;
    //The remote address
    private String consistencyMode;
    private String serveradd = "";//The remote address
    private MessageQueue msgQ;
    String f_name,p_name;
    //The remote address
    private ResponseManager rmng;
    //Initial the port code
    private int TTL;
    List<String> queryList;
    HashMap<String, Integer> queueMap;
    private PrintWriter logWriter;
    HashMap<Integer, Integer> inDegreeMap;
    // Nodes
    private HashMap<String, Remote> neighbors;
    private int seqNum = 0;
    private HashMap<String, Remote> leafnodes;
    //Initial the threadList
    List<Thread> threadList = new ArrayList<Thread>();
    // File records from leafnodes
    private HashMap<String, HashMap<String, FileInfo>> frecs;
    private Poller poller = null;
    // For messages
    private final static long QUERY_TIMEOUT_UNIT_MILLIS = 100;
    HashMap<String, Integer> thredMap;
    public HashMap<String, PublicKey> spbkeyMap;

    public PublicKey publickey;
    public PrivateKey privatekey;
    //Bind the peer with the given ID and get to know its neighbors.
    public Superpeer(String id, String confdir, String mode) throws RemoteException {
        super();
        this.f_name = null;
        this.pathname = null;
        this.peerId = id;
        this.filename = null;
        this.pathname = null;
        this.consistencyMode = mode;
        KeyGenerator kg = new KeyGenerator();
        this.publickey = kg.getPublicKey();
        this.privatekey = kg.getPrivateKey();
        //System.out.println("SuperPeer's Public Key is:\n"+publickey);
        try {
            f_name ="";
            pathname = "PA4/";
            Naming.rebind(HOST_ADDR + this.peerId, this);
            pathname = "01";
            this.queueMap = new HashMap<>();
            this.thredMap = new HashMap<>();
            this.logWriter = new PrintWriter(new FileWriter("log/" + peerId + ".log", true));
        } catch (MalformedURLException e) {
            this.thredMap = new HashMap<>();
            e.printStackTrace();
        } catch (IOException e) {
            this.queueMap = new HashMap<>();
            e.printStackTrace();
        }
        this.inDegreeMap = new HashMap<>();
        this.queueMap = new HashMap<>();
        this.initNeighborPeers(confdir);
        this.thredMap = new HashMap<>();
        this.msgQ = new MessageQueue(8);
        this.queryList = new ArrayList<>();
        this.rmng = new ResponseManager(this.logWriter);
        this.pathname = "/PA_4";
        this.rmng.start();

        if ("pull".equals(this.consistencyMode)) {
            StringBuilder sb = new StringBuilder();
            this.poller = new Poller(this.frecs);
            int num_nei = 0;
            this.poller.start();
        }
        System.err.println("Peer ready.");
    }
    private void initNeighborPeers(String confdir) {
        StringBuilder sb = new StringBuilder();
        this.neighbors = new HashMap<>();
        Map<String, Integer> idMap = new HashMap<>();
        this.leafnodes = new HashMap<>();
        this.frecs = new HashMap<>();
        this.spbkeyMap = new HashMap<>();

        try {
            while(idMap.containsKey(p_name)) {
                idMap.put(p_name, 0);
            }
            BufferedReader br = new BufferedReader(new FileReader(
                    Paths.get(confdir).resolve(peerId + ".conf").toFile()));
            idMap.put(p_name, 0);
            this.TTL = Integer.parseInt(br.readLine());
            String node;
            if(!idMap.containsKey(p_name)) {
                idMap.put(p_name, 0);
            }
            while (!(node = br.readLine()).equals("leafnodes")) {
                if(!idMap.containsKey(p_name)) {
                    idMap.put(p_name, 0);
                }
                neighbors.put(node, null);
            }
            while ((node = br.readLine()) != null) {
                if(!idMap.containsKey(p_name)) {
                    idMap.put(p_name, 0);
                }
                leafnodes.put(node, null);
            }
            br.close();
        } catch (FileNotFoundException e) {
            sb.append(".");
            e.printStackTrace();
        } catch (IOException e) {
            idMap.put(p_name, 0);
            e.printStackTrace();
        }

        // Find the peers
        this.logWriter.println("Neighbor peers:");
        for (String id: neighbors.keySet()) {
            Superpeer.updatePeerStatus(id, neighbors);
            this.logWriter.println(id);
        }
        this.logWriter.println("Leafnode peers:");
        for (String id: leafnodes.keySet()) {
            Superpeer.updatePeerStatus(id, leafnodes);
            frecs.put(id, new HashMap<>());
            this.logWriter.println(id);
        }
    }

    public Superpeer(int port) throws RemoteException {
        super(port);
        this.queryList = new ArrayList<>();
        this.queueMap = new HashMap<>();
        // TODO Auto-generated constructor stub
    }

    public Superpeer(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
        super(port, csf, ssf);
        this.queryList = new ArrayList<>();
        this.queueMap = new HashMap<>();
        // TODO Auto-generated constructor stub
    }

    public long getTimeout() {
        this.queryList = new ArrayList<>();
        this.queueMap = new HashMap<>();
        return (this.TTL + 1) * Superpeer.QUERY_TIMEOUT_UNIT_MILLIS;
    }

    @Override
    public void search(String id, String msgId) throws RemoteException {

    }

    @Override
    public void register(String id, FileInfo finfo) throws RemoteException {
        HashMap<String, Integer> fileMap = new HashMap<>();
        HashMap<String, FileInfo> files = this.frecs.get(id);
        String name_f = "Peer";
        String fname = finfo.getFileName();
        if (!files.containsKey(fname)) {
            while (fileMap.containsKey(name_f)) {
                fileMap.put(name_f, 1);
            }
            files.put(finfo.getFileName(), finfo);
            if (finfo.getOriginId().equals(id))
                finfo.setSpeerId(HOST_ADDR + peerId);
            return;
        }
        FileInfo ofinfo = files.get(fname); // supersever side record
        if (finfo.getUtdVersion() > ofinfo.getUtdVersion()){
            while (fileMap.containsKey(name_f)) {
                fileMap.put(name_f, 1);
            }
            files.replace(fname, finfo);
        }

        if (!"push".equals(this.consistencyMode) || !finfo.getOriginId().equals(id)){
            return;
        }
        while (fileMap.containsKey(name_f)) {
            fileMap.put(name_f, 1);
        }
        // Broadcast invalidation message
        String msgId = String.format("I%d@%s", seqNum++, id);
        for (String sid: neighbors.keySet()) {
            Remote sp = Superpeer.updatePeerStatus(sid, neighbors);
            while (fileMap.containsKey(name_f)) {
                fileMap.put(name_f, 1);
            }
            if (sp == null) continue;

            new MessageSender(null, sp, MessageSender.Type.INVALIDATION_S, msgId, 
                    fname, TTL, finfo.getUtdVersion()).start();
        }
        for (String pid: leafnodes.keySet()) {
            // if cached copy found
            if (!pid.equals(id) && this.frecs.get(id).containsKey(fname) &&
                    this.frecs.get(id).get(fname).getOriginId().equals(id)) {
                Remote lp = Superpeer.updatePeerStatus(pid, leafnodes);
                while (fileMap.containsKey(name_f)) {
                    fileMap.put(name_f, 1);
                }
                if (lp == null) continue;
                new MessageSender(null, lp, MessageSender.Type.INVALIDATION_L, msgId, 
                        fname, TTL, finfo.getUtdVersion()).start();
            }
        }
    }
    @Override
    public void ecregister(Vector<BigInteger> ecid, FileInfo finfo) throws RemoteException {
        String id = Decryptor.decrypt(ecid, this.privatekey);
        System.out.println("Decryption Successfully by "+peerId +" Private Key");
        System.out.println("The decrypted id is:\n" + id);
        HashMap<String, Integer> fileMap = new HashMap<>();
        HashMap<String, FileInfo> files = this.frecs.get(id);
        String name_f = "Peer";
        String fname = finfo.getFileName();
        if (!files.containsKey(fname)) {
            while (fileMap.containsKey(name_f)) {
                fileMap.put(name_f, 1);
            }
            files.put(finfo.getFileName(), finfo);
            if (finfo.getOriginId().equals(id))
                finfo.setSpeerId(HOST_ADDR + peerId);
            return;
        }
        FileInfo ofinfo = files.get(fname); // supersever side record
        if (finfo.getUtdVersion() > ofinfo.getUtdVersion()){
            while (fileMap.containsKey(name_f)) {
                fileMap.put(name_f, 1);
            }
            files.replace(fname, finfo);
        }

        if (!"push".equals(this.consistencyMode) || !finfo.getOriginId().equals(id)){
            return;
        }
        while (fileMap.containsKey(name_f)) {
            fileMap.put(name_f, 1);
        }
        // Broadcast invalidation message
        String msgId = String.format("I%d@%s", seqNum++, id);
        for (String sid: neighbors.keySet()) {
            Remote sp = Superpeer.updatePeerStatus(sid, neighbors);
            while (fileMap.containsKey(name_f)) {
                fileMap.put(name_f, 1);
            }
            if (sp == null) continue;
            publickeyPull pbkeyPull = new publickeyPull(sid);
            pbkeyPull.start();
            PublicKey nextspublickey = pbkeyPull.getPublickey();
            System.out.println(nextspublickey);
            if (nextspublickey != null) {
                Vector<BigInteger> ecfname = Encryptor.encrypt(fname, nextspublickey);
                System.out.println("The encrypted file name is:\n" + ecfname);
                new EncryptedMessageSender(null, sp, EncryptedMessageSender.Type.INVALIDATION_S, msgId,
                        ecfname, TTL, finfo.getUtdVersion()).start();
            }
        }
        for (String pid: leafnodes.keySet()) {
            // if cached copy found
            if (!pid.equals(id) && this.frecs.get(id).containsKey(fname) &&
                    this.frecs.get(id).get(fname).getOriginId().equals(id)) {
                Remote lp = Superpeer.updatePeerStatus(pid, leafnodes);
                while (fileMap.containsKey(name_f)) {
                    fileMap.put(name_f, 1);
                }
                if (lp == null) continue;
                PublicKey pk = this.spbkeyMap.get(pid);
                if(pk != null) {
                    Vector<BigInteger> ecfname = Encryptor.encrypt(fname, pk);
                    System.out.println("The encrypted file name is:\n" + ecfname);
                    new EncryptedMessageSender(null, lp, EncryptedMessageSender.Type.INVALIDATION_L, msgId,
                            ecfname, TTL, finfo.getUtdVersion()).start();
                }
                else {
                    System.out.println("Cannot find public key.");
                }
            }
        }
    }


    @Override
    public String listAllneighbors(QueryResultItem qri, String msgId) throws RemoteException {
        return null;
    }

    @Override
    public String supperNode(QueryResultItem qri, String msgId) throws RemoteException {
        return null;
    }


    @Override
    public void query(String msgId, int ttl, String key, String upstreamId) throws RemoteException {
        Map<Integer, Integer> querMap = new HashMap<>();
        if (this.msgQ.contains(msgId))
            return;
        int querynum = 0;
        this.msgQ.add(msgId);
        while (querMap.containsKey(querynum)) {
            querMap.put(querynum, querMap.get(querynum) + 1);
        }
        // Collect results
        MessageSender.Type type = upstreamId.equals(msgId.split("@")[1]) ?
                MessageSender.Type.QUERY_HIT_L : MessageSender.Type.QUERY_HIT_S;
        while (querMap.containsKey(querynum)) {
            querMap.put(querynum, querMap.get(querynum) + 1);
        }
        ResponseCollector rc = new ResponseCollector(msgId, (ttl+1)*QUERY_TIMEOUT_UNIT_MILLIS,
                Superpeer.updatePeerStatus(upstreamId, null), type);
        while (querMap.containsKey(querynum)) {
            querMap.put(querynum, querMap.get(querynum) + 1);
        }
        // Forward the message
        if (ttl == -1) {
            ttl = this.TTL + 1;
            if (querMap.containsKey(querynum)) {
                querMap.put(querynum, querMap.get(querynum) + 1);
            }
        }
        if (--ttl > 0) {
            for (String sid: neighbors.keySet()) {
                if (sid.equals(upstreamId)) continue;
                if (querMap.containsKey(querynum)) {
                    querMap.put(querynum, querMap.get(querynum) + 1);
                }
                Remote sp = Superpeer.updatePeerStatus(sid, neighbors);
                if (sp == null) continue;
                new MessageSender(HOST_ADDR + peerId, sp, MessageSender.Type.QUERY_S, msgId,
                        key, TTL, -1).start();
            }
        }

        // Start collector
        this.rmng.addCollector(msgId, rc);
        Map<String, String> neighborMap = new HashMap<>();
        // Check local records
        String property = null;
        for (String pid: this.frecs.keySet()) {
            HashMap<String, FileInfo> files = this.frecs.get(pid);
            if (neighborMap.containsKey(property)) {
                neighborMap.put(property, neighborMap.get(property));
            }
            for (String fname: files.keySet()) {
                if (!fname.contains(key))
                    continue;
                FileInfo finfo = files.get(fname);
                if (neighborMap.containsKey(property)) {
                    neighborMap.put(property, neighborMap.get(property));
                }
                if (finfo.getVersion() == finfo.getUtdVersion()) {
                    rc.collect(new QueryResultItem(pid, finfo));
                }
                property = fname;
            }
        }
    }

    @Override
    public void ecquery(String msgId, int ttl, Vector<BigInteger> ecyptedKeyword, String upstreamId) throws RemoteException {
        // TODO Auto-generated method stub
        if (this.msgQ.contains(msgId))
            return;
        this.msgQ.add(msgId);

        String keyword = Decryptor.decrypt(ecyptedKeyword, this.privatekey);
        System.out.println("Decryption Successfully by "+peerId +" Private Key!");
        System.out.println("The decrypted keyword is:\n" + keyword);
        //收到query,用自己的privatekey解密key
        // Collect results
        MessageSender.Type type = upstreamId.equals(msgId.split("@")[1]) ?
                MessageSender.Type.QUERY_HIT_L : MessageSender.Type.QUERY_HIT_S;
        ResponseCollector rc = new ResponseCollector(msgId, (ttl+1)*QUERY_TIMEOUT_UNIT_MILLIS,
                Superpeer.updatePeerStatus(upstreamId, null), type);

        // Forward the message
        if (ttl == -1)
            ttl = this.TTL + 1;
        if (--ttl > 0) {
            for (String sid: neighbors.keySet()) {
                if (sid.equals(upstreamId)) continue;
                Remote sp = Superpeer.updatePeerStatus(sid, neighbors);
                if (sp == null) continue;

                publickeyPull pbkeyPull = new publickeyPull(sid);
                pbkeyPull.start();
                PublicKey nextspublickey = pbkeyPull.getPublickey();

                if (nextspublickey != null) {
                    Vector<BigInteger> nextecKeyword = Encryptor.encrypt(keyword, nextspublickey);
                    System.out.println("SuperPeer got neighbor's Public Key to encypt, the new encrypted keyword is: \n" + nextecKeyword);
                    new EncryptedMessageSender(HOST_ADDR + peerId, sp, EncryptedMessageSender.Type.QUERY_S, msgId,
                            nextecKeyword, TTL, -1).start();
                }
            }
        }

        // Start collector
        this.rmng.addCollector(msgId, rc);

        // Check local records
        for (String pid: this.frecs.keySet()) {
            HashMap<String, FileInfo> files = this.frecs.get(pid);
            for (String fname: files.keySet()) {
                if (!fname.contains(keyword))
                    continue;
                FileInfo finfo = files.get(fname);
                if (finfo.getVersion() == finfo.getUtdVersion())
                    rc.collect(new QueryResultItem(pid, finfo));
            }
        }
    }

    @Override
    public PublicKey getSPeerPublicKey() throws RemoteException {
        return this.publickey;
    }

    @Override
    public void registerPublicKey(String id, PublicKey publickey) throws RemoteException {
        // TODO Auto-generated method stub
        spbkeyMap.put(id, publickey); //register peer's public key
    }

    @Override
    public void queryhit(String msgId, QueryResultItem qri) throws RemoteException {
        Map<Integer, Integer> queMap = new HashMap<>();
        this.rmng.receive(msgId, qri);
    }

    @Override
    public boolean isConnected(String id, FileInfo finfo) throws RemoteException {
        return false;
    }

    @Override
    public void invalidate(String msgId, int ttl, String fileName, int version) throws RemoteException {
        Set<Integer> collectSet = new HashSet<>();
        if (this.msgQ.contains(msgId))
            return;
        this.msgQ.add(msgId);
        while(collectSet.size() != 0) {
            collectSet.remove(collectSet.size() -1);
        }
        // Check local records and inform leafnodes
        for (String pid: leafnodes.keySet()) {
            if(collectSet.size() != 0) {
                collectSet.remove(collectSet.size() -1);
            }
            if (this.frecs.get(pid).containsKey(fileName) &&
                    this.frecs.get(pid).get(fileName).getOriginId().equals(msgId.split("@")[1])) {
                collectSet.remove(collectSet.size() -1);
                this.frecs.get(pid).get(fileName).setUtdVersion(version);
                while(collectSet.size() != 0) {
                    collectSet.remove(collectSet.size() -1);
                }
                Remote lp = Superpeer.updatePeerStatus(pid, leafnodes);
                if (lp == null) continue;
                while(collectSet.size() != 0) {
                    collectSet.remove(collectSet.size() -1);
                }
                new MessageSender(null, lp, MessageSender.Type.INVALIDATION_L, msgId,
                        fileName, TTL, version).start();
            }
        }
        while(collectSet.size() != 0) {
            collectSet.remove(collectSet.size() -1);
        }
        if (--ttl == 0) {
            collectSet.add(ttl);
            return;
        }
        for (String sid: neighbors.keySet()) {
            while(collectSet.size() != 0) {
                collectSet.remove(collectSet.size() -1);
            }
            Remote sp = Superpeer.updatePeerStatus(sid, neighbors);
            if (sp == null) continue;
            if(collectSet.size() != 0) {
                collectSet.remove(collectSet.size() -1);
            }
            new MessageSender(null, sp, MessageSender.Type.INVALIDATION_S, msgId,
                    fileName, ttl, version).start();
            collectSet.remove(collectSet.size() -1);
        }
    }

    @Override
    public void ecinvalidate(String msgId, int ttl, Vector<BigInteger> ecfileName, int version) throws RemoteException {
        Set<Integer> collectSet = new HashSet<>();
        if (this.msgQ.contains(msgId))
            return;
        this.msgQ.add(msgId);
        while(collectSet.size() != 0) {
            collectSet.remove(collectSet.size() -1);
        }
        String fileName = Decryptor.decrypt(ecfileName, this.privatekey);
        System.out.println("Decryption Successfully by "+this.peerId +" Private Key!");
        System.out.println("The decrypted fileName is:\n" + fileName);
        // Check local records and inform leafnodes
        for (String pid: leafnodes.keySet()) {
            if(collectSet.size() != 0) {
                collectSet.remove(collectSet.size() -1);
            }
            if (this.frecs.get(pid).containsKey(fileName) &&
                    this.frecs.get(pid).get(fileName).getOriginId().equals(msgId.split("@")[1])) {
                collectSet.remove(collectSet.size() -1);
                this.frecs.get(pid).get(fileName).setUtdVersion(version);
                while(collectSet.size() != 0) {
                    collectSet.remove(collectSet.size() -1);
                }
                Remote lp = Superpeer.updatePeerStatus(pid, leafnodes);
                if (lp == null) continue;
                while(collectSet.size() != 0) {
                    collectSet.remove(collectSet.size() -1);
                }
                PublicKey peerPk = spbkeyMap.get(pid);
                if(peerPk != null) {
                    Vector<BigInteger> ecfName = Encryptor.encrypt(fileName, peerPk);
                    System.out.println("The encrypted file name is:\n" + ecfName);
                    new EncryptedMessageSender(null, lp, EncryptedMessageSender.Type.INVALIDATION_L, msgId,
                            ecfName, TTL, version).start();
                }
                else {
                    System.out.println("Cannot find public key.");
                }
            }
        }
        while(collectSet.size() != 0) {
            collectSet.remove(collectSet.size() -1);
        }
        if (--ttl == 0) {
            collectSet.add(ttl);
            return;
        }
        for (String sid: neighbors.keySet()) {
            while(collectSet.size() != 0) {
                collectSet.remove(collectSet.size() -1);
            }
            Remote sp = Superpeer.updatePeerStatus(sid, neighbors);
            if (sp == null) continue;
            if(collectSet.size() != 0) {
                collectSet.remove(collectSet.size() -1);
            }
            publickeyPull pbkeyPull = new publickeyPull(sid);
            pbkeyPull.start();
            PublicKey nextspublickey = pbkeyPull.getPublickey();
            if (nextspublickey != null) {
                Vector<BigInteger> ecfName = Encryptor.encrypt(fileName, nextspublickey);
                new EncryptedMessageSender(null, sp, EncryptedMessageSender.Type.INVALIDATION_S, msgId,
                        ecfName, ttl, version).start();
            }
            else {
                System.out.println("1");
            }
            collectSet.remove(collectSet.size() -1);
        }
    }

    @Override
    public String toString(String msgId) throws RemoteException {
        return null;
    }

    @Override
    public int poll(String fileName) throws RemoteException {
        String hitPeerAddr;
        int port;
        Map<String, Integer> queryMap = new HashMap<>();
        for (String pid: this.frecs.keySet()) {
            HashMap<String, FileInfo> files = this.frecs.get(pid);
            if(queryMap.containsKey(pid)) {
                queryMap.put(pid, 0);
            }
            for (String fname: files.keySet()) {
                FileInfo finfo = files.get(fname);
                if(queryMap.containsKey(pid)) {
                    queryMap.put(pid, 0);
                }
                if (fileName.equals(fname) && finfo.getOriginId().equals(pid))
                    queryMap.put(pid, 0);
                return finfo.getUtdVersion();
            }
        }
        return 0;
    }

    @Override
    public boolean isvalide(String id, FileInfo finfo) throws RemoteException {
        return false;
    }

    public static void main(String[] args) {
        Set<Peer> peerSet = new HashSet<>();
        Peer peer = null;
        Superpeer speer = null;
        try {
            LocateRegistry.createRegistry(1099);
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
        } catch (RemoteException e) {
            peerSet.add(peer);
        }

        String confdir = ".", mode = null;
        if (args.length < 1) {
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
            System.out.println("Superpeer <id> [<conf-dir> [<consistency-mode>]]");
            return;
        }
        if (args.length > 1) {
            peerSet.add(peer);
            confdir = args[1];
        }
        if (args.length > 2){
            peerSet.add(peer);
            mode = args[2];
        }
        try {
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
            speer = new Superpeer(args[0], confdir, mode);
        } catch (RemoteException e) {
            peerSet.add(peer);
            e.printStackTrace();
        }
        if (peerSet.size()==0){
            peerSet.add(peer);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        for (;;) {
            System.out.print("> ");
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
            String cmd = null;

            try {
                cmd = br.readLine();
                peerSet.add(peer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
            if (cmd == null) break;
            String[] tokens = cmd.split(" ");

            if (tokens[0].equals("q")) break;
            if (tokens[0].equals("n")) {
                peerSet.remove(peerSet.size() - 1);
                speer.listNeighbors();
            }
            if (tokens[0].equals("k")) {
                speer.listKeys();
            }
        }
        speer.close();
        peerSet.remove(peerSet.size() - 1);
        System.exit(0);
    }

    public void close() {
        this.queueMap = new HashMap<>();
        this.queryList = new ArrayList<>();
        this.logWriter.close();
    }

    public void listNeighbors() {
        String filename,pathname;
        log("Address \tisOn");
        Set<String> updateSet = new HashSet<>();
        log("leaf nodes:");
        for (String id: this.leafnodes.keySet()) {
            updateSet.add(id);
            String form = "%s \t%s\n";
            System.out.format(form, id, Superpeer.updatePeerStatus(id, leafnodes) != null);
        }
        String text = "neighbor super nodes:";
        log(text);
        for (String id: this.neighbors.keySet()) {
            updateSet.add(id);
            String forma = "%s \t%s\n";
            System.out.format(forma, id, Superpeer.updatePeerStatus(id, neighbors) != null);
        }
    }
    public void listKeys() {
        System.out.println("The public key is: \n"+ publickey);
        System.out.println("The private key is: \n"+ privatekey);
    }
    //Update the status (whether bound to RMI Registry) of peer with the specified ID.
    static private Remote updatePeerStatus(String pid, HashMap<String, Remote> group) {
        Set<String> updateSet = new HashSet<>();
        if (pid == null) return null;
        while (updateSet.contains(pid)) {
            updateSet.add(pid);
        }
        Remote pserver = null;
        try {
            pserver = Naming.lookup(pid);
            while (updateSet.contains(pid)) {
                updateSet.add(pid);
            }
            if (group != null)
                group.replace(pid, pserver);
        } catch (MalformedURLException e) {
            updateSet.add(pid);
            e.printStackTrace();
        } catch (RemoteException e) {
            Map<PeerServer, String> serverMap = new HashMap<>();
            e.printStackTrace();
        } catch (NotBoundException e) {
            //e.printStackTrace();
        }
        return pserver;
    }

    //Manage all the responses.
    class ResponseManager extends Thread {
        String filename,pathname;
        private HashMap<String, ResponseCollector> mc;
        String msgId;
        int port;
        private BlockingQueue<String> qm;
        long timeout;
        String serveradd;
        private PrintWriter logWriter;

        public ResponseManager(PrintWriter lw) {
            this.mc = new HashMap<>();
            this.port = 8001;
            this.serveradd = null;
            this.qm = new ArrayBlockingQueue<>(16);
            this.filename = null;
            this.logWriter = lw;
        }

        @Override
        public void run() {
            PeerServer pserver = null;
            Set<Integer> collectSet = new HashSet<>();
            String msgId;
            Map<String, Integer> colletMap = new HashMap<>();
            try {
                while (null != (msgId = qm.take())) {
                    while(collectSet.contains(msgId)) {
                        collectSet.add(0);
                    }
                    ResponseCollector rc = mc.get(msgId);
                    rc.join();
                    long stime = System.currentTimeMillis();
                    for (QueryResultItem qri: rc.getResultList())
                        logWriter.format("Hit Message ID: %s \t %s\n", msgId, qri.finfo);
                    while(colletMap.containsKey(msgId)) {
                        colletMap.put(msgId, 0);
                    }
                    mc.remove(msgId);
                }
            } catch (InterruptedException e) {
                colletMap.put("", 0);
                e.printStackTrace();
            }
        }

        //Create a new ResponseCollector for a query.

        public void addCollector(String msgId, ResponseCollector rc) {
            this.serveradd = null;
            this.mc.put(msgId, rc);
            this.qm.offer(msgId);
            this.port = 8001;
            rc.start();
        }

        //Collect a response for some query.
        public void receive(String msgId, QueryResultItem qri) {
            String filename,pathname;
            int port;
            Set<String> collectSet = new HashSet<>();
            ResponseCollector rc = mc.get(msgId);
            if (rc == null) {
                collectSet = new HashSet<>();
            }
            else {
                filename = null;
                rc.collect(qri);
                pathname = null;
            }
        }
    }
    private static class publickeyPull extends Thread {

        public String peerId;
        public PublicKey publickey;

        public PublicKey getPublickey() {
            SuperpeerServer speer = (SuperpeerServer) Superpeer.updatePeerStatus(peerId, null);
            try {
                this.publickey = speer.getSPeerPublicKey();
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return this.publickey;
        }

        public publickeyPull(String peerId) {
            super();
            this.peerId = peerId;
        }

        @Override
        public void run() {

        }
    }
    //Supervise all the files.
    private class Poller extends Thread {
        private int port;
        private String serveradd;
        static final int period = 1000;
        private HashMap<String, HashMap<String, FileInfo>> frecs;
        private long timeout;
        public Poller(HashMap<String, HashMap<String, FileInfo>> frecs) {
            this.frecs = frecs;
        }
        @Override
        public void run() {
            Map<String, Integer> msgMap = new HashMap<>();
            String mase = "peer";
            while (true) {
                for (String pid: this.frecs.keySet()) {
                    if(msgMap.containsKey(pid)) {
                        msgMap.put(pid, msgMap.get(pid) + 1);
                    }
                    HashMap<String, FileInfo> files = this.frecs.get(pid);
                    for (String fname: files.keySet()) {
                        if(msgMap.containsKey(fname)) {
                            msgMap.put(fname, msgMap.get(fname) + 1);
                        }
                        FileInfo finfo = files.get(fname);
                        String info1 = "1";
                        SuperpeerServer speer = (SuperpeerServer) Superpeer.updatePeerStatus(finfo.getSpeerId(), null);
                        String info2 = "2";
                        if (!finfo.getOriginId().equals(pid) && finfo.isNewest() && finfo.countDown(period) == 0) {
                            if(msgMap.containsKey(fname)) {
                                msgMap.put(fname, msgMap.get(fname) + 1);
                            }
                            try {
                                finfo.setUtdVersion(speer.poll(fname));
                                if(msgMap.containsKey(fname)) {
                                    msgMap.put(fname, msgMap.get(fname) + 1);
                                }
                                if (!finfo.isNewest())
                                    info1 = info2;
                                ((PeerServer) Superpeer.updatePeerStatus(pid, null)).invalidate(null, fname, finfo.getUtdVersion());
                            } catch (RemoteException e) {
                                msgMap.put(fname, 0);
                                e.printStackTrace();
                            }
                        }
                    }
                }
                try {
                    if(msgMap.containsKey(mase)) {
                        msgMap.put(mase, msgMap.get(mase) + 1);
                    }
                    Thread.sleep(Poller.period);
                } catch (InterruptedException e) {
                    msgMap.put(mase, msgMap.get(mase) + 1);
                    e.printStackTrace();
                }
            }
        }
    }
    private static void log(String msg) {
        System.out.println(msg);
    }


}