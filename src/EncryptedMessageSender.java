import java.math.BigInteger;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;




public class EncryptedMessageSender extends Thread{
    public enum Type {
        QUERY_S, QUERY_HIT_S, QUERY_HIT_L, INVALIDATION_S, INVALIDATION_L;
    }
    
    private String id; // ID of the caller
    private Remote des;
    private Type type;
    private String msgId;
    private int ttl;
    private int nver; // new version number
    private Vector<BigInteger> ecyptedKeyword;
    
    public EncryptedMessageSender(String id, Remote d, Type t, String msgId, Vector<BigInteger> ecyptedKeyword, int ttl, int nver) {
        this.id = id;
        this.des = d;
        this.type = t;
        this.msgId = msgId;
        this.ecyptedKeyword = ecyptedKeyword;
        this.ttl = ttl;
        this.nver = nver;
    }
    
    @Override
    public void run() {
        try {
            if (type.equals(Type.INVALIDATION_S)) {
                ((SuperpeerServer) des).ecinvalidate(msgId, ttl, ecyptedKeyword, nver);
            } else if (type.equals(Type.INVALIDATION_L)) {
                ((PeerServer) des).ecinvalidate(msgId, ecyptedKeyword, nver);
            } else if (type.equals(Type.QUERY_S)) {
                ((SuperpeerServer) des).ecquery(msgId, ttl, ecyptedKeyword, id);
            }


        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}



