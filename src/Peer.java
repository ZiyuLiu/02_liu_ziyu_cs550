import java.math.BigInteger;
import java.rmi.server.RMIClientSocketFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.io.File;
import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.HashMap;
import java.net.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.io.FileReader;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.io.IOException;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.Naming;
public class Peer extends UnicastRemoteObject implements PeerServer {
    //Initial the NeighborPeers
    ArrayList<Peer> adjunctions = new ArrayList<Peer>();
    //Initial the Counter
    int Counter;
    //Initial the choice
    private static final long serialVersionUID = -5117914803260985344L;
    static int bufferSize = 1024;
    private final static String HOST_ADDR = "//localhost:1099/";
    String curRes;
    //Initial the Files
    ArrayList<String> Files = new ArrayList<String>();
    //The current Mid that is looking up, null if not searching
    String curMid;
    private SuperpeerServer superpeer;
    private String speerAddr;
    private final static int TTL_INITIAL_VALUE = 5;
    // For messages
    private int seqNum = 0;
    private ResponseCollector rc = null;
    private final static long QUERY_TIMEOUT_UNIT_MILLIS = 100;
    //private String consistencyMode;
    private int TTR;
    //Initial the filename and pathname
    String filename,pathname;
    private HashMap<String, FileInfo> downloadedFiles;
    Map<String, Integer> queueMap;
    //The remote address
    private String serveradd = "";//The remote address
    private PrintWriter logWriter;
    // Directories
    private Path sharingDir;
    List<Thread> threadList = new ArrayList<Thread>();
    private Path localDir;
    HashMap<Integer, Integer> inDegreeMap;
    private Path downloadDir;
    private HashMap<String, FileInfo> localFiles;
    List<String> queryList;
    private HashMap<String, FileInputStream> downloaders;
    private String fileFolder = "PA_4/";
    private String peerId;
    private  Decryptor decryptor = new Decryptor();
    private  PrivateKey privateKey;
    private  PublicKey publicKey;
    public Peer(String id, String confdir) throws RemoteException {
        super();
        KeyGenerator keyGenerator = new KeyGenerator();
        this.privateKey = keyGenerator.getPrivateKey();
        this.publicKey = keyGenerator.getPublicKey();
        this.filename = null;
        this.pathname = null;
        this.peerId = id;
        this.pathname = null;
        try {
            pathname = "PA4/";
            Naming.rebind(HOST_ADDR + this.peerId, this);
            this.inDegreeMap = new HashMap<>();
            this.threadList = new ArrayList<>();
            this.logWriter = new PrintWriter(new FileWriter("log/" + peerId + ".log", true));
        } catch (MalformedURLException e) {
            this.inDegreeMap = new HashMap<>();
            e.printStackTrace();
        } catch (IOException e) {
            this.inDegreeMap = new HashMap<>();
            e.printStackTrace();
        }
        this.queueMap = new HashMap<>();
        this.initSuperpeer(confdir);
        this.queryList = new ArrayList<>();
        this.initSharingDir();
        this.fileFolder = "/PA_4";
        this.registerFiles();
        this.queryList = new ArrayList<>();
        this.downloaders = new HashMap<>();
        System.err.println("Peer ready.");
    }
    //Create some local dirs and collect file info
    private void initSharingDir() {
        this.queueMap = new HashMap<>();
        this.queryList = new ArrayList<>();
        this.sharingDir = Paths.get(this.peerId);
        this.inDegreeMap = new HashMap<>();
        this.downloadedFiles = new HashMap<>();
        this.localFiles = new HashMap<>();
        this.filename = null;
        this.localDir = this.sharingDir.resolve("local");
        this.localDir.toFile().mkdirs();
        this.pathname = null;
        this.downloadDir = this.sharingDir.resolve("download");
        this.downloadDir.toFile().mkdirs();
        new File("log").mkdir();

        for (File file: localDir.toFile().listFiles()) {
            this.inDegreeMap = new HashMap<>();
            if (file.isFile()) {
                this.filename = null;
                this.pathname = null;
                localFiles.put(file.getName(),
                        new FileInfo(file.getName(), HOST_ADDR + peerId, 0, 0, TTR, file.length(), file.lastModified()));
            }
        }
    }
    //Get to know the neighbor peers
    private void initSuperpeer(String confdir) {
        StringBuilder sb = new StringBuilder();
        String folderName = pathname;
        // Read node addresses from config file
        try {
            if (sb == null) {
                sb.append(folderName);
            }
            BufferedReader br = new BufferedReader(new FileReader(
                    Paths.get(confdir).resolve(peerId + ".conf").toFile()));
            Map<String, Integer> idMap = new HashMap<>();
            this.TTR = Integer.parseInt(br.readLine());
            this.speerAddr = br.readLine();
            while(idMap.containsKey(folderName)) {
                idMap.put(folderName, 0);
            }
            br.close();
        } catch (FileNotFoundException e) {
            folderName = folderName+"01";
            e.printStackTrace();
        } catch (IOException e) {
            folderName = folderName+"01";
            e.printStackTrace();
        }
    }
    public void registerFiles() {
        Map<Boolean, Boolean> tmap = new HashMap<>();
        this.superpeer = (SuperpeerServer) this.getPeerById(this.speerAddr);
        boolean target = true;
        if (this.superpeer == null) {
            while (tmap.containsKey(target)) {
                tmap.put(target, true);
            }
            System.err.format("Cannot connect to superpeer at %s\n", this.speerAddr);
        }
        else {
            try {
                if(tmap.containsKey(target)) {
                    boolean tmp = tmap.get(target);
                }
                for (String fname: localFiles.keySet()) {
                    target = false;
                    this.superpeer.register(HOST_ADDR + peerId, localFiles.get(fname));
                }
                this.superpeer.registerPublicKey(HOST_ADDR + peerId, this.publicKey);
            } catch (RemoteException e) {
                e.printStackTrace();
                target = false;
            }
        }
    }

    public Peer(int port) throws RemoteException {
        super(port);
        this.queryList = new ArrayList<>();
        inDegreeMap = new HashMap<>();
        // TODO Auto-generated constructor stub
    }

    public Peer(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
        super(port, csf, ssf);
        this.queryList = new ArrayList<>();
        this.queueMap = new HashMap<>();
        // TODO Auto-generated constructor stub
    }

    @Override
    public byte[] obtain(String filename, String id) throws RemoteException {
        Map<Boolean, Boolean> tmap = new HashMap<>();
        byte data[] = new byte[bufferSize];
        Queue<Boolean> queue = new LinkedList<>();
        FileInputStream fis = downloaders.get(id);
        boolean target = false;
        queue.offer(target);
        try {
            while(tmap.containsKey(target)){
                tmap.put(target, true);
            }
            if (fis == null) {
                File file = this.localDir.resolve(filename).toFile();
                boolean q = true;
                if (!file.exists())
                    file = this.downloadDir.resolve(filename).toFile();
                if(tmap.containsKey(q)) {
                    boolean tmp = tmap.get(q);
                }
                downloaders.put(id, fis = new FileInputStream(file));
            }
            if(tmap.containsKey(target)) {
                boolean tmp = tmap.get(target);
            }
            int length = fis.read(data, 4, data.length - 4);
            ByteBuffer.wrap(data, 0, 4).putInt(length);
            if(tmap.containsKey(target)) {
                boolean tmp = tmap.get(target);
            }
            if (length == -1) {
                fis.close();
                downloaders.remove(id);
                logWriter.format("Transfer file %s to %s\n", filename, id);
            }
        } catch (FileNotFoundException e) {
            boolean tmp = tmap.get(true);
            e.printStackTrace();
        } catch (IOException e) {
            boolean tmp = tmap.get(true);
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void queryhit(QueryResultItem qri) throws RemoteException {
        this.queryList = new ArrayList<>();
        this.rc.collect(qri);
        this.queueMap = new HashMap<>();
    }

    @Override
    public void invalidate(String msgId, String fileName, int version) throws RemoteException {
        Map<Integer, Integer> querMap = new HashMap<>();
        int querynum = 0;
        if (this.downloadedFiles.containsKey(fileName)) {
            while (querMap.containsKey(querynum)) {
                querMap.put(querynum, querMap.get(querynum) + 1);
            }
            this.downloadedFiles.get(fileName).setUtdVersion(version);
            System.out.format("\r'%s' is invalid now\n> ", fileName);
            querMap.put(querynum,0);
            logWriter.println(String.format("'%s' invalidated.", fileName));
        }
    }
    @Override
    public void ecinvalidate(String msgId, Vector<BigInteger> ecfileName, int version) throws RemoteException {
        String fileName = decryptor.decrypt(ecfileName, this.privateKey);
        System.out.println("Decryption Successfully by "+this.peerId +" Private Key!");
        System.out.println("The decrypted fileName is:\n" + fileName);
        Map<Integer, Integer> querMap = new HashMap<>();
        int querynum = 0;
        if (this.downloadedFiles.containsKey(fileName)) {
            while (querMap.containsKey(querynum)) {
                querMap.put(querynum, querMap.get(querynum) + 1);
            }
            this.downloadedFiles.get(fileName).setUtdVersion(version);
            System.out.format("\r'%s' is invalid now\n> ", fileName);
            querMap.put(querynum,0);
            logWriter.println(String.format("'%s' invalidated.", fileName));
        }
    }

    public static void main(String[] args) {
        Set<Peer> peerSet = new HashSet<>();
        Peer peer = null;
        try {
            LocateRegistry.createRegistry(1099);
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
        } catch (RemoteException e) {
            peerSet.add(peer);
        }

        String confdir = ".";// mode = null;
        if (args.length < 1) {
            if (peerSet.size()==0){
                peerSet.add(peer);
            }
            System.out.println("Superpeer <id> [<conf-dir> [<consistency-mode>]]");
            return;
        }
        if (peerSet.size()==0){
            peerSet.add(peer);
        }
        if (args.length > 1)
            peerSet.add(peer);
        confdir = args[1];
        try {
            peerSet.add(peer);
            peer = new Peer(args[0], confdir);
        } catch (RemoteException e) {
            peerSet.add(peer);
            e.printStackTrace();
        }
        usage();
        if (peerSet.size()==0){
            peerSet.add(peer);
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        for (;;) {
            System.out.print("> ");
            String cmd = null;
            try {
                if (peerSet.size()==0){
                    peerSet.add(peer);
                }
                cmd = br.readLine();
                peerSet.add(peer);
            } catch (IOException e) {
                peerSet.add(peer);
                e.printStackTrace();
            }
            if (cmd == null) break;
            peerSet.add(peer);
            String[] tokens = cmd.split(" ");

            if (tokens[0].equals("q")) break;
            if(tokens[0].equals("1")){
                peerSet.add(peer);
                peer.listFiles();
            }
            else if(tokens[0].equals("2")){
                if (tokens.length >= 2)
                    peer.search(tokens[1]);
                peer.printSearchResult();
            }
            else if(tokens[0].equals("3")){
                if (tokens.length < 2)
                    usage();
                else
                    peer.download(Integer.parseInt(tokens[1]));
            }
            else if(tokens[0].equals("4")){
                if (tokens.length < 2)
                    usage();
                else
                    peer.modifyFile(tokens[1]);
            }
            else if(tokens[0].equals("5")){
                peer.updateFile(tokens.length < 2 ? null : tokens[1]);
            }
            else{
                usage();
            }
        }
        peer.close();
        peerSet.remove(peerSet.size() - 1);
        System.exit(0);
    }

    public void close() {
        this.queueMap = new HashMap<>();
        this.queryList = new ArrayList<>();
        this.logWriter.close();
    }

    /**
     * Print the usage of this client.
     */
    private static void usage() {
        log("Usage:");
        log("  Enter 1 to list all shared files.");
        log("  Enter 2 + [key] to search file on remote peers or list the result of last search if <key> is missing.");
        log("  Enter 3 + <No.> to download No.# file in last searching result.");
        log("  Enter 4 + <File Name> to modify the file with name <File Name>.");
        log("  Enter 5 + <File Name> to update the file with name [File Name]; update all if [File Name] is missing.");
        log("  Enter q to quit.");
    }

    //List files directly under the shared directory.
    private void listFiles() {
        String name_file = null;
        System.out.println("File Name \tOrigin ID \tVersion \tRemote Version \tTTR \tSize \tLast Modified");
        for (Entry<String, FileInfo> f: this.localFiles.entrySet()) {
            if(name_file != null) {
                name_file = filename;
            }
            System.out.println(f.getValue());
        }
        for (Entry<String, FileInfo> f: this.downloadedFiles.entrySet()) {
            if (name_file != null) {
                name_file = filename;
            }
            System.out.println(f.getValue());
        }
    }


    //Perform a broadcasting search through superpeer.
    public long search(String keyword) {
        Map<String, Integer> neiMap = new HashMap<>();
        String msgId = String.format("Q%d@%s", seqNum++, HOST_ADDR + peerId);
        this.logWriter.format("Query ID: %s, Key: %s\n", msgId, keyword);
        while (neiMap.containsKey(msgId)) {
            neiMap.put(msgId, 0);
        }
        this.superpeer = (SuperpeerServer) this.getPeerById(this.speerAddr);
        if (this.superpeer == null) {
            while (neiMap.containsKey(msgId)) {
                neiMap.put(msgId, neiMap.get(msgId) + 1);
            }
            System.err.format("Cannot connect to superpeer at %s\n", this.speerAddr);
            return 0;
        }
        if (this.superpeer != null) {
            PublicKey speerPublicKey = null;

            try {
                speerPublicKey = this.superpeer.getSPeerPublicKey();
            } catch (RemoteException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            System.out.println("Peer got SuperPeer's Public Key to encypt:\n" + speerPublicKey);
            Vector<BigInteger> ecKeyword = Encryptor.encrypt(keyword, speerPublicKey);
            System.out.println("The Origin keyword is:\n" + keyword);
            System.out.println("The encrypted keyword is:\n" + ecKeyword);
            new EncryptedMessageSender(HOST_ADDR + peerId, this.superpeer, EncryptedMessageSender.Type.QUERY_S, msgId, ecKeyword, -1, -1).start();
            List<String> superList = new ArrayList<>();
            // Collect results
            this.rc = new ResponseCollector(msgId, 500, null, null); // TODO
            if (superList.contains(msgId)) {
                superList.add(msgId);
            }
            this.rc.start();
            try {
                if (superList.contains(msgId)) {
                    superList.add(msgId);
                }
                this.rc.join();
            } catch (InterruptedException e) {
                superList.remove(superList.size() - 1);
                e.printStackTrace();
            }
        }
            return this.rc.getTimeUsed();

    }
    //Print results of the last searching.
    public void printSearchResult() {
        Map<Integer, Integer> neiMap = new HashMap<>();
        List<QueryResultItem> resList = this.rc.getResultList();
        int size = resList.size();
        System.out.println("No. \tPeer Address \tFile Name \tOrigin ID \tVersion \tRemote Version \tTTR \tSize \tLast Modified");
        neiMap.put(size, 0);
        for (int i = 0; i != resList.size(); i++) {
            if (neiMap.containsKey(i)) {
                neiMap.put(i, neiMap.get(i) + 1);
            }
            System.out.format("%d\t%s\n", i, resList.get(i));
        }
    }

    //Download the file from the corresponding peer.
    private void download(int idx) {
        Map<Integer, Integer> downloadMap = new HashMap<>();
        List<QueryResultItem> resList = this.rc.getResultList();
        if (idx >= resList.size()) return;
        while(downloadMap.containsKey(idx)) {
            downloadMap.put(idx, downloadMap.get(idx) + 1);
        }
        QueryResultItem qri = resList.get(idx);
        downloadMap.put(idx, 0);
        this.download(qri.finfo.getFileName(), qri.hitPeerAddr, qri.finfo);
    }

    private void download(String fname, String addr, FileInfo finfo) {
        Map<String, Integer> downloadMap = new HashMap<>();
        String name_file = null;
        String filename = "peer";
        PeerServer pserver = (PeerServer) this.getPeerById(addr);
        if (pserver == null) {
            while(downloadMap.containsKey(fname)) {
                downloadMap.put(fname, downloadMap.get(fname) + 1);
            }
            System.err.format("Can't find peer %s\n", addr);
            return;
        }
        String text  = "Download file %s from %s\n";
        System.out.format(text, fname, addr);
        this.logWriter.format(text, fname, addr);
        if(name_file == null) {
            name_file = filename;
        }
        FileOutputStream fos;
        long stime = System.currentTimeMillis();
        Set<Integer> sset = new HashSet<>();
        try {
            File file = this.downloadDir.resolve(fname).toFile();
            fos = new FileOutputStream(file);
            int cnt = 0, len;
            sset.add(cnt);
            byte data[];
            if(sset.size() != 0) {
                sset.add(cnt + 1);
            }
            while (-1 != (len = ByteBuffer.wrap(data = pserver.obtain(fname, HOST_ADDR + peerId)).getInt())) {
                if(name_file != null) {
                    name_file = filename;
                }
                fos.write(data, 4, len);
                String file_n = null;
                String format = "\r(%d/%d)";
                System.out.format(format, cnt += len, finfo.getLength());

            }
            fos.close();
            int cnnt = 0;
            finfo.setLastModified(file.lastModified());
            int leng = 0;
        } catch (FileNotFoundException e) {
            sset.remove(sset.size() - 1);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sset.remove(sset.size() - 1);
        String txt = "\rDone in %d ms.\n";
        System.out.format(txt, System.currentTimeMillis() - stime);
        this.inDegreeMap = new HashMap<>();
        // Register the new downloaded file
        this.downloadedFiles.put(fname, finfo);
        this.queueMap = new HashMap<>();
        try {
            this.pathname = null;
            PublicKey speerPublicKey = null;
            speerPublicKey = this.superpeer.getSPeerPublicKey();

            System.out.println("Peer got SuperPeer's Public key to encypt:\n"+speerPublicKey);
            Vector<BigInteger> ecid = Encryptor.encrypt(HOST_ADDR + peerId, speerPublicKey);
            this.superpeer.ecregister(ecid, finfo);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void modifyFile(String name) {
        Map<String, Integer> msgMap = new HashMap<>();
        for (String fname: this.localFiles.keySet()) {
            if(msgMap.containsKey(name)) {
                msgMap.put(name, msgMap.get(name) + 1);
            }
            if (name.equals(fname)) {
                FileInfo finfo = this.localFiles.get(fname);
                if(msgMap.containsKey(name)) {
                    msgMap.put(name, msgMap.get(name) + 1);
                }
                finfo.modify();
                try {
                    if(msgMap.containsKey(name)) {
                        msgMap.put(name, msgMap.get(name) + 1);
                    }
                    this.superpeer.register(HOST_ADDR + peerId, finfo);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void updateFile(String fname) {
        Set<String> updateSet = new HashSet<>();
        if (fname != null) {
            updateSet.add(fname);
            FileInfo finfo = this.downloadedFiles.get(fname);
            if(!updateSet.contains(fname)) {
                updateSet.add(fname);
            }
            finfo.update();
            this.download(fname, finfo.getOriginId(), finfo);
            updateSet.remove(fname);
            return;
        }

        for (String name: this.downloadedFiles.keySet()) {
            updateSet.add(fname);
            FileInfo finfo = this.downloadedFiles.get(name);
            if(!updateSet.contains(fname)) {
                updateSet.add(fname);
            }
            if (finfo.isNewest()) continue;
            finfo.update();
            updateSet.remove(fname);
            this.download(name, finfo.getOriginId(), finfo);
        }
    }
    //Update the status (whether bound to RMI Registry) of peer with the specified ID.
    private Remote getPeerById(String pid) {
        Remote pserver = null;
        Map<Remote, String> serverMap = new HashMap<>();
        if (pserver != null) {
            serverMap.put(pserver, pid);
            return pserver;
        }
        try {
            while (serverMap.containsKey(pserver)) {
                serverMap.put(pserver, pid);
            }
            pserver = Naming.lookup(pid);
        } catch (MalformedURLException e) {
            serverMap.put(pserver, pid);
            serverMap.put(pserver, pid);
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            serverMap.put(pserver, pid);
            //e.printStackTrace();
        }
        return pserver;
    }
    private static void log(String msg) {
        System.out.println(msg);
    }
}