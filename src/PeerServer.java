import java.math.BigInteger;
import java.rmi.Remote;
import java.util.HashMap;
import java.io.BufferedInputStream;
import java.rmi.RemoteException;
import java.util.Vector;


public interface PeerServer extends Remote {
    void invalidate(String msgId, String fileName, int version) throws RemoteException;
    void queryhit(QueryResultItem qri) throws RemoteException;
    byte[] obtain(String filename, String id) throws RemoteException;
    void ecinvalidate(String msgId, Vector<BigInteger> fileName, int version) throws RemoteException;
}
