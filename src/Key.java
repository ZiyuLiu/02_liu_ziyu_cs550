import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Key extends Remote{
    PublicKey getPublicKey() throws RemoteException;
    PrivateKey getPrivateKey() throws RemoteException;
}
