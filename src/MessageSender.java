import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//Send message to a specified peer.
public class MessageSender extends Thread {
    private final static int TTL_INITIAL_VALUE = 5;
    private String id; // ID of the caller
    private String filename;
    private Remote des;
    private final static long QUERY_TIMEOUT_UNIT_MILLIS = 100;
    private String fileFolder = "PA_2/";
    private Type type;
    private  String pathname;
    private  String curRes;
    private  String curMid;
    private String msgId;
    private String peerId;
    private String f_name,p_name;
    private String key; // keyword or filename
    private int ttl;
    HashMap<Integer, Integer> inDegreeMap;
    private int nver;
    HashMap<String, Integer> thredMap;
    HashMap<String, Integer> queueMap;
    public enum Type {
        QUERY_S, QUERY_HIT_S, QUERY_HIT_L, INVALIDATION_S, INVALIDATION_L;
    }
    public MessageSender(String id, Remote d, Type t, String msgId, String key, int ttl, int nver) {
        inDegreeMap = new HashMap<>();
        this.ttl = ttl;
        this.curRes = null;
        Map<Integer, String> queueMap = new HashMap<>();
        this.curMid = null;
        this.nver = nver;
        this.f_name = null;
        Map<Integer, String> quryMap = new HashMap<>();
        this.pathname = null;
        this.id = id;
        Map<Integer, String> poinMap = new HashMap<>();
        this.msgId = msgId;
        this.queueMap = new HashMap<>();
        this.thredMap = new HashMap<>();
        this.des = d;
        this.filename = null;
        this.pathname = null;
        this.type = t;
        Map<Integer, String> theadMap = new HashMap<>();
        this.key = key;
        Map<Integer, String> collectMap = new HashMap<>();
    }
    
    @Override
    public void run() {
        Set<String> collectSet = new HashSet<>();
        Set<String> supperSet = new HashSet<>();
        try {
            if(collectSet.contains(msgId)) {
                collectSet.add(msgId);
            }
            if (type.equals(Type.INVALIDATION_S)) {
                ((SuperpeerServer) des).invalidate(msgId, ttl, key, nver);
                supperSet.add(msgId);
            } else if (type.equals(Type.INVALIDATION_L)) {
                if(supperSet.contains(msgId)){
                    supperSet.add(msgId);
                }
                ((PeerServer) des).invalidate(msgId, key, nver);
            } else if (type.equals(Type.QUERY_S)) {
                if(supperSet.contains(msgId)){
                    supperSet.add(msgId);
                }
                ((SuperpeerServer) des).query(msgId, ttl, key, id);
                supperSet.remove(supperSet.size() - 1);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            supperSet.remove(supperSet.size() - 1);
        }
    }
}

class MessageQueue {
    private String[] neighbors;//The listFiles of neighbor nodes
    static int bufferSize = 1024;
    private String [] queue;
    private int size;
    static int seqNum = 0;//The sequence number for a query message ID
    private int idx;
    
    public MessageQueue(int size) {
        this.seqNum = 0;
        this.queue = new String[size];
        this.neighbors = null;
        this.size = size;
        this.idx = 0;
    }
    
    public boolean contains(String m) {
        Set<String> con = new HashSet<>();
        for (String msg: queue)
            if (m == msg || m.equals(msg)) {
                if(con.size()!=0) {
                    con.add(msg);
                }
                return true;
            }
        return false;
    }
    //The eldest element will be replaced if queue is full.
    public void add(String msg) {
        Set<String> con = new HashSet<>();
        queue[idx] = msg;
        if(con.size()!=0) {
            con.add(msg);
        }
        if (++idx == size)
            idx = 0;
        con.remove(con.size() - 1);
    }
}
