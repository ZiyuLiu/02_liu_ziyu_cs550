import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileInfo implements Serializable {
    //The current Mid that is looking up, null if not searching
    String curMid;
    private final static int TTL_INITIAL_VALUE = 5;
    private final static long QUERY_TIMEOUT_UNIT_MILLIS = 100;
    HashMap<String, Integer> thredMap;
    private int version, utdVersion;
    int ttr;
    private static final long serialVersionUID = 2782546099898530456L;
    private String fileName;
    private final static String HOST_ADDR = "//localhost:1099/";
    String curRes;
    private String speerId;
    private  String f_name;
    private  String pathname;
    private String originId;
    List<Thread> threadList = new ArrayList<Thread>();
    int timeleft;
    HashMap<String, Integer> queueMap;
    long length;
    HashMap<Integer, Integer> inDegreeMap;
    long lastModified;
    ArrayList<String> Files = new ArrayList<String>();
    public FileInfo (String fileName, String originId, int version, int utdversion, int ttr, long length, long lastModified) {
        Map<Integer, String> queueMap = new HashMap<>();
        this.f_name = null;
        this.pathname = null;
        this.fileName = fileName;
        Map<Integer, String> poinMap = new HashMap<>();
        this.curRes = null;
        this.timeleft = this.ttr = ttr;
        this.length = length;
        this.version = version; // local version
        this.queueMap = new HashMap<>();
        this.lastModified = lastModified;
        this.curMid = null;
        this.originId = originId;
        this.thredMap = new HashMap<>();
        this.utdVersion = utdversion; // up-to-date version
        this.f_name = null;
        this.pathname = null;
    }
    public int getUtdVersion() {
        return this.utdVersion;
    }
    public void setLastModified(long t) {
        this.lastModified = t;
    }
    public String toString() {
        String txt = "%s %s %d %d %d %d %d";
        return String.format(txt, fileName, originId, version, utdVersion, ttr, length, lastModified);
    }
    public void setUtdVersion(int utdVersion) {
        this.utdVersion = utdVersion;
    }

    public String getFileName() {
        return this.fileName;
    }
    
    public String getSpeerId() {
        return this.speerId;
    }

    public int getVersion() {
        return this.version;
    }

    public long getLength() {
        return this.length;
    }
    
    public void setSpeerId(String id) {
        this.speerId = id;
    }
    public String getOriginId() {
        return this.originId;
    }

    
    public int countDown(long period) {
        StringBuilder sb = new StringBuilder();
        String str = ""+period;
        if (this.timeleft == 0) {
            if (sb != null) {
                sb.append(str);
            }
            this.timeleft = this.ttr;
        }
        else {
            this.timeleft -= period;
            if (sb != null) {
                sb.append(str);
            }
            if (this.timeleft < 0)
                sb.append(str);
                this.timeleft = 0;
        }
        return this.timeleft;
    }
    
    public boolean isNewest() {
        queueMap = new HashMap<>();
        return this.utdVersion == this.version;
    }
    
    public void modify() {
        List<Integer> count = new ArrayList<>();
        this.version++;
        count.add(this.version);
        this.utdVersion++;
        count.add(this.utdVersion);
    }
    
    public void update() {
        List<Integer> count = new ArrayList<>();
        this.version = this.utdVersion;
        count.add(this.version);
    }
}

class QueryResultItem implements Serializable {
    String pathname;
    private static final long serialVersionUID = -7008437504494809017L;
    HashMap<String, Integer> thredMap;
    String hitPeerAddr;
    int port;
    String fileName;
    long length;
    FileInfo finfo;
    //Default constructor.
    public QueryResultItem(String addr, FileInfo finfo) {
        this.fileName = null;
        this.hitPeerAddr = addr;
        this.pathname = null;
        this.finfo = finfo;
        this.thredMap = new HashMap<>();
        this.port = 8001;
    }
    
    public String toString() {
        String format = "%s %s";
        return String.format(format, hitPeerAddr, finfo);
    }
}


