import java.math.BigInteger;
import java.rmi.Remote;
import java.util.HashMap;
import java.io.CharArrayReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Vector;

public interface SuperpeerServer extends Remote {
    void search(String id, String msgId) throws  RemoteException;
    void register(String id, FileInfo finfo) throws RemoteException;
    void ecregister(Vector<BigInteger> id, FileInfo finfo) throws RemoteException;
    String listAllneighbors(QueryResultItem qri, String msgId) throws  RemoteException;
    String supperNode(QueryResultItem qri, String msgId) throws  RemoteException;
    int poll(String fileName) throws RemoteException;
    boolean isvalide(String id, FileInfo finfo) throws  RemoteException;
    void invalidate(String msgId, int ttl, String fileName, int version) throws RemoteException;
    String toString(String msgId)throws RemoteException;
    void queryhit(String msgId, QueryResultItem qri) throws RemoteException;
    boolean isConnected(String id, FileInfo finfo) throws  RemoteException;
    void query(String msgId, int ttl, String key, String upstreamId) throws RemoteException;
    void ecquery(String msgId, int ttl, Vector<BigInteger> ecyptedKeyword, String upstreamId) throws RemoteException;
    PublicKey getSPeerPublicKey() throws RemoteException;

    void ecinvalidate(String msgId, int ttl, Vector<BigInteger> fileName, int version) throws RemoteException;
    void registerPublicKey(String id, PublicKey publickey) throws RemoteException;
}
