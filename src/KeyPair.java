import java.rmi.RemoteException;

public class KeyPair implements Key{
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private KeyGenerator keyGenerator = new KeyGenerator();
    private Encryptor encryptor = new Encryptor();
    private Decryptor decryptor = new Decryptor();
    public KeyPair(){
        this.publicKey = keyGenerator.getPublicKey();
        this.privateKey = keyGenerator.getPrivateKey();
    }

    @Override
    public PublicKey getPublicKey() throws RemoteException {
        return this.publicKey;
    }

    @Override
    public PrivateKey getPrivateKey() throws RemoteException {
        return this.privateKey;
    }
}
