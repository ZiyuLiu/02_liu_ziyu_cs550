﻿150
00:05:32,050 --> 00:05:33,750
霍华德会疯的
{\3c&H000000&\fs30}Howard's gonna lose his mind.

151
00:05:33,750 --> 00:05:35,150
等下，你还没告诉他吗
{\3c&H000000&\fs30}Wait, you haven't told him yet?

152
00:05:35,150 --> 00:05:36,090
没有
{\3c&H000000&\fs30}No.

153
00:05:36,090 --> 00:05:39,620
你先告诉我了吗 哦 伯妮
{\3c&H000000&\fs30}You told me first? Oh, Bernie!

154
00:05:40,790 --> 00:05:43,260
这不应该发生的 我们一直很小心
{\3c&H000000&\fs30}This wasn't supposed to happen-- we were careful.

155
00:05:43,260 --> 00:05:44,230
对啊 我甚至觉得你在哺乳期间
{\3c&H000000&\fs30}Yeah, I didn't even think you could get pregnant

156
00:05:44,230 --> 00:05:45,260
不可能怀孕
{\3c&H000000&\fs30}while you were breastfeeding.

157
00:05:45,260 --> 00:05:47,900
你猜怎么着 你可以
{\3c&H000000&\fs30}Well, guess what? You can.

158
00:05:47,900 --> 00:05:50,500
好吧 你看 这是一件好事
{\3c&H000000&\fs30}Okay, look, look, this is a good thing.

159
00:05:50,500 --> 00:05:51,970
哈雷要有一个小弟弟
{\3c&H000000&\fs30}Halley's gonna have a little brother

160
00:05:51,970 --> 00:05:53,370
或小妹妹一起玩了
{\3c&H000000&\fs30}or sister to play with.

161
00:05:53,370 --> 00:05:55,940
我想画风会非常可爱
{\3c&H000000&\fs30}I guess that would be pretty cute.

162
00:05:55,940 --> 00:05:57,710
对啊 你知道吗 我就是我爸妈的一个意外
{\3c&H000000&\fs30}And, you, know, I was a surprise to my parents,

163
00:05:57,710 --> 00:05:59,140
但我爸说这是他们这辈子遇到的
{\3c&H000000&\fs30}and my dad said it was the best thing

164
00:05:59,150 --> 00:06:00,640
最好的事
{\3c&H000000&\fs30}that ever happened to them.

165
00:06:00,650 --> 00:06:04,320
好吧 或许这个宝宝真的是一个恩赐
{\3c&H000000&\fs30}Okay. Maybe this baby actually is a blessing.

166
00:06:04,320 --> 00:06:06,180
天啊 亲爱的 他当然是
{\3c&H000000&\fs30}Oh, my God, honey, of course it is.

167
00:06:08,350 --> 00:06:09,720
我怎么能再怀孕呢
{\3c&H000000&\fs30}How am I pregnant again?

168
00:06:09,720 --> 00:06:12,120
对啊 你当时咋想的
{\3c&H000000&\fs30}Yeah, what were you thinking?

169
00:06:15,130 --> 00:06:18,300
好了 我办好值机手续了
{\3c&H000000&\fs30}All right, I'm all checked in to my flight.

170
00:06:18,300 --> 00:06:20,000
你要走了宝宝不开心
{\3c&H000000&\fs30}Well, I'm sad you're leaving.

171
00:06:20,000 --> 00:06:22,070
你为什么定了当天往返的机票
{\3c&H000000&\fs30}Why'd you only book a flight for one day?

172
00:06:22,070 --> 00:06:24,770
我来求婚的
{\3c&H000000&\fs30}I came here to propose.

173
00:06:24,770 --> 00:06:26,700
如果你说不 我不想赖在这儿
{\3c&H000000&\fs30}If you'd said no, I wouldn't want to stick around

174
00:06:26,710 --> 00:06:28,970
看着你蠢蠢的脸
{\3c&H000000&\fs30}looking at your stupid face.

175
00:06:30,310 --> 00:06:32,540
呃 提醒你 你的脸只有在“说不”的桥段里
{\3c&H000000&\fs30}Now, mind you, your face is only stupid

176
00:06:32,550 --> 00:06:34,550
才是蠢蠢的
{\3c&H000000&\fs30}in the "No" Version of the story.

177
00:06:34,550 --> 00:06:38,750
但我说了愿意 所以我一生都是这样
{\3c&H000000&\fs30}But I said yes, so I get a lifetime of this.

178
00:06:38,750 --> 00:06:41,350
是啊 聪明脸
{\3c&H000000&\fs30}Yes, you do, smart face.

179
00:06:41,350 --> 00:06:44,320
你为什么不多待几天
{\3c&H000000&\fs30}Why don't you stay a few extra days?

180
00:06:44,320 --> 00:06:46,690
呃 我没带衣服
{\3c&H000000&\fs30}Well, I don't have any other clothes.