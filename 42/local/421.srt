﻿420
00:18:09,900 --> 00:18:12,870
谁想表达反对意见吗
Anybody have anything snarky to say about that?

421
00:18:14,910 --> 00:18:16,960
谅你们也不敢
Didn't think so.

422
00:18:17,910 --> 00:18:19,540
我想多说几句
I'd like to say something.

423
00:18:20,740 --> 00:18:23,380
贝弗利 我知道
Beverly, I know that we don't bring out

424
00:18:23,380 --> 00:18:24,750
我们的感情不是最好的
the best in each other.

425
00:18:24,750 --> 00:18:28,920
但我们的感情却析出了美妙的结晶
But something wonderful did come from our relationship:

426
00:18:28,920 --> 00:18:30,220
就是台上这位年轻人
that young man right there.

427
00:18:31,260 --> 00:18:33,090
完全同意
I couldn't agree more.

428
00:18:35,390 --> 00:18:37,680
简直感人
That's beautiful.

429
00:18:39,730 --> 00:18:42,850
谢谢 好了 我们继续
Thank you. All right, let's continue.

430
00:18:42,850 --> 00:18:45,770
恕我冒昧 我也想说几句
Yeah, excuse me, I need to say something

431
00:18:45,770 --> 00:18:48,770
对这位生命中特殊的人
to someone pretty special, and...

432
00:18:48,770 --> 00:18:50,910
我不想再等了
I just can't wait any longer.

433
00:18:50,910 --> 00:18:52,610
梦果然能成真
It's happening.

434
00:18:52,610 --> 00:18:54,580
莱纳德
Leonard...

435
00:18:58,280 --> 00:19:00,780
我们之间有笑有泪
You and I have our ups and downs.

436
00:19:00,780 --> 00:19:04,170
但我永远都当你是我的亲人
But I have always considered you my family.

437
00:19:04,170 --> 00:19:08,090
不管我们父母是否在搞七捻三
Even before the recent threat of our parents fornicating

438
00:19:08,090 --> 00:19:09,930
像两只皱巴巴的老兔子
like wrinkly old rabbits.

439
00:19:14,600 --> 00:19:18,880
我很少表露 但你们对我无比重要
I don't always show it, but you are of great importance to me.

440
00:19:20,190 --> 00:19:21,890
你们两个人
Both of you.

441
00:19:21,890 --> 00:19:23,470
噢
Oh...

442
00:19:23,470 --> 00:19:25,020
谢谢你
Thank you.

443
00:19:25,030 --> 00:19:26,520
赞
Okay.

444
00:19:26,530 --> 00:19:30,950
我郑重宣布 你们俩结为夫妇
I now pronounce you husband and wife.

445
00:19:30,950 --> 00:19:33,450
以及家里另一位怪怪的小丈夫
And weird other husband who came with the apartment.

446
00:19:45,230 --> 00:19:47,480
谢谢你送我们去机场
Thank you for taking us to the airport.

447
00:19:47,480 --> 00:19:50,410
我们仨在一起的时光 我很开心
Hey, I'm just thrilled we're all getting along for a minute.

448
00:19:50,420 --> 00:19:51,550
我也是
Yeah, me, too.

449
00:19:51,550 --> 00:19:54,200
贝弗利 给你带来困扰 我很抱歉
Beverly, I'm sorry if I upset you.

450
00:19:54,200 --> 00:19:55,850
覆水难收 埃弗瑞德
Water under the bridge, Alfred.