﻿270
00:10:47,520 --> 00:10:50,340
才不是呢 瑪麗是位很好的女士
Oh, not at all. Mary happens to be a wonderful woman.

271
00:10:50,340 --> 00:10:52,640
如果這讓你吃醋了的話 那倒只是福利
And if it antagonizes you, that's just a bonus.

272
00:10:54,850 --> 00:10:57,070
瑪麗 我很抱歉把你捲進來
Mary, I'm sorry you're in the middle of this.

273
00:10:57,070 --> 00:10:59,100
不不 沒什麼好抱歉的
No, no, nothing to be sorry about.

274
00:10:59,100 --> 00:11:01,350
我真心挺喜歡你父親的
I genuinely like your father.

275
00:11:01,360 --> 00:11:04,110
什麼 但他只是個平庸的學者
What? But he's a mediocre academic.

276
00:11:04,110 --> 00:11:07,690
並且根據貝弗利所說 他的床上功夫遜透了
And according to Beverly, his sexual prowess is subpar.

277
00:11:08,830 --> 00:11:12,110
他基本上就是前列腺肥大版的萊納德
He's basically Leonard with a bigger prostate.

278
00:11:14,500 --> 00:11:17,720
你的意思是我老爸配不上你媽嗎
Are you saying that my dad's not good enough for your mom?

279
00:11:17,720 --> 00:11:20,040
是啊 順便還挖苦了一下你
Yes, while also getting in a solid dig at you.

280
00:11:20,040 --> 00:11:21,620
很一語雙關吧
Pretty efficient, huh?

281
00:11:22,880 --> 00:11:25,710
這太荒唐了 我要去對面待會兒
This is ridiculous. I-I'm going across the hall.

282
00:11:25,710 --> 00:11:27,630
憑什麼你拍屁股走人而把我和你爭吵不休的父母
But why should you get to go and leave me here

283
00:11:27,630 --> 00:11:28,630
留在一起
with your bickering parents?

284
00:11:28,630 --> 00:11:29,880
好吧 那你走啊
Fine, then you go!

285
00:11:29,880 --> 00:11:32,020
算了 我不想和她待在一起 我走吧
Well, I don't want to stay here with her. I'll go.

286
00:11:32,020 --> 00:11:32,930
我和你一起走
I'll go with you.

287
00:11:32,940 --> 00:11:34,190
這樣我還是被和謝爾頓丟在一起啊
That still leaves me here with him.

288
00:11:34,190 --> 00:11:35,720
等一下 等一下
Hang on, hang on!

289
00:11:35,720 --> 00:11:37,220
我們這麼聰明 一定能想出解決辦法的
We're smart, we can figure this out.

290
00:11:37,220 --> 00:11:40,690
瑪麗和貝弗利不能共處一室
Mary and Beverly can't be together.

291
00:11:40,690 --> 00:11:43,860
埃弗瑞德和貝弗利不能待在一起
Uh, Alfred and Beverly can't be together.

292
00:11:43,860 --> 00:11:46,360
萊納德和我不能待在一起
Leonard and I can't be together.

293
00:11:46,370 --> 00:11:49,120
所以 我可以和埃弗瑞德
Now, I could be with Alfred...

294
00:11:49,120 --> 00:11:50,740
但是我不喜歡他那張老臉
but I don't like his face.

295
00:11:52,320 --> 00:11:53,740
哦我知道了
Oh, here! I've got it!

296
00:11:53,740 --> 00:11:55,410
誰準備好參加婚禮了
Who's ready for a wedding?

297
00:11:55,410 --> 00:11:57,740
棒極了 又加了一個人 要重新來過了
Great, now I have to start all over.

298
00:12:05,950 --> 00:12:08,120
所以製藥公司給你的待遇怎麼樣
So how's the world of pharmaceuticals treating you?

299
00:12:08,120 --> 00:12:09,790
挺不賴的 事實上我剛被分配去負責
Pretty good. I actually just got assigned

300
00:12:09,790 --> 00:12:11,170
一個比原來環境好得多的街區
a much better territory.