﻿220
00:08:43,220 --> 00:08:45,430
所以 你到底睡了我老媽沒有
So, did you defile my mother or not?

221
00:08:46,390 --> 00:08:48,940
謝爾頓
Sheldon!

222
00:08:48,940 --> 00:08:51,020
你太不像話了
You're being rude.

223
00:08:51,030 --> 00:08:52,440
恕我直言 我向你保證
If I may, I can assure you,

224
00:08:52,440 --> 00:08:53,890
我們只是搭了同一輛的士並且聊了會兒天
your mother and I did nothing more

225
00:08:53,890 --> 00:08:56,110
除此之外什麼都沒做
than share a cab and a conversation.

226
00:08:56,110 --> 00:08:58,280
那你們的聊天內容有沒有涉及
Did that conversation include the phrase

227
00:08:58,280 --> 00:09:00,450
"你的小枴杖真是順手啊"
"Your genitals are a joy to behold"?

228
00:09:00,450 --> 00:09:02,650
夠了
That's enough!

229
00:09:02,650 --> 00:09:07,620
聽著 我向你發誓 我絕對沒有 也沒有其他人 說過那樣的話
Look, I promise you, neither I, nor anyone, has ever said that.

230
00:09:07,620 --> 00:09:12,290
你一點也不瞭解他的女朋友
You don't know his girlfriend very well.

231
00:09:12,300 --> 00:09:15,410
或者是 真是順手啊我的小枴杖
Or what a joy it is to behold my genitals.

232
00:09:17,580 --> 00:09:20,090
好了 你們大家 把東西放下來
All right, you guys, uh, get settled in,

233
00:09:20,090 --> 00:09:22,340
然後我們就去對面向大家打個招呼
then we'll go across the hall and say hi to everyone.

234
00:09:22,340 --> 00:09:24,810
你知道嗎 真是難以置信我竟然從來沒見過萊納德
You know, It's hard to believe I've never met Leonard.

235
00:09:24,810 --> 00:09:27,090
噢 那可能他的毒品是從
Well, he probably buys his illegal drugs

236
00:09:27,090 --> 00:09:28,260
某個地方小販那裏買來的吧
from a local vendor.

237
00:09:29,680 --> 00:09:31,230
這一點也不好笑 懷特
Not funny, Wyatt.

238
00:09:31,230 --> 00:09:33,770
-我覺得很好笑啊 -謝謝
- I thought it was really funny. - Thanks.

239
00:09:33,770 --> 00:09:37,150
好吧 如果大家都想拿別人的問題開玩笑
Well, fine, if everyone wants to make jokes about our problems,

240
00:09:37,150 --> 00:09:38,100
那我也會
then I can, too.

241
00:09:38,110 --> 00:09:40,440
敲敲 是誰啊
Knock, knock. Who's there?

242
00:09:40,440 --> 00:09:43,160
我們家簡直令人羞恥
Our family is an embarrassment.

243
00:09:43,160 --> 00:09:45,090
那可不是個笑話
That's not much of a joke.

244
00:09:45,090 --> 00:09:45,740
好了
Okay.

245
00:09:45,750 --> 00:09:48,110
聽著 老媽 我知道你很忐忑 但是我向你保證
Listen, Mom, I know you're nervous, but I promise you,

246
00:09:48,120 --> 00:09:50,450
沒人會對你或者我們家評頭論足
no one is gonna judge you or this family.

247
00:09:50,450 --> 00:09:51,950
我很抱歉
Oh, I'm sorry.

248
00:09:51,950 --> 00:09:53,290
只是這是我們第一次見
It's just we're meeting Leonard's parents

249
00:09:53,290 --> 00:09:54,620
萊納德的家人
for the first time,

250
00:09:54,620 --> 00:09:57,790
並且 並且他們都是知識分子而且非常聰明
and-and they're academics and-and intellectuals,

251
00:09:57,790 --> 00:10:00,180
我不想讓他們覺得我們家是白人垃圾
and I don't want them thinking we're white trash.

252
00:10:01,350 --> 00:10:04,600
那 不然呢 你以為他們會覺得我們是什麼顏色的垃圾
Well, what color trash do you think they'll believe?

253
00:10:07,130 --> 00:10:08,970
你怎麼會認為我會和一個剛認識的男人
How could you think that I would spend the night

254
00:10:08,970 --> 00:10:10,390
鬼混一晚
with a man I just met?

255
00:10:10,390 --> 00:10:13,810
那個叫耶穌的男人讓你們在非洲建教堂
A man named Jesus convinced you to build a church in Africa.

256
00:10:13,810 --> 00:10:16,140
你真讓他失望
You're kind of a sucker.

257
00:10:16,140 --> 00:10:19,480
反正什麼也沒發生 對吧
Well, nothing happened, right?

258
00:10:19,480 --> 00:10:20,360
都過去了
I-It's over.

259
00:10:20,360 --> 00:10:21,650
除非我們要結第三次婚
Until we get married a third time,

260
00:10:21,650 --> 00:10:24,570
不然你們再也不用見面了
you guys will never have to see each other again.