﻿150
00:05:44,710 --> 00:05:46,210
什麼事
Like what?

151
00:05:46,210 --> 00:05:47,540
他們知道你弟弟的事嗎
Do they know about your brother?

152
00:05:47,540 --> 00:05:51,460
不是所有的 只知道在監獄和吸毒的事情
Uh, not everything, just, like, the jail and drugs part.

153
00:05:51,460 --> 00:05:53,510
你為什麼那麼做
Why would you do that?

154
00:05:53,510 --> 00:05:54,880
-什麼 -他們不需要
- What? - The world doesn't need

155
00:05:54,880 --> 00:05:56,220
知道我們的問題
to know our problems.

156
00:05:56,220 --> 00:05:57,220
媽媽 對不起 但是
Well, Mom, I'm sorry, but...

157
00:05:57,220 --> 00:05:59,940
嘿 看啊 這兒也有沃爾格林連鎖藥店
Hey, look, they got Walgreens here, too.

158
00:06:01,220 --> 00:06:04,110
你真以為這能轉移話題嗎 懷亞特
You really think it's helpful to change the subject, Wyatt?

159
00:06:04,110 --> 00:06:06,390
親愛的 我只想讓旅途變得愉快些
Just trying to make this a happy trip, dear.

160
00:06:06,390 --> 00:06:08,230
別這麼做了
Well, quit it!

161
00:06:10,360 --> 00:06:12,560
蘭德爾 不敢相信
So, uh, Randall, can't believe

162
00:06:12,570 --> 00:06:14,030
多年後你終於來看我了
after all these years you finally get to visit me

163
00:06:14,030 --> 00:06:15,370
在加利福尼亞
in California.

164
00:06:15,370 --> 00:06:17,290
幸好我是一個非暴力罪犯
Well, good thing I was a nonviolent offender,

165
00:06:17,290 --> 00:06:19,320
否則我都不能離開本州
otherwise I couldn't have left the state.

166
00:06:19,320 --> 00:06:23,240
好了 別再說監獄的事了
All right, that's enough jail talk.

167
00:06:23,240 --> 00:06:26,300
佩妮知道我在哪 她會給我送煙
Penny knows where I was; she sent me cigarettes.

168
00:06:26,300 --> 00:06:29,000
你給你弟弟送煙
You sent your brother cigarettes?

169
00:06:29,000 --> 00:06:30,750
他製作和販賣冰毒 蘇珊
He was cooking and selling crystal meth, Susan,

170
00:06:30,750 --> 00:06:33,920
我覺得吸煙是小事吧
I think we can let the cigarettes slide.

171
00:06:33,920 --> 00:06:35,670
你別再裝酷爸的樣子了
Stop trying to be the cool dad;

172
00:06:35,670 --> 00:06:39,510
你有件T恤上還印了我們家的貓咪呢
you have a shirt with our cat's picture on it.

173
00:06:39,510 --> 00:06:41,980
管他呢 反正我們終於來了 小拳手
Anyway, we're here, slugger.

174
00:06:44,770 --> 00:06:47,650
現在好了 他們都知道我的住址了
That's great, now they know where I live.

175
00:06:47,650 --> 00:06:48,900
你在說什麼
What are you talking about?

176
00:06:48,900 --> 00:06:50,440
他們一直知道你住在哪
They've always known where you live.

177
00:06:50,440 --> 00:06:51,600
是啊 如果你想逃離他們的視線
Yeah, if you want to go off the grid,

178
00:06:51,610 --> 00:06:53,330
你就得搬出你老媽的房子
you have to move out of your mother's house.

179
00:06:55,160 --> 00:06:57,330
我們能不能先談一談
Can we take a moment to discuss

180
00:06:57,330 --> 00:06:59,490
我剛剛為了你向政府官員說謊了
that I just lied to the government for you?

181
00:06:59,500 --> 00:07:00,610
是啊
Yeah.

182
00:07:00,610 --> 00:07:02,780
換做我 我肯定不會為你那樣做的
I would not have done that for you.

183
00:07:03,920 --> 00:07:06,750
霍華德 你就給那人回個電話 看看他到底要幹嘛
Howard, please just call the man, see what he wants.

184
00:07:06,750 --> 00:07:08,670
好 好
All right, all right.

185
00:07:08,670 --> 00:07:10,120
嘿
Hey,

186
00:07:10,120 --> 00:07:12,170
確保你別說漏嘴 他來的時候你並不在家
make sure you tell him that you weren't home when he came by

187
00:07:12,180 --> 00:07:14,180
並且你的印度小夥伴在你剛進門的
and that your Indian friend gave you the message

188
00:07:14,180 --> 00:07:16,850
第一時間就通知了你
the moment you stepped through the door.

189
00:07:16,850 --> 00:07:20,470
好的 你好
Yes, hello. Uh,

190
00:07:20,470 --> 00:07:23,550
我是霍華德·沃羅威茨 找理查德·威廉姆斯上校
this is Howard Wolowitz for Colonel Richard Williams.

191
00:07:23,550 --> 00:07:25,220
我後悔了 乾脆別提到我
Oh, I take it back, don't mention me.

192
00:07:25,220 --> 00:07:28,310
嗨 威廉姆斯上校 您有什麼需要找我的嗎
Hi, Colonel Williams, how can I help you?

193
00:07:28,310 --> 00:07:31,610
什麼 呃 是 他是印度人
What? Oh, uh, yes, he is from India.

194
00:07:33,530 --> 00:07:36,650
不 我不清楚他的移民資格狀況
No, I don't know his immigration status.

195
00:07:39,490 --> 00:07:42,650
放鬆點 我還在等他接電話呢
Relax, I'm still on hold!

196
00:07:44,160 --> 00:07:46,240
你說
Speaking.

197
00:07:46,240 --> 00:07:50,410
好的 當然 我週四可以和你會面
Okay, sure, I can meet with you on Thursday.

198
00:07:50,410 --> 00:07:52,000
就在加州理工會面 可以
Caltech is fine.

199
00:07:52,000 --> 00:07:54,170
呃 我能問問找我有什麼事嗎
Yeah, and may I ask what this is about?

200
00:07:55,550 --> 00:07:57,500
不可以嗎
I may not?

201
00:07:57,500 --> 00:08:00,510
他也是這麼跟我說的
That's what he said to me.

202
00:08:01,640 --> 00:08:02,760
這是你的 老媽
Here you are, Mother.

203
00:08:02,760 --> 00:08:04,390
謝謝
Thank you.

204
00:08:04,400 --> 00:08:06,060
我很開心你能留下來
I'm glad you decided to stay.

205
00:08:06,060 --> 00:08:09,510
能和你共度這樣的特殊日子 對佩妮和我有著非同一般的意義
It's gonna be special for Penny and me to share this with you.

206
00:08:09,520 --> 00:08:13,240
我巴不得這一天趕快結束
I can't wait for this day to be over.

207
00:08:14,070 --> 00:08:16,440
是啊 非同一般 我老媽就是這樣
Yeah, special, like that.

208
00:08:17,270 --> 00:08:19,860
他們來了
That's them.

209
00:08:19,860 --> 00:08:20,860
本來就夠尷尬了
Please don't make things

210
00:08:20,860 --> 00:08:22,830
求你別再雪上加霜了
any more awkward than they already are.