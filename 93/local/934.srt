﻿190
00:07:08,950 --> 00:07:10,180
去还是不去
{\3c&H000000&\fs30}Is that a yes or a no?

191
00:07:10,180 --> 00:07:13,380
苍天啊 给蜜月留点悬念吧
{\3c&H000000&\fs30}Geez, save some for the honeymoon.

192
00:07:16,060 --> 00:07:17,920
看 诺维茨基在那儿
{\3c&H000000&\fs30}Look at Nowitzki over there.

193
00:07:17,920 --> 00:07:19,120
我不敢相信她想从艾米身边
{\3c&H000000&\fs30}I can't believe she tried to steal

194
00:07:19,130 --> 00:07:20,890
撬走谢尔顿
{\3c&H000000&\fs30}Sheldon from Amy.

195
00:07:20,890 --> 00:07:22,530
你猜怎么着
{\3c&H000000&\fs30}You know what?

196
00:07:22,530 --> 00:07:24,430
我要过去告诉她他们订婚了
{\3c&H000000&\fs30}I'm gonna go there and tell her that they're engaged now

197
00:07:24,430 --> 00:07:26,530
她的诡计失败了
{\3c&H000000&\fs30}and that her little plan didn't work.

198
00:07:26,530 --> 00:07:27,730
因为你撑谢尔顿
{\3c&H000000&\fs30}Because you're sticking up for Sheldon,

199
00:07:27,730 --> 00:07:29,730
还是因为你还在为她拒绝你而生气
{\3c&H000000&\fs30}or because you're still mad she rejected you?

200
00:07:29,740 --> 00:07:31,500
太远了 听不到
{\3c&H000000&\fs30}Too far away, can't hear you.

201
00:07:32,740 --> 00:07:33,770
嗨 雷蒙娜
{\3c&H000000&\fs30}Hello, Ramona.

202
00:07:33,770 --> 00:07:35,440
嗨
{\3c&H000000&\fs30}Mm. Hello.

203
00:07:35,440 --> 00:07:38,540
为什么你自己一个人坐
{\3c&H000000&\fs30}Why are you sitting by yourself?

204
00:07:38,540 --> 00:07:39,880
哦 对了
{\3c&H000000&\fs30}Oh, that's right,

205
00:07:39,880 --> 00:07:43,050
谢尔顿在新泽西向艾米求婚
{\3c&H000000&\fs30}Sheldon's in New Jersey being engaged to Amy.

206
00:07:44,050 --> 00:07:45,620
我听说了
{\3c&H000000&\fs30}I heard.

207
00:07:45,620 --> 00:07:47,250
既然谢尔顿出局了
{\3c&H000000&\fs30}Now that Sheldon's out of the picture,

208
00:07:47,250 --> 00:07:51,060
我可以再给你一次跟我约会的机会
{\3c&H000000&\fs30}I could give you one more chance to go out with me.

209
00:07:51,060 --> 00:07:53,260
不 不用了
{\3c&H000000&\fs30}Nope, I'm good.

210
00:07:55,460 --> 00:07:58,030
你确定吗
{\3c&H000000&\fs30}You sure?

211
00:07:58,030 --> 00:08:00,460
我以后不会再邀请了哦
{\3c&H000000&\fs30}I will not ask again.

212
00:08:00,470 --> 00:08:03,030
我衷心希望你不要
{\3c&H000000&\fs30}I sincerely hope not.

213
00:08:03,870 --> 00:08:05,800
很好
{\3c&H000000&\fs30}Very well.

214
00:08:05,810 --> 00:08:08,370
我要趁气氛变尴尬前离开
{\3c&H000000&\fs30}I'm going to leave before this gets awkward.

215
00:08:11,680 --> 00:08:13,640
嘿
{\3c&H000000&\fs30}Hey.

216
00:08:13,650 --> 00:08:14,880
想让我做饭吗
{\3c&H000000&\fs30}Want me to make dinner?

217
00:08:14,880 --> 00:08:17,050
嗯 但你先坐下
{\3c&H000000&\fs30}Uh, sure, but first, why don't you have a seat?

218
00:08:17,050 --> 00:08:18,780
我要给你看样东西
{\3c&H000000&\fs30}There's something I need to show you.

219
00:08:18,780 --> 00:08:22,550
哦 如果是给我看如何做饭就太好了
{\3c&H000000&\fs30}Ooh, if it's how to make dinner, that'd be great.

220
00:08:30,400 --> 00:08:33,500
这是验孕棒吗
{\3c&H000000&\fs30}Is this a... pregnancy test?

221
00:08:33,500 --> 00:08:35,600
是的
{\3c&H000000&\fs30}Yes.

222
00:08:38,900 --> 00:08:42,870
阳性吗
{\3c&H000000&\fs30}That means... positive?

223
00:08:42,880 --> 00:08:45,140
是的
{\3c&H000000&\fs30}Yes.

224
00:08:46,080 --> 00:08:47,950
不会吧
{\3c&H000000&\fs30}No.

225
00:08:51,080 --> 00:08:52,050
是的
{\3c&H000000&\fs30}Yes.

226
00:08:52,050 --> 00:08:55,150
不会的
{\3c&H000000&\fs30}N-- No.

227
00:08:56,020 --> 00:08:58,220
是的
{\3c&H000000&\fs30}Yes.

228
00:08:59,560 --> 00:09:01,860
不要吧
{\3c&H000000&\fs30}No!

229
00:09:05,530 --> 00:09:07,300
这怎么可能呢
{\3c&H000000&\fs30}How could this even happen?

230
00:09:07,300 --> 00:09:09,400
我们一直很小心啊
{\3c&H000000&\fs30}Uh, w-- we were careful.