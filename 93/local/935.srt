﻿240
00:09:24,680 --> 00:09:27,280
这是有点吓人 但是
{\3c&H000000&\fs30}Look, I know it's scary, but...

241
00:09:27,290 --> 00:09:30,750
我们都是能承担起责任的大人了 我们能行的
{\3c&H000000&\fs30}we're both responsible adults, we can do this.

242
00:09:30,760 --> 00:09:32,360
你真的这么认为
{\3c&H000000&\fs30}You really think so?

243
00:09:33,130 --> 00:09:35,790
并没有
{\3c&H000000&\fs30}No!

244
00:09:39,670 --> 00:09:41,670
-你好 斯图尔特 -你好 拉杰
{\3c&H000000&\fs30}- Hey, Stuart. - Oh, hey, Raj.

245
00:09:41,670 --> 00:09:42,730
有什么可以帮到你
{\3c&H000000&\fs30}What can I help you with?

246
00:09:42,740 --> 00:09:44,870
我想买订婚礼物
{\3c&H000000&\fs30}I need to buy an engagement gift.

247
00:09:44,870 --> 00:09:47,810
呃 那你来错地方了
{\3c&H000000&\fs30}Well, you came to the wrong place.

248
00:09:47,810 --> 00:09:49,840
买给谢尔顿和艾米的
{\3c&H000000&\fs30}It's for Sheldon and Amy.

249
00:09:49,840 --> 00:09:52,140
-不是吧 他俩订婚了 -对啊
{\3c&H000000&\fs30}- No way! They're engaged? - Yeah.

250
00:09:52,140 --> 00:09:54,310
这是喜讯啊
{\3c&H000000&\fs30}Well, that's exciting news.

251
00:09:54,310 --> 00:09:56,080
谁能想到谢尔顿和艾米会是下一对
{\3c&H000000&\fs30}Who would've thought Sheldon and Amy would be the next two

252
00:09:56,080 --> 00:09:57,080
喜结连理的夫妇
{\3c&H000000&\fs30}to tie the knot?

253
00:09:57,080 --> 00:09:58,720
可不是嘛 我才是
{\3c&H000000&\fs30}Tell me about it. I'm the one

254
00:09:58,720 --> 00:10:01,790
在莱纳德和佩妮婚礼上抢到捧花的人
{\3c&H000000&\fs30}who caught the bouquet at Leonard and Penny's wedding.

255
00:10:01,790 --> 00:10:03,120
好吧
{\3c&H000000&\fs30}Okay.

256
00:10:03,120 --> 00:10:04,690
呃 我看看
{\3c&H000000&\fs30}Uh... you know,

257
00:10:04,690 --> 00:10:07,160
他们应该会喜欢这个
{\3c&H000000&\fs30}they might like this.

258
00:10:08,860 --> 00:10:11,100
超人和神奇女侠 挺浪漫的
{\3c&H000000&\fs30}Superman and Wonder Woman, it's kind of romantic.

259
00:10:11,100 --> 00:10:12,830


260
00:10:12,830 --> 00:10:14,160
其实呢
{\3c&H000000&\fs30}You know what?

261
00:10:14,170 --> 00:10:16,000
我干嘛给他们买礼物
{\3c&H000000&\fs30}Why am I buying them a gift?

262
00:10:16,000 --> 00:10:16,930
他们已经有爱情了
{\3c&H000000&\fs30}They have love.

263
00:10:16,940 --> 00:10:18,440
去他们的小确幸
{\3c&H000000&\fs30}Screw them and their happiness.

264
00:10:18,440 --> 00:10:20,770
你这里有什么适合苦逼单身狗的么
{\3c&H000000&\fs30}What do you have for someone who's bitter and alone?

265
00:10:22,480 --> 00:10:26,010
屋子里的基本全是吧
{\3c&H000000&\fs30}Literally everything.

266
00:10:27,180 --> 00:10:29,450
谢尔顿 这两位是我研究组的领导
{\3c&H000000&\fs30}Sheldon, these are the heads of my research team.

267
00:10:29,450 --> 00:10:30,580
你们好
{\3c&H000000&\fs30}Oh, hello.

268
00:10:30,580 --> 00:10:31,920
赞恩博士和哈里斯博士
{\3c&H000000&\fs30}Dr. Zane, Dr. Harris,

269
00:10:31,920 --> 00:10:33,650
这是我的未婚夫 谢尔顿·库珀博士
{\3c&H000000&\fs30}this is my fiance, Dr. Sheldon Cooper.

270
00:10:33,650 --> 00:10:35,020
这是我第一次这么叫他
{\3c&H000000&\fs30}That's the first time I've said that

271
00:10:35,020 --> 00:10:36,890
说得我浑身起鸡皮疙瘩
{\3c&H000000&\fs30}and it kind of gave me the goose bumps.

272
00:10:36,890 --> 00:10:39,060
库珀博士
{\3c&H000000&\fs30}Dr. Cooper,

273
00:10:39,060 --> 00:10:41,290
-很高兴见到你 -噢
{\3c&H000000&\fs30}- we are so excited to meet you. - Oh.

274
00:10:41,290 --> 00:10:43,190
这么说太客气了
{\3c&H000000&\fs30}Well, that's very kind of you.

275
00:10:43,200 --> 00:10:46,700
想要的话 饭后我可以给你们的菜单签个名
{\3c&H000000&\fs30}If you'd like, I could autograph your menus after dinner, yeah?

276
00:10:46,700 --> 00:10:48,870
但可别拿去网上拍卖
{\3c&H000000&\fs30}But I better not see those on eBay.

277
00:10:50,140 --> 00:10:51,970
不不不 我们只是很高兴见到
{\3c&H000000&\fs30}No, no, no, we're just excited to meet the man

278
00:10:51,970 --> 00:10:53,800
赢得这位才华横溢的女士芳心的男人
{\3c&H000000&\fs30}who landed this brilliant woman here.

279
00:10:53,810 --> 00:10:54,870
噢
{\3c&H000000&\fs30}Oh!

280
00:10:54,870 --> 00:10:55,940
也没那么难
{\3c&H000000&\fs30}That wasn't hard,

281
00:10:55,940 --> 00:10:57,780
她倒贴我的
{\3c&H000000&\fs30}she threw herself at me.

282
00:10:59,780 --> 00:11:02,180
但是要想宇宙向我展示它的小秘密
{\3c&H000000&\fs30}Now, getting the universe to show me its naughty bits,

283
00:11:02,180 --> 00:11:04,850
那才要费一番功夫
{\3c&H000000&\fs30}that-that took some doing.

284
00:11:04,850 --> 00:11:07,080
谢尔顿是物理学家
{\3c&H000000&\fs30}Sheldon's a physicist.

285
00:11:07,090 --> 00:11:09,990
呵呵 很不错啊
{\3c&H000000&\fs30}Oh. Oh, that's nice.

286
00:11:13,160 --> 00:11:14,760
艾米 最近我读了你的
{\3c&H000000&\fs30}Amy, I recently read your paper on lesions

287
00:11:14,760 --> 00:11:16,960
关于大脑嗅觉受体病变的论文
{\3c&H000000&\fs30}in the olfactory receptors in the brain.

288
00:11:16,960 --> 00:11:18,700
很有见地
{\3c&H000000&\fs30}It was inspired.

289
00:11:18,700 --> 00:11:21,030
看来水平还不算臭啊
{\3c&H000000&\fs30}Oh, well, I guess it didn't stink.

290
00:11:21,030 --> 00:11:23,300
即使臭了 那只老鼠也闻不到
{\3c&H000000&\fs30}But if it did, that rat wouldn't have known it.