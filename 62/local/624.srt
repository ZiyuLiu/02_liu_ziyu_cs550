﻿90
00:02:58,040 --> 00:03:01,090
她在这儿很好的给我们示范了如何装傻
It was nice of her to show us playing dumb with an example.

91
00:03:01,090 --> 00:03:02,510
什么
What?

92
00:03:02,510 --> 00:03:04,210
喔
Oh.

93
00:03:04,210 --> 00:03:06,380
嗨 对不起 我现在该走了
Hi. Okay, hey there, I got-- I'm sorry, I got to go now.

94
00:03:06,380 --> 00:03:09,300
-佩妮 等等 -为什么
- Penny, wait. - Why?

95
00:03:09,300 --> 00:03:12,180
我想感谢你们克服重重困难
I wanted to thank you for going through all the trouble

96
00:03:12,190 --> 00:03:15,440
邀请我来参加婚礼
of planning a second wedding ceremony for me,

97
00:03:15,440 --> 00:03:17,610
但很可惜我不能参加
but unfortunately I cannot attend.

98
00:03:17,610 --> 00:03:19,730
为什么 怎么了
Well, why? What's wrong?

99
00:03:19,730 --> 00:03:21,360
我们还要继续装傻吗
Wha-- are we still doing the dumb thing?

100
00:03:21,360 --> 00:03:23,450
好的 为什么 怎么了
Okay, why, what's wrong?

101
00:03:23,450 --> 00:03:25,610
我只是不能在这儿忍受你爸
I just cannot stay here while your father

102
00:03:25,620 --> 00:03:27,820
故意让我难堪
goes out of his way to humiliate me.

103
00:03:27,820 --> 00:03:30,950
天啊 他怎么羞辱你了
Oh, golly, however did he humiliate you?

104
00:03:30,960 --> 00:03:32,570
别说了 谢尔顿
Stop it, Sheldon.

105
00:03:32,570 --> 00:03:36,080
我应该问别说什么 还是不再说话了
Do I say "Stop what?" Or just throw in the towel?

106
00:03:36,080 --> 00:03:38,290
我不明白为什么我要看着你爸
I don't see why I should have to watch your father

107
00:03:38,300 --> 00:03:41,500
和某个迷信的乡巴佬在一起
parade around with some Bible-thumping bumpkin.

108
00:03:41,500 --> 00:03:43,880
等下 你是在说我妈吗
Oh, excuse me, that is my mother you're talking about,

109
00:03:43,880 --> 00:03:46,090
虽然说得很对
however accurately.

110
00:03:46,090 --> 00:03:47,590
好了 贝弗利
Okay, Beverly,

