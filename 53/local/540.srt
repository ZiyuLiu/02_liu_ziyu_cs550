1
00:00:00,473 --> 00:00:02,174
"生活大爆炸" 前情提要...
Previously on The Big Bang Theory...

2
00:00:02,896 --> 00:00:04,830
当时有个女孩
Well, there was this girl.

3
00:00:05,198 --> 00:00:07,132
你干嘛了?
What did you do?

4
00:00:07,200 --> 00:00:08,133
没什么大事
Nothing.

5
00:00:08,201 --> 00:00:11,102
真的 就亲了一下
Really, it was just kissing.

6
00:00:14,340 --> 00:00:15,940
好吧
All right.

7
00:00:16,008 --> 00:00:18,243
那 我们还是要结婚的吧?
So, we're still getting married?

8
00:00:20,112 --> 00:00:21,279
是啊
Yes.

9
00:00:22,581 --> 00:00:24,549
因为我们彼此相爱
Because we love each other.

10
00:00:24,616 --> 00:00:26,217
是啊
Yes.

11
00:00:26,285 --> 00:00:29,654
而且这是我们最开心的一天
And it's the happiest day of our life.

12
00:00:29,722 --> 00:00:31,022
别得寸进尺
Don't push it.

13
00:00:33,759 --> 00:00:35,927
我需要一点时间
I need some time to take a step back

14
00:00:35,995 --> 00:00:39,164
重新考虑一下我们的关系
and reevaluate our situation.

15
00:00:40,199 --> 00:00:41,800
哦
Oh.

16
00:00:41,867 --> 00:00:43,902
再见 Sheldon
Bye, Sheldon.

17
00:00:46,338 --> 00:00:48,339
咕噜...
Well, Gollum...

18
00:00:50,475 --> 00:00:53,677
你最懂戒指了
you're an expert on rings.

19
00:00:53,745 --> 00:00:56,046
那我该拿这个戒指怎么办呢?
What do I do with this one?

20
00:00:56,745 --> 00:00:58,046
现在...

21
00:00:59,184 --> 00:01:01,251
你想要哪个套餐?
So, what package are you thinking?

22
00:01:01,319 --> 00:01:03,253
这套里面包括了音乐和鲜花
Mm, this one comes with music and flowers.

23
00:01:03,321 --> 00:01:06,690
他们还会把整个视频传到网上
Oh, they even stream the whole thing live on the Internet.

24
00:01:06,758 --> 00:01:08,125
干嘛要传网上?
Why would we want that?

25
00:01:08,193 --> 00:01:10,527
因为有很多漂亮的金发妞
'Cause there's a lot of gorgeous blondes out there

26
00:01:10,595 --> 00:01:13,296
不相信自己会嫁一个又矮又近视的科学家
who don't believe they can land a short, nearsighted scientist.

27
00:01:13,364 --> 00:01:15,832
我们传播点希望吧
Let's give them hope.

28
00:01:15,899 --> 00:01:17,466
随便啦 就传到网上吧
Whatever. Put us on the Internet.

29
00:01:17,534 --> 00:01:20,369
反正我一直想给婚礼加个评论区
I've always wanted a wedding with a comment section.

30
00:01:20,437 --> 00:01:24,673
我... 如果你不喜欢 我们可以先不结婚
I... If you're not into this, we can do it another time.

31
00:01:24,742 --> 00:01:26,042
不不 我想结婚
No. No, I want to.

32
00:01:26,110 --> 00:01:28,211
听着 我们已经拖得够久了
Look, we've put this off long enough.

33
00:01:28,278 --> 00:01:29,712
来吧
Let's do it.

34
00:01:29,780 --> 00:01:32,915
我们第一次滚床单的时候你也这么说
That's exactly what you said the first time we slept together.

35
00:01:34,484 --> 00:01:36,318
抱歉
Oh, excuse me.

36
00:01:36,386 --> 00:01:37,553
是Sheldon
Sheldon.

37
00:01:37,621 --> 00:01:38,754
嘿
Hey.

38
00:01:38,821 --> 00:01:40,688
Leonard 你结婚了吗?
Leonard, have you gotten married yet?

39
00:01:40,757 --> 00:01:41,857
没 干嘛这么问?
No. Why?

40
00:01:41,924 --> 00:01:43,591
- 很好 别结!  - 为什么?
- Good. Don't do it!  - Why not?

41
00:01:43,659 --> 00:01:45,927
我发现了非常重要的情报
Some important new information has come to light.

42
00:01:45,995 --> 00:01:48,930
女人最渣了
Women are the worst.

43
00:01:48,998 --> 00:01:50,832
我以为最疼不过被纸割伤 但我错了
I thought it was paper cuts, but I was wrong.

44
00:01:50,900 --> 00:01:53,701
没有纸能伤我这么深
No piece of paper ever cut me this deep.

45
00:01:53,770 --> 00:01:55,470
出什么事了?
What happened now?

46
00:01:55,537 --> 00:01:58,907
Amy和我分手了
Amy has ended our relationship.

47
00:01:58,975 --> 00:02:00,242
不 真的吗?
Oh, no. Seriously?

48
00:02:00,309 --> 00:02:01,009
出什么事了?
What's going on?

49
00:02:01,077 --> 00:02:02,510
Amy和Sheldon提分手了
Amy broke up with Sheldon.

50
00:02:02,578 --> 00:02:03,744
她提了?
She did?

51
00:02:03,812 --> 00:02:06,213
Penny哭了吗?
Is Penny crying?

52
00:02:06,281 --> 00:02:07,281
没
No.

53
00:02:07,315 --> 00:02:08,082
好吧 当然没有
No, of course not.

54
00:02:08,150 --> 00:02:10,751
她们的快乐建立在我们痛苦之上
They thrive on our suffering.

55
00:02:10,819 --> 00:02:12,552
兄弟... 我很难过
Buddy, I... I'm so sorry.

56
00:02:12,620 --> 00:02:14,554
有什么我能做的?
Is there anything I can do?

57
00:02:14,622 --> 00:02:15,990
有 如果我以后
Yes. If I ever talk about

58
00:02:16,057 --> 00:02:18,259
再说要和女孩子去约会 记得翻白眼
going out with a girl again, roll your eyes at me

59
00:02:18,326 --> 00:02:21,395
就跟你干蠢事 我翻你白眼一样
like I do to you when you say dumb things.

60
00:02:21,462 --> 00:02:22,897
Sheldon 好吧 虽然
Sheldon, uh, o-okay, just because

61
00:02:22,964 --> 00:02:24,364
你和Amy的事情不顺利
you're going through this with Amy

62
00:02:24,432 --> 00:02:26,400
并不意味着所有女人都渣
doesn't mean that all women are bad.

63
00:02:26,467 --> 00:02:28,769
随便了
Whatever.

64
00:02:28,836 --> 00:02:30,136
我才听说
Hey, I just heard

65
00:02:30,204 --> 00:02:31,772
你和Sheldon的事 你还好吗?
about you and Sheldon. Are you okay?

66
00:02:31,839 --> 00:02:32,939
不太好
Not really.

67
00:02:33,007 --> 00:02:34,441
你能过来一下吗?
Can you come over?

68
00:02:34,509 --> 00:02:36,042
嗯 其实我现在在拉斯维加斯
Uh, actually I'm in Vegas.

69
00:02:36,110 --> 00:02:38,244
Leonard和我就要结婚了
Leonard and I are about to get married.

70
00:02:38,313 --> 00:02:39,546
等等
Hold on.

71
00:02:39,614 --> 00:02:42,015
你们结婚居然不邀请我?
You're getting married and you didn't invite me?

72
00:02:42,082 --> 00:02:44,651
我们只是一时兴起
Well, it was kind of a spur-of-the-moment thing.

73
00:02:44,719 --> 00:02:46,052
哇
Wow.

74
00:02:46,120 --> 00:02:49,856
希望我可以在这接到你扔来的花
Hope I can catch the bouquet from here.

75
00:02:49,924 --> 00:02:51,725
Amy 不要这样
Amy, don't be like that.

76
00:02:51,793 --> 00:02:53,727
为什么我听到了Amy的名字?
Why did I just hear Amy's name?

77
00:02:53,795 --> 00:02:54,894
Penny正和她打电话呢
Penny's on the phone with her.

78
00:02:54,962 --> 00:02:56,095
她说我什么了吗?
Did she say anything about me?

79
00:02:56,163 --> 00:02:57,630
没事了 我才不关心呢
Never mind. I don't care.

80
00:02:57,698 --> 00:02:59,498
如果你关心的话你可以告诉我她说了什么
Well, if you care, you can find out and tell me.

81
00:02:59,566 --> 00:03:02,635
当你发现我其实并不关心这些时不要吃惊
Just don't be shocked when you find out that I don't care.

82
00:03:02,703 --> 00:03:03,769
你能不能别这么激动?
Okay, will you relax?

83
00:03:03,838 --> 00:03:05,939
这又不是什么大不了的事
You're not missing anything special.

84
00:03:06,807 --> 00:03:08,574
说什么呢?
Hey?

85
00:03:08,642 --> 00:03:09,642
她很不开心
She's upset.

86
00:03:09,676 --> 00:03:11,077
婚礼将会非常好的
Look, it's gonna be a great wedding.

87
00:03:11,145 --> 00:03:13,947
看你那穿着西服的小样
Look at you in your little suit.

88
00:03:14,014 --> 00:03:15,715
Amy不开心? 是因为我吗?
Amy's upset? Is it about me?

89
00:03:15,782 --> 00:03:17,250
不是 我觉得是因为我们婚礼没邀请她
No, I think it's because we're eloping.

90
00:03:17,318 --> 00:03:19,552
是因为你们的婚礼让她难受吗?
Your marriage is causing her pain?

91
00:03:19,619 --> 00:03:20,853
太棒了 我收回刚才的话
Yeah, great, I take it back.

92
00:03:20,920 --> 00:03:23,356
结吧! 为爱狂欢!
Go ahead and do it. Yay for love!

93
00:03:23,423 --> 00:03:26,892
* 宇宙一度又烫又稠密 *
* Our whole universe was in a hot, dense state *

94
00:03:26,960 --> 00:03:30,862
* 140亿年前终于爆炸了... 等着瞧! *
* Then nearly 14 billion years ago expansion started... Wait! *

95
00:03:30,930 --> 00:03:31,831
* 地球开始降温 *
* The Earth began to cool *

96
00:03:31,898 --> 00:03:34,567
* 自养生物来起哄 穴居人发明工具 *
* The autotrophs began to drool,Neanderthals developed tools *

97
00:03:34,634 --> 00:03:37,202
* 我们建长城 我们建金字塔 *
* We built the Wall We built the pyramids *

98
00:03:37,270 --> 00:03:39,937
* 数学 自然科学 历史 揭开神秘 *
* Math, Science, History,unraveling the mystery *

99
00:03:40,005 --> 00:03:41,840
* 一切由大爆炸开始 *
* That all started with a big bang *

100
00:03:41,907 --> 00:03:43,508
生活大爆炸 第九季第1集

90
00:03:17,318 --> 00:03:19,552
是因为你们的婚礼让她难受吗?
Your marriage is causing her pain?

91
00:03:19,619 --> 00:03:20,853
太棒了 我收回刚才的话
Yeah, great, I take it back.

92
00:03:20,920 --> 00:03:23,356
结吧! 为爱狂欢!
Go ahead and do it. Yay for love!

93
00:03:23,423 --> 00:03:26,892
* 宇宙一度又烫又稠密 *
* Our whole universe was in a hot, dense state *

94
00:03:26,960 --> 00:03:30,862
* 140亿年前终于爆炸了... 等着瞧! *
* Then nearly 14 billion years ago expansion started... Wait! *

95
00:03:30,930 --> 00:03:31,831
* 地球开始降温 *
* The Earth began to cool *

96
00:03:31,898 --> 00:03:34,567
* 自养生物来起哄 穴居人发明工具 *
* The autotrophs began to drool,Neanderthals developed tools *

97
00:03:34,634 --> 00:03:37,202
* 我们建长城 我们建金字塔 *
* We built the Wall We built the pyramids *

98
00:03:37,270 --> 00:03:39,937
* 数学 自然科学 历史 揭开神秘 *
* Math, Science, History,unraveling the mystery *

99
00:03:40,005 --> 00:03:41,840
* 一切由大爆炸开始 *
* That all started with a big bang *

100
00:03:41,907 --> 00:03:43,508
生活大爆炸 第九季第1集


