130
00:05:07,690 --> 00:05:09,824
自助早餐
at the strip club next door.

131
00:05:09,891 --> 00:05:14,128
谢了 不过我不太想吃洒着闪光粉的炒鸡蛋
Thanks, but I don't like glitter on my scrambled eggs.

132
00:05:14,196 --> 00:05:18,199
应该不是脱衣舞娘做的早餐吧 不过无所谓啦
I don't think the strippers prepare the meal, but okay.

133
00:05:19,601 --> 00:05:22,837
那个... 现在这样和我预想的也不太一样
Look, it... it's not how I pictured it either,

134
00:05:22,904 --> 00:05:24,472
不过我还是很高兴我们真的要结婚了
but I'm still glad we're doing it.

135
00:05:24,540 --> 00:05:26,374
- 我也是  - 真的吗?
- Me, too.  - You sure?

136
00:05:26,442 --> 00:05:27,842
- 真的  - 那你能保证
- Yes.  - And you promise

137
00:05:27,909 --> 00:05:29,844
对刚才车上我说的事完全不介意吗?
you're okay with everything from the car?

138
00:05:29,911 --> 00:05:31,611
天啊 你能不能别提了?
Oh, my God, would you stop bringing it up?

139
00:05:31,679 --> 00:05:33,513
你说得对 抱歉
You're right. I'm sorry.

140
00:05:33,581 --> 00:05:35,015
我们有后半辈子的时间
We have the rest of our lives

141
00:05:35,083 --> 00:05:38,319
为过去的事纠缠不休呢
to dredge stuff up from the past and fight about it.

142
00:05:39,587 --> 00:05:40,687
所以 你说呢?
So what do you think?

143
00:05:40,755 --> 00:05:42,589
我们要不要去旁边吃点东西?
Should we run next door and grab a bite?

144
00:05:42,657 --> 00:05:44,124
要是一会叫到我们了怎么办?
What if they call our names?

145
00:05:44,192 --> 00:05:47,694
没关系 他们给我这个震动号牌了
Oh, don't worry. They gave me this vibrating coaster.

146
00:05:47,762 --> 00:05:51,130
还真是跟童话一样美好啊
Oh, and the fairy tale continues.

147
00:05:53,601 --> 00:05:55,569
谢谢你们
Thank you for doing this.

148
00:05:55,636 --> 00:05:56,737
不用谢
Our pleasure.

149
00:05:56,804 --> 00:05:58,004
你感觉好点吗?
You feeling okay?

150
00:05:58,072 --> 00:06:00,073
我们在一起的时间太长了
We were together for so long,

151
00:06:00,140 --> 00:06:02,509
说实话 我自己都不知道现在是什么感觉
I honestly don't know what I'm feeling.

152
00:06:02,577 --> 00:06:04,644
我深表理解
Well, that's understandable.

153
00:06:04,712 --> 00:06:07,180
你忘了 这种感觉就是"快乐"
You forgot. It's called "Happy."

154
00:06:07,248 --> 00:06:08,615
Howard你说什么呢
Howard.

155
00:06:08,683 --> 00:06:09,716
我又没说什么不好的话
I'm not saying anything bad.

156
00:06:09,784 --> 00:06:11,885
只不过是她爱上了束缚住她的人
Just that she was in love with her captor

157
00:06:11,952 --> 00:06:15,221
而现在终于从他黑暗变态的地牢里逃出来了而已
and somehow managed to escape from his dark and crazy dungeon.

158
00:06:16,891 --> 00:06:18,057
我明白你的感受
I know what you're going through.

159
00:06:18,125 --> 00:06:20,526
我... 我上一次分手的时候也很难过
My... my last breakup was pretty tough.

160
00:06:20,594 --> 00:06:22,795
你前女友叫什么?
Oh. What was her name?

161
00:06:22,863 --> 00:06:23,963
嘿 我说的是真的
Hey, it's a true story.

162
00:06:24,031 --> 00:06:26,065
别这样审问我
I don't need the third degree.

163
00:06:31,038 --> 00:06:33,839
神啊 Sheldon 你在这里干什么呢?
For God's sake, Sheldon, what are you doing?

164
00:06:33,908 --> 00:06:35,440
我没想进去
I didn't want to come in.

165
00:06:35,508 --> 00:06:38,610
有人告诉我 如果我进去的话大家都会觉得不舒服
I was told it would make everyone feel uncomfortable.

166
00:06:39,379 --> 00:06:40,578
所以我就在这里待着好了
So I'll just stay out here

167
00:06:40,646 --> 00:06:42,948
还要假装没有很想去洗手间的样子
and pretend that I don't have to go to the bathroom.

168
00:06:46,152 --> 00:06:47,886
Howard 交给你了
Howard, do something.

169
00:06:47,954 --> 00:06:49,988
来了
I'm on it.

170
00:06:56,963 --> 00:07:00,631
Sheldon 出现在这对你没好处啊
Sheldon, you being here might not be making things better.

171
00:07:00,698 --> 00:07:02,599
我发现了
I see.

172
00:07:02,667 --> 00:07:05,202
所以你们才邀请了除了我以外的所有人吗?
And is that why everybody was invited but me?

173
00:07:05,270 --> 00:07:07,204
他们没有邀请除了你以外的所有人
They didn't invite everybody but you.

174
00:07:07,272 --> 00:07:08,772
Bernadette邀请了我而已
Bernadette invited me,

175
00:07:08,840 --> 00:07:10,074
Stuart就住在这里
and Stuart lives here.

176
00:07:10,142 --> 00:07:12,843
谁想吃刚出炉的肉桂卷?
Who wants hot cinnamon rolls?

177
00:07:15,747 --> 00:07:18,649
所以 一会听到的音乐就是你们的提示
So, when you hear the music, that's your cue.

178
00:07:18,716 --> 00:07:19,951
还有什么问题吗?
Any questions?

179
00:07:20,018 --> 00:07:21,919
呃 我们购买的套装里
Oh, the package that we paid for

180
00:07:21,986 --> 00:07:24,520
好像还应该包括地毯上铺满玫瑰花瓣的
said the aisle was supposed to be strewn with rose petals.