﻿444
00:18:19,050 --> 00:18:21,350
谢谢你 谢尔顿
{\3c&H000000&\fs30}Thank you, Sheldon.

445
00:18:21,350 --> 00:18:23,650
然而 我却变成了暴怒的绿巨人
{\3c&H000000&\fs30}Instead, I was like the Hulk, and I...

446
00:18:23,660 --> 00:18:26,190
够了 别再说复仇者联盟了好么
{\3c&H000000&\fs30}Okay, please stop talking about the Avengers.

447
00:18:27,390 --> 00:18:29,290
总而言之
{\3c&H000000&\fs30}Anyway.

448
00:18:29,290 --> 00:18:31,190
我为你感到骄傲
{\3c&H000000&\fs30}I'm proud of you.

449
00:18:31,200 --> 00:18:32,930
我会试着和你分享观众的注意力
{\3c&H000000&\fs30}And I'm going to try to do a better job

450
00:18:32,930 --> 00:18:37,400
因为我们是一个团队
{\3c&H000000&\fs30}of sharing the spotlight because we're a team.

451
00:18:37,400 --> 00:18:42,840
你懂吗 就像道奇队那样
{\3c&H000000&\fs30}You know? Much like t-the Dodgers.

452
00:18:44,710 --> 00:18:49,350
如果他们也有超能力 能打击犯罪
{\3c&H000000&\fs30}If they had superpowers, and fought crime.

453
00:18:49,350 --> 00:18:53,350
而且雷神也是他们一员
{\3c&H000000&\fs30}And Thor was in them.

454
00:18:53,350 --> 00:18:55,250
谢尔顿 我知道这对你来说不容易
{\3c&H000000&\fs30}Sheldon, I know this isn't easy,

455
00:18:55,250 --> 00:18:59,320
但你还有一辈子的时候去练习
{\3c&H000000&\fs30}but you'll have a whole lifetime to practice.

456
00:18:59,320 --> 00:19:03,630
也许真要花一辈子的时间 我对这不在行
{\3c&H000000&\fs30}I-It could take that long, I'm really bad at it.

457
00:19:05,960 --> 00:19:08,060
也许我该从现在行动起来
{\3c&H000000&\fs30}You know, maybe, um,

458
00:19:08,070 --> 00:19:11,700
回去帕萨迪纳
{\3c&H000000&\fs30}I should start right now, and go back to Pasadena

459
00:19:11,700 --> 00:19:14,400
让你好好享受属于自己的时刻
{\3c&H000000&\fs30}and let you have this experience to yourself.

460
00:19:14,410 --> 00:19:16,270
你只是想回去
{\3c&H000000&\fs30}You just want to go back

461
00:19:16,270 --> 00:19:18,540
那个人人稀罕你的地方吧
{\3c&H000000&\fs30}'cause that's where everybody makes a fuss over you.

462
00:19:18,540 --> 00:19:21,880
你的同事们说对了 你真是冰雪聪明
{\3c&H000000&\fs30}You know, your colleagues are right, you are brilliant.

463
00:19:32,700 --> 00:19:34,230
大家好
{\3c&H000000&\fs30}Hello.

464
00:19:34,240 --> 00:19:35,530
-嘿 -欢迎回来 艾米
{\3c&H000000&\fs30}- Hey. - Amy, welcome back.

465
00:19:35,530 --> 00:19:37,700
让我看看戒指
{\3c&H000000&\fs30}Oh, l-let me see the ring.

466
00:19:37,710 --> 00:19:40,210
喔 挺不错啊
{\3c&H000000&\fs30}Ooh, nice.

467
00:19:40,210 --> 00:19:42,510
嘿 非礼勿视
{\3c&H000000&\fs30}H-Hey, her eyes are up there.

468
00:19:44,750 --> 00:19:47,410
就是那个女人亲了谢尔顿吗
{\3c&H000000&\fs30}I-Is that the woman who kissed Sheldon?

469
00:19:47,420 --> 00:19:49,220
呃 可能是吧
{\3c&H000000&\fs30}Uh... Could be.

470
00:19:49,220 --> 00:19:51,720
说不准 说说普林斯顿的事情吧
{\3c&H000000&\fs30}Hard to say. Tell us about Princeton.

471
00:19:51,720 --> 00:19:53,490
等我一下
{\3c&H000000&\fs30}Excuse me for a minute.

472
00:19:53,490 --> 00:19:55,360
不 我们待会儿接着聊
{\3c&H000000&\fs30}Not-- well, we'll catch up later.

473
00:19:55,360 --> 00:19:57,820
这将是继上次我阿姨和我表妹
{\3c&H000000&\fs30}This is going to be the biggest smackdown

474
00:19:57,830 --> 00:19:59,830
在家庭聚会上穿了同一件沙丽之后
{\3c&H000000&\fs30}since my Aunt Noopur showed up at the family reunion

475
00:19:59,830 --> 00:20:02,600
史上最大型的一次正面撕逼
{\3c&H000000&\fs30}wearing the same sari as my cousin Sruti.

476
00:20:03,870 --> 00:20:06,830
你是诺维茨基博士吗
{\3c&H000000&\fs30}Dr. Nowitzki?

477
00:20:06,840 --> 00:20:08,600
噢
{\3c&H000000&\fs30}Oh.

478
00:20:08,600 --> 00:20:12,470
福勒博士 呃 你好
{\3c&H000000&\fs30}Dr. Fowler. Um, hello.

479
00:20:13,310 --> 00:20:15,040
谢谢你 太谢谢你了
{\3c&H000000&\fs30}Thank you. Thank you so much.

444
00:18:19,050 --> 00:18:21,350
谢谢你 谢尔顿
{\3c&H000000&\fs30}Thank you, Sheldon.

445
00:18:21,350 --> 00:18:23,650
然而 我却变成了暴怒的绿巨人
{\3c&H000000&\fs30}Instead, I was like the Hulk, and I...

446
00:18:23,660 --> 00:18:26,190
够了 别再说复仇者联盟了好么
{\3c&H000000&\fs30}Okay, please stop talking about the Avengers.

447
00:18:27,390 --> 00:18:29,290
总而言之
{\3c&H000000&\fs30}Anyway.

448
00:18:29,290 --> 00:18:31,190
我为你感到骄傲
{\3c&H000000&\fs30}I'm proud of you.

449
00:18:31,200 --> 00:18:32,930
我会试着和你分享观众的注意力
{\3c&H000000&\fs30}And I'm going to try to do a better job

450
00:18:32,930 --> 00:18:37,400
因为我们是一个团队
{\3c&H000000&\fs30}of sharing the spotlight because we're a team.

451
00:18:37,400 --> 00:18:42,840
你懂吗 就像道奇队那样
{\3c&H000000&\fs30}You know? Much like t-the Dodgers.

452
00:18:44,710 --> 00:18:49,350
如果他们也有超能力 能打击犯罪
{\3c&H000000&\fs30}If they had superpowers, and fought crime.

453
00:18:49,350 --> 00:18:53,350
而且雷神也是他们一员
{\3c&H000000&\fs30}And Thor was in them.

454
00:18:53,350 --> 00:18:55,250
谢尔顿 我知道这对你来说不容易
{\3c&H000000&\fs30}Sheldon, I know this isn't easy,

455
00:18:55,250 --> 00:18:59,320
但你还有一辈子的时候去练习
{\3c&H000000&\fs30}but you'll have a whole lifetime to practice.

456
00:18:59,320 --> 00:19:03,630
也许真要花一辈子的时间 我对这不在行
{\3c&H000000&\fs30}I-It could take that long, I'm really bad at it.

457
00:19:05,960 --> 00:19:08,060
也许我该从现在行动起来
{\3c&H000000&\fs30}You know, maybe, um,

458
00:19:08,070 --> 00:19:11,700
回去帕萨迪纳
{\3c&H000000&\fs30}I should start right now, and go back to Pasadena

459
00:19:11,700 --> 00:19:14,400
让你好好享受属于自己的时刻
{\3c&H000000&\fs30}and let you have this experience to yourself.

460
00:19:14,410 --> 00:19:16,270
你只是想回去
{\3c&H000000&\fs30}You just want to go back

461
00:19:16,270 --> 00:19:18,540
那个人人稀罕你的地方吧
{\3c&H000000&\fs30}'cause that's where everybody makes a fuss over you.

462
00:19:18,540 --> 00:19:21,880
你的同事们说对了 你真是冰雪聪明
{\3c&H000000&\fs30}You know, your colleagues are right, you are brilliant.

463
00:19:32,700 --> 00:19:34,230
大家好
{\3c&H000000&\fs30}Hello.

464
00:19:34,240 --> 00:19:35,530
-嘿 -欢迎回来 艾米
{\3c&H000000&\fs30}- Hey. - Amy, welcome back.

465
00:19:35,530 --> 00:19:37,700
让我看看戒指
{\3c&H000000&\fs30}Oh, l-let me see the ring.

466
00:19:37,710 --> 00:19:40,210
喔 挺不错啊
{\3c&H000000&\fs30}Ooh, nice.

467
00:19:40,210 --> 00:19:42,510
嘿 非礼勿视
{\3c&H000000&\fs30}H-Hey, her eyes are up there.

468
00:19:44,750 --> 00:19:47,410
就是那个女人亲了谢尔顿吗
{\3c&H000000&\fs30}I-Is that the woman who kissed Sheldon?

469
00:19:47,420 --> 00:19:49,220
呃 可能是吧
{\3c&H000000&\fs30}Uh... Could be.

470
00:19:49,220 --> 00:19:51,720
说不准 说说普林斯顿的事情吧
{\3c&H000000&\fs30}Hard to say. Tell us about Princeton.

471
00:19:51,720 --> 00:19:53,490
等我一下
{\3c&H000000&\fs30}Excuse me for a minute.

472
00:19:53,490 --> 00:19:55,360
不 我们待会儿接着聊
{\3c&H000000&\fs30}Not-- well, we'll catch up later.

473
00:19:55,360 --> 00:19:57,820
这将是继上次我阿姨和我表妹
{\3c&H000000&\fs30}This is going to be the biggest smackdown

474
00:19:57,830 --> 00:19:59,830
在家庭聚会上穿了同一件沙丽之后
{\3c&H000000&\fs30}since my Aunt Noopur showed up at the family reunion

475
00:19:59,830 --> 00:20:02,600
史上最大型的一次正面撕逼
{\3c&H000000&\fs30}wearing the same sari as my cousin Sruti.

476
00:20:03,870 --> 00:20:06,830
你是诺维茨基博士吗
{\3c&H000000&\fs30}Dr. Nowitzki?

477
00:20:06,840 --> 00:20:08,600
噢
{\3c&H000000&\fs30}Oh.

478
00:20:08,600 --> 00:20:12,470
福勒博士 呃 你好
{\3c&H000000&\fs30}Dr. Fowler. Um, hello.

479
00:20:13,310 --> 00:20:15,040
谢谢你 太谢谢你了
{\3c&H000000&\fs30}Thank you. Thank you so much.