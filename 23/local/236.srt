﻿310
00:11:58,270 --> 00:11:59,400
和我们聊聊这个项目吧"
{\3c&H000000&\fs30}Why don't you tell us about that?"

311
00:11:59,410 --> 00:12:01,670
听到没有 这才是好问题
{\3c&H000000&\fs30}See, that's a great question.

312
00:12:01,670 --> 00:12:04,610
好吧 跟我们聊聊那个项目吧
{\3c&H000000&\fs30}Okay, what was that like?

313
00:12:04,610 --> 00:12:08,050
我不能告诉你 那可是机密
{\3c&H000000&\fs30}Oh, I can't tell you that, it's top-secret.

314
00:12:17,920 --> 00:12:20,920
天啊 心好累
{\3c&H000000&\fs30}Boy, that was exhausting.

315
00:12:20,930 --> 00:12:23,390
无意冒犯 但是你的同事好无礼
{\3c&H000000&\fs30}You know, no offense, but your colleagues were pretty rude.

316
00:12:25,160 --> 00:12:26,930
他们无礼 你认真的吗
{\3c&H000000&\fs30}Really, they were rude?

317
00:12:26,930 --> 00:12:27,930
当然
{\3c&H000000&\fs30}Yes.

318
00:12:27,930 --> 00:12:29,300
他们总是顾着说你的事情
{\3c&H000000&\fs30}They just kept talking about you

319
00:12:29,300 --> 00:12:31,000
说你多么了不起
{\3c&H000000&\fs30}and how great you are,

320
00:12:31,000 --> 00:12:33,600
我不停提起自己但他们每次都无视我
{\3c&H000000&\fs30}no matter how many times I brought me up.

321
00:12:33,610 --> 00:12:35,410
他们是我的同事
{\3c&H000000&\fs30}You know, these are my colleagues

322
00:12:35,410 --> 00:12:36,940
想谈论我的工作成果
{\3c&H000000&\fs30}and they want to talk about my work.

323
00:12:36,940 --> 00:12:38,810
你有什么不爽的
{\3c&H000000&\fs30}Why does that bother you so much?

324
00:12:38,810 --> 00:12:41,040
因为我也在场
{\3c&H000000&\fs30}Because I was there.

325
00:12:41,050 --> 00:12:43,080
就像你叫擎天柱一起吃饭
{\3c&H000000&\fs30}It's like having Optimus Prime over to dinner

326
00:12:43,080 --> 00:12:45,250
却不让他变身卡车一样
{\3c&H000000&\fs30}and not asking him to turn into a truck.

327
00:12:46,080 --> 00:12:48,080
你知道吗 谢尔顿
{\3c&H000000&\fs30}You know what, Sheldon?

328
00:12:48,090 --> 00:12:50,850
你并不是到哪里都能智商压制
{\3c&H000000&\fs30}You're not always the smartest person in every room.

329
00:12:50,860 --> 00:12:53,420
甚至在这里 也不一定
{\3c&H000000&\fs30}You may not even be the smartest person in this room.

330
00:12:53,430 --> 00:12:55,190
哦 不好意思
{\3c&H000000&\fs30}Oh, I am sorry.

331
00:12:55,190 --> 00:12:58,530
你是说尼尔·德格拉塞(天文学家)躲在沙发后面么
{\3c&H000000&\fs30}What, is Neil deGrasse Tyson hiding behind the couch?

332
00:12:58,530 --> 00:12:59,730
他要真在 看来他也没那么聪明嘛
{\3c&H000000&\fs30}'Cause if he is, he's not that smart,

333
00:12:59,730 --> 00:13:01,830
因为沙发后面可全是灰
{\3c&H000000&\fs30}it's pretty dusty back there.

334
00:13:05,170 --> 00:13:06,140
嘿 你要去哪儿嘛
{\3c&H000000&\fs30}Hey, where are you going?

335
00:13:06,140 --> 00:13:08,000
我要气冲冲地回我屋里去
{\3c&H000000&\fs30}I'm storming off to my room.

336
00:13:08,010 --> 00:13:10,240
那我要气冲冲地回哪个屋
{\3c&H000000&\fs30}Well, then where am I supposed to storm off to?

337
00:13:10,240 --> 00:13:12,410
哼 你那么聪明 你自己想去吧
{\3c&H000000&\fs30}Well, you're so smart, why don't you figure it out?

338
00:13:16,820 --> 00:13:18,550
还有其他卧室么
{\3c&H000000&\fs30}Is there another bedroom?

339
00:13:19,850 --> 00:13:21,990
要不书房也行
{\3c&H000000&\fs30}Perhaps a-a den?

340
00:13:24,890 --> 00:13:28,360
你们小两口对子宫里的新事件
{\3c&H000000&\fs30}So, how are you guys doing with all the new

341
00:13:28,360 --> 00:13:31,630
适应得怎么样
{\3c&H000000&\fs30}events in your womb?

342
00:13:33,430 --> 00:13:35,600
挺好的
{\3c&H000000&\fs30}Good, you know?

343
00:13:35,600 --> 00:13:37,700
很明显它是个意外惊喜
{\3c&H000000&\fs30}Obviously, it was a surprise.

344
00:13:37,700 --> 00:13:39,770
哭也哭了
{\3c&H000000&\fs30}There was some crying

345
00:13:39,770 --> 00:13:41,070
吵也吵了
{\3c&H000000&\fs30}and some yelling. Some suggestion

346
00:13:41,070 --> 00:13:42,770
打和好炮的提议也被拒绝了
{\3c&H000000&\fs30}of make-up sex that did not go over well,

347
00:13:42,770 --> 00:13:44,770
就算没办法再中奖了也不行
{\3c&H000000&\fs30}even though it's not like we can get more pregnant.

348
00:13:47,850 --> 00:13:50,950
但我们意识到这是个礼物
{\3c&H000000&\fs30}But then we realized that it's a gift, in the sense

349
00:13:50,950 --> 00:13:52,680
即使不是我们主动要的
{\3c&H000000&\fs30}that we didn't ask for it,

350
00:13:52,680 --> 00:13:54,080
也不是我们主动选的
{\3c&H000000&\fs30}and we may not have chosen it...

351
00:13:54,090 --> 00:13:58,050
而且我们已经有一个礼物了
{\3c&H000000&\fs30}And we already have one.

352
00:13:58,060 --> 00:14:00,260
我要是喜欢一件衣服
{\3c&H000000&\fs30}You know, whenever I find a top I like,

353
00:14:00,260 --> 00:14:04,160
我会回头把同款不同色也给入了
{\3c&H000000&\fs30}I always go back and get a second one in a different color.

354
00:14:06,730 --> 00:14:11,130
希望你们的宝宝不会不同色
{\3c&H000000&\fs30}Which I hope is not the case with your baby.

355
00:14:12,670 --> 00:14:16,040
我知道你们有点抓狂 但你们是合格的父母
{\3c&H000000&\fs30}I know you guys are freaked out, but you're great parents,

356
00:14:16,040 --> 00:14:18,840
如果你们需要帮忙 尽管找我们
{\3c&H000000&\fs30}and if you ever need help, we are here for you.

357
00:14:18,840 --> 00:14:20,610
无论什么事 吱一声就行
{\3c&H000000&\fs30}Yeah, anything at all, just ask.

358
00:14:21,650 --> 00:14:23,380
噢 你知道你们能帮我们什么吗
{\3c&H000000&\fs30}Ooh, you know what you could do?

359
00:14:23,380 --> 00:14:26,180
你们也生一个吧
{\3c&H000000&\fs30}You could have a baby, too.

360
00:14:28,890 --> 00:14:31,550
你说啥我听不见
{\3c&H000000&\fs30}I'm sorry, what?

361
00:14:31,560 --> 00:14:34,560
没错 这是个好主意 我们同甘共苦
{\3c&H000000&\fs30}No, that's a great idea, we could go through it together.

362
00:14:34,560 --> 00:14:35,960
应该会很好玩 对吧
{\3c&H000000&\fs30}Wouldn't that be fun?

363
00:14:37,160 --> 00:14:38,360
你们刚刚还在说
{\3c&H000000&\fs30}You guys were just saying

364
00:14:38,360 --> 00:14:40,760
自己有多抓狂多痛苦
{\3c&H000000&\fs30}how freaked out and miserable you are.

365
00:14:40,770 --> 00:14:42,900
我满口跑火车
{\3c&H000000&\fs30}I say lots of crazy things.

366
00:14:42,900 --> 00:14:45,800
毕竟我怀孕了 荷尔蒙作祟
{\3c&H000000&\fs30}I'm pregnant and hormonal.

367
00:14:45,800 --> 00:14:48,170
快点 生一个 赶紧的
{\3c&H000000&\fs30}Do it! Have a baby, do it!

368
00:14:49,540 --> 00:14:52,040
我们的孩子一起玩耍 画面超有爱的
{\3c&H000000&\fs30}Come on, it'd be so cute, our kids playing together?

369
00:14:52,040 --> 00:14:53,880
你们怎么说 赶紧床单滚起来
{\3c&H000000&\fs30}What do you say, why don't you two hit the old mattress

370
00:14:53,880 --> 00:14:55,780
造个小人儿
{\3c&H000000&\fs30}and whip up a family?